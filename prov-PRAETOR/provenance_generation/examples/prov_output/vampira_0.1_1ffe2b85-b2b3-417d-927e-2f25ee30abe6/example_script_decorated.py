
from praetor import genBindings
import types
genBindings.get_modules()
open = genBindings.provWrapOpen(open)
from astroquery.simbad import Simbad

try:
    if isinstance(Simbad, types.FunctionType):
        Simbad = genBindings.decorate_imported_function(Simbad,genBindings.provWrap)
        print("Simbad is function" )
    elif isinstance(Simbad, types.BuiltinFunctionType):
        Simbad = genBindings.decorate_imported_function(Simbad,genBindings.provWrap)
        print("Simbad is built-in function" )
    else:
        genBindings.decorate_all_in_module(Simbad,genBindings.provWrap)
        print("Simbad is Module" )
except:
    print("Module or function Simbad not found")

from astropy import coordinates

try:
    if isinstance(astropy.units.u, types.FunctionType):
        astropy.units.u = genBindings.decorate_imported_function(astropy.units.u,genBindings.provWrap)
        print("astropy.units.u is function" )
    elif isinstance(astropy.units.u, types.BuiltinFunctionType):
        astropy.units.u = genBindings.decorate_imported_function(astropy.units.u,genBindings.provWrap)
        print("astropy.units.u is built-in function" )
    else:
        genBindings.decorate_all_in_module(astropy.units.u,genBindings.provWrap)
        print("astropy.units.u is Module" )
except:
    print("Module or function astropy.units.u not found")


try:
    if isinstance(coordinates, types.FunctionType):
        coordinates = genBindings.decorate_imported_function(coordinates,genBindings.provWrap)
        print("coordinates is function" )
    elif isinstance(coordinates, types.BuiltinFunctionType):
        coordinates = genBindings.decorate_imported_function(coordinates,genBindings.provWrap)
        print("coordinates is built-in function" )
    else:
        genBindings.decorate_all_in_module(coordinates,genBindings.provWrap)
        print("coordinates is Module" )
except:
    print("Module or function coordinates not found")

import astropy.units as u


@genBindings.provWrap
def extract_coords_expand_search(table):
    ra = table['RA'][0]
    dec = table['DEC'][0]
    c = coordinates.SkyCoord(ra + '' + dec, frame='fk4', unit=(u.hourangle, u.deg))
    r = 5 * u.arcminute
    result_table = Simbad.query_region(c, radius=r)
    return result_table


results = Simbad.query_object("m31")
expanded_results = extract_coords_expand_search(results)

with open('result_table.txt', 'w') as f:
    f.write(str(expanded_results))

genBindings.get_modules()