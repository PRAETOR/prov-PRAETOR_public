import os

#print('using new')

memory = True # record allocated RAM per code segment - True/False
wrap_open = True # record provenance for pythons open function - True/False
build_prov = True  # merge all recorded provenance files - True/False
flatten_prov = True  # requires provtoolbox installation, removes duplicates and shrinks - True/False
global_tracking = True # record information on global variables - True/False
mongo_db = False

provenance_directory = '/home/mj1e16/praetor_gitlab/provenance_generation/provenance-generation/examples/prov_output/' # location for generated provenance storage
modules = ['coordinates', 'Simbad', 'astropy.units.u']  # list of python modules/functions to record provenance for - use the name assigned to it in python
blacklist = ['subprocess._cleanup'] #, '_cleanup']  # list of function to not include provenance for
staticmethods = [] # leave blank unless there are complaints about static methods
json_file = 'full_json.json' # name of json file. No need to change, it's defined here as many files need access to it
run_dir = os.path.dirname(os.path.realpath(__file__)) # os.getcwd()
unique_string = '!!!!!!!!!' # identifies func start/end in logs, must not be present in the logs otherwise
max_value_length = 128 # Max number of characters before an entity becomes a big entity and stored separate from the provenance

if provenance_directory[-1] != '/':
    provenance_directory += '/'


