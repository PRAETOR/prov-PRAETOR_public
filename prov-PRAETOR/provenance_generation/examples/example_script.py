from astroquery.simbad import Simbad
from astropy import coordinates
import astropy.units as u


def extract_coords_expand_search(table):
    ra = table['RA'][0]
    dec = table['DEC'][0]
    c = coordinates.SkyCoord(ra + '' + dec, frame='fk4', unit=(u.hourangle, u.deg))
    r = 5 * u.arcminute
    result_table = Simbad.query_region(c, radius=r)
    return result_table


results = Simbad.query_object("m31")
expanded_results = extract_coords_expand_search(results)

with open('result_table.txt', 'w') as f:
    f.write(str(expanded_results))
