import os, fnmatch, subprocess

def findReplace(directory, find, replace, filePattern):
    for path, dirs, files in os.walk(os.path.abspath(directory)):
        for filename in fnmatch.filter(files, filePattern):
            filepath = os.path.join(path, filename)
            with open(filepath) as f:
                s = f.read()

            s = s.replace(find, replace)
            with open(filepath, "w") as f:
                f.write(s)
            if find in filename:
                new_name = filename.replace(find, replace)
                subprocess.call(['mv', path+'/'+filename, path+'/'+new_name])

def find_print(directory, find, replace, filePattern):
    for path, dirs, files in os.walk(os.path.abspath(directory)):
        for filename in fnmatch.filter(files, filePattern):
            filepath = os.path.join(path, filename)
            with open(filepath) as f:
                for line in f.readlines():
                    if find in line:
                        print(line[line.index(find):line.index(find)+7])
                        print(filepath, line)

#findReplace('.', 'vampira', 'praetor', '*.py')
#findReplace('.', 'prtr', 'prtr', '*.py')
#findReplace('.', 'vampira', 'praetor', '*.py')
#findReplace('.', 'PRAETOR', 'PRAETOR', '*.py')

fp = "/home/mj1e16/vampira_gitlab/praetor_public/prov-PRAETOR_public/prov-PRAETOR/provenance_queries/RDF/fuseki/data"

findReplace(fp, 'vampira', 'praetor', '*.ttl')
findReplace(fp, 'VAMPIRA', 'PRAETOR', '*.ttl')
findReplace(fp, 'Vampira', 'Praetor', '*.ttl')
findReplace(fp, 'vamp', 'prtr', '*.ttl')

