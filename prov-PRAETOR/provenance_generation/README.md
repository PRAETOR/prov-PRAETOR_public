# PRAETOR

## Table of Contents

- [Installation](https://gitlab.mpcdf.mpg.de/PRAETOR/prov-PRAETOR_public/-/blob/main/prov-PRAETOR/provenance_generation/README.md#installation)
- [Running PRAETOR](https://gitlab.mpcdf.mpg.de/PRAETOR/prov-PRAETOR_public/-/blob/main/prov-PRAETOR/provenance_generation/README.md#running-praetor)
- [Full example](https://gitlab.mpcdf.mpg.de/PRAETOR/prov-PRAETOR_public/-/tree/main/prov-PRAETOR/provenance_generation/examples)
- [Advanced options](https://gitlab.mpcdf.mpg.de/PRAETOR/prov-PRAETOR_public/-/blob/main/prov-PRAETOR/provenance_generation/README.md#advanced-options)

---

<br>

## Installation

### Installing via pip 

Install prov-PRAETOR including all needed python modules from the PyPI hub:

```
pip install praetor
```

For a python2 installation use the following command:

```
pip install praetor2
```

In addition, a further software packages that can not be installed via pip is needed:

```
wget https://repo1.maven.org/maven2/org/openprovenance/prov/provconvert/0.9.29/provconvert-0.9.29.deb
```
```
dpkg --install provconvert-0.9.29.deb
```


### Installing by hand 

* Before installing prov-PRAETOR some python modules are required and need to be installed manually (also via pip).

    - [codegen](https://pypi.org/project/codegen/) 
    - [memory_profiler](https://pypi.org/project/memory-profiler/)
    - [pandas](https://pypi.org/project/pandas/)
    - [rdflib](https://pypi.org/project/rdflib/)

To install prov-PRAETOR by hand directly from the repository via pip clone the reporitory |

```
git clone https://gitlab.mpcdf.mpg.de/PRAETOR/prov-PRAETOR_public.git
```

```
for python3 (only 3.5+ supported)
    cd python3_package/praetor 
    pip install .

    or 

for python2 (2.7+)
    cd praetor_public/praetor/provenance_generation/python_package/praetor
    pip install .
``` 


### Using the code without installation

* There is also an option to run the code outside the directories.

    - python3; download [praetor_3.zip](https://gitlab.mpcdf.mpg.de/praetor/provenance-generation/-/blob/master/python3/praetor_3.zip) into your workflow directory and unzip it.

    - python2; download [praetor_2.zip](https://gitlab.mpcdf.mpg.de/praetor/provenance-generation/-/blob/master/python2/praetor_2.zip) into your workflow directory and unzip it.


### Using a singularity container 

In case you are working with a workflow on a cluster it might be useful to have a singularity installation of prov-PRAETOR. [Here](https://gitlab.mpcdf.mpg.de/PRAETOR/prov-PRAETOR_public/-/tree/main/prov-PRAETOR/provenance_generation/singularity?ref_type=heads) you can find a receipe how to build a container.


### Provenance Information conversion

In case the software has been installed by hand or just running within the directory, some of the functionality of PRAETOR requires transforming the provenance results into different formats. The [ProvToolBox](https://lucmoreau.github.io/ProvToolbox/) is the base to transform provenance between formats. Detailed installation instructions can be found through the link and it
can be installed via a debian binary as follows:
```
wget https://repo1.maven.org/maven2/org/openprovenance/prov/provconvert/0.9.29/provconvert-0.9.29.deb
```
```
dpkg --install provconvert-0.9.29.deb
```

---

<br>

## Running praetor

Run instructions are the same for all python 2 and 3 versions, packaged or otherwise: 

To run praetor on your python script, prepend your command with praetor_run.py for example:

```
praetor_run.py python <YOUR_PYTHON_SCRIPT.py> <any_optional_arguments>
```

The optional aruments here refer to any arguments that you would usually run with your python script. 

Note, if the praetor_run.py command cannot be found, remember to refresh your terminal, also you may need to enable executables from python packages, see [here](https://stackoverflow.com/questions/35898734/pip-installs-packages-successfully-but-executables-not-found-from-command-line) for examples on how to so. Failing that, installing praetor within a conda environment is also a viable solution. 

<br>


The expected output will be generated in your specified output directory wihtin a folder that has the name of the ID of the pipeline run.

* The output consists of:

    - `/json` - directory containing bindings from your run (values specific to the run)
    - `/templates` - directory containing templates of all functions/classes run during execution
    - `/big_entities` - any value used in python over 128 characters is stored here in a file, the file name is used as the value in the provenance
    - `/function_store` - copies of each function/class run
    - `console_output.log` - a copy of the log from the run, separated at each function instance using your specified string
    - `mergey.provn` - final combined provenance files from the run (unflattened)
    - `flatten_file.provn` - concise version of `mergey.provn` 

---

<br>

## Full example how to run a python script and optain provenance for it
Have a look in the [example](https://gitlab.mpcdf.mpg.de/PRAETOR/prov-PRAETOR_public/-/tree/main/prov-PRAETOR/provenance_generation/examples)

---

<br>

## Advanced options

A full documentation of the individual functions of the PRAETOR software can be found [here](https://praetor.pages.mpcdf.de/provenance-generation)


### The Settings File

The settings file (e.g. [praetor_settings.py](https://gitlab.mpcdf.mpg.de/PRAETOR/prov-PRAETOR_public/-/blob/main/prov-PRAETOR/provenance_generation/python3/praetor_settings.py)) controls things such as the granularity, output location, and information contained within the generated provenance. Alterations to the praetor_settings.py file can be made at anytime with the original copy,for these new settings to take effect, the package may need to be reinstalled or the settings file can be changed without reinstallation by doing the following: 

1) Creating your copy of the praetor_settings.py file in a chosen directory and renaming the file to praetor_settings_user.py
    ```
    cp praetor_settings.py /new/directory/location/praetor_settings_user.py
    ```

2) Add the name of the directory containing the settings file as an environment variable named PRAETOR
    ```
    export PRAETOR="/new/directory/location/"
    ```

After completion of these steps, the file praetor_settings_user.py can be altered at any time and the changes to the settings will take effect without the need for reinstallation. Note this is useful for installation within a container.

<br>

- `memory` - determines whether the memory usage of each function will be tracked, requires the installation of the python memory_prof package (possible values - True/False)
- `wrap_open` - detemines whether pythons built in open function will be wrapped to record the opening of files (possible values - True/False)
- `build_prov` - if set to True, it will combine all of the bindings and templates made during pipeline execution. If set to False you will need to run the combine_prov() routine found within convert2prov.py after pipeline execution (possible values - True/False) 
- `flatten_prov` - If set to True, it will "flatten" the produced provenance, removing duplicates, reformating and reducing provenance size. Setting to True requires a ProvToolBox installation and build_prov must also be set to true (possible values - True/False)
- `global_tracking` - Option to track the change in pythons global variables during pipeline execution. At the beggining and end of each function execution, new or altered global variables will be tracked, recorded, and linked to the executing function as an input or output, respectively (possible values - True/False)
- `modules` - a list of the imported modules or functions to also record provenance for, the names should be in string format and as they are referred to  within the python script. If the modules of the pipeline are unknown, a list of them can be easily created via the [pipreqs](https://github.com/bndr/pipreqs) python package.
- `provenance_directory` - location where output will be generated, default set to './'
- `blacklist` - a list of functions or modules to not generate provenance for, the names should be in string format and as they are referred to within the python script
- `json_file` - name of output json file, can easily be left as 'full_json.json'
- `run_dir` - directory where the script was run, needed for operation and generated automatically
- `unique_string` - string printed to the terminal at the start and end of each function to enable separation of the log file by function
- `max_value_length` - maximum number of allowed characters for an input or output to be stored within the provenance, if the limit is exceeded then the object is stored within the bigEntities directory of the output with a filename equal to the identifier of the proxy object in the provenance

---

### Recording provenance of imported modules and functions

To enable the provenance collection of imported functions or modules within your workflow:

1) Add names of these packages, as stated in your python script, to the modules list within praetor_settings.py
2) Use the same commands as stated in either of the previous two methods to generate the provenance

If the modules of the pipeline are unknown, a list of them can be easily created via the pipreqs python package.

For example, if you have these imports and want to record provenance for each:

```python
import numpy 
import matplotlib.pyplot as plt
from astropy.convolution import convolve_fft
```

Then should alter your praetor_settings file as follows:
```python
 modules = ['numpy', 'plt', 'convolve_fft']
```

Note, the following import format is not currently supported.

```python
from numpy import *
```
---

<br>

### Adding Custom Information into your workflow

With global_tracking set to true, declaring a new general variable above any function execution will add the intended information to the function.

Fake functions can be added to the provenance via comments in your python script when combined with the following syntax: 

- `praetor_insert` - initialises the fake function generation
- `in_names` - input names for function, separated by commas
- `in_values` - input values for function, separated by commas
- `out` - output values to the function, separated by commas
- `name` - name of the function to add

Each of the above fields should be defined and separated by spaces. No spaces should be present within each individual field. Below shows an example input for generating the virtual function and the representation of the definition of that function in python. 

```python
# praetor_insert in_names=in1,in2 in_values=1,2 out=1,2 name=function 
```

Will become a virtual function in your code that looks like this:
```python
def function(in1,in2):
    return out1,out2
```

---

<br>


### Adding additional information and combining provenance

For information on attaching quality metrics within an already produced provenance, please go to [attaching_quality](https://gitlab.mpcdf.mpg.de/PRAETOR/prov-PRAETOR_public/-/tree/main/prov-PRAETOR/provenance_generation/attaching_quality)
