import re
import hashlib

def find_all(inString,findx):
    '''
    Sub-routine used to find import locations
    :param inString: input string to search
    :param findx: substring to look for
    :return: all start locations of findx
    '''
    return [m.start() for m in re.finditer(findx, inString)]

def find_all_locs(string, substring):
    '''
    Sub-routine used to find import locations
    :param inString: input string to search
    :param findx: substring to look for
    :return: all start locations of findx
    '''
    locs = [m.start() for m in re.finditer(substring, string)]
    return locs


def md5(fname):
    '''
    Function to create hash codes that represent data, files, etc.
    :param fname: Name of file to read and hash
    :return: Hash code specific to file fname
    '''
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


def change_agent(agent_line, quality_string):
    '''
    Function alters the agent in a PROV-N document to include quality information
    :param agent_line: String of PROV-N format data that specifies an agent
    :param quality_string: String of quality to be added
    :return: Newly updated agent_line that includes the quality_string addition
    '''
    insert_loc = find_all(agent_line, ']\)')[-1]
    new_line = agent_line[:insert_loc] + quality_string + agent_line[insert_loc:]
    return new_line


def change_entity(entity_line, quality_string):
    '''
    Function alters the entity in a PROV-N document to include quality information
    :param entity_line: String of PROV-N format data the specifies an entity
    :param quality_string: quality_string: String of quality to be added
    :return: Newly updated entity_line that includes the quality_string addition
    '''
    insert_loc = find_all(entity_line, ']\)')[-1]
    new_line = entity_line[:insert_loc] + quality_string + entity_line[insert_loc:]
    return new_line


def add_quality(prov_file, quality_name, quality_value, entity_name=None, prov_out='quality_added.provn'):
    '''
    Function that takes a PROV-N file, a quality metric and associate value, and optionally an entity name as input and
    attaches the quality to the agent in the PROV-N file if no entity_name is specified, otherwise it attaches it to the
    specified entity
    :param prov_file: Name of PROV-N document to attach quality within
    :param quality_name: Name of quality metric
    :param quality_value: Value  of quality metric
    :param entity_name: Value of entity to attach information to
    :param prov_out: Name of generated provenance file with the added quality
    :return: The creation of prov_out which is prov_file but with added quality
    '''
    agent_identifier = 'agent\(var:lifeline'
    quality_string = ", prtr:quality = {{ '{0}', {1} }}".format(quality_name, quality_value)

    if entity_name != None:
        try:
            hash_value = md5(entity_name)
        except:
            hash_value = None

        is_there_hash = data.find(hash_value)

    with open(prov_out, 'w') as outfile:
        outfile.write('')

    with open(prov_file) as infile:
        with open(prov_out, 'a') as outfile:
            for line in infile:
                if entity_name == None:
                    if agent_identifier in line:
                        new_line = change_agent(line, quality_string)
                    else:
                        new_line = line
                elif hash_value != None:
                    if hash_value in line:
                        new_line = change_entity(line, quality_string)
                    else:
                        new_line = line
                else:
                    if entity_name in line:
                        new_line = change_entity(line, quality_string)
                    else:
                        new_line = line
                outfile.write(new_line)


76
## test of add quality - commented for now but revist
# def add_quality_small_data(prov_file, quality_name, quality_value, entity_name=None):
#     '''
#     Function that takes a PROV-N file, a quality metric and associate value, and optionally an entity name as input and
#     attaches the quality to the agent in the PROV-N file if no entity_name is specified, otherwise it attaches it to the
#     specified entity
#     :param prov_file: Name of PROV-N document to attach quality within
#     :param quality_name: Name of quality metric
#     :param quality_value: Value  of quality metric
#     :param entity_name: Value of entity to attach information to
#     :return: The creation of prov_out which is prov_file but with added quality
#     '''
#     with open(prov_file, 'r') as f:
#         data = f.read()
#
#     quality_string = ", prtr:quality = {{ '{0}', {1} }}".format(quality_name, quality_value)
#
#     if entity_name == None:
#         agent_start = find_all_locs(data, 'agent\(var:lifeline')
#         for x in agent_start:
#             agent_line_end = data[x:].find('\n')
#             agent_line = data[x:x + agent_line_end]
#             insert_loc = find_all(agent_line, ']\)')[-1]
#             new_line = agent_line[:insert_loc] + quality_string + agent_line[insert_loc:]
#             data = data[:x] + new_line + data[x + agent_line_end:]
#
#     else:
#         entity_name = "/home/mj1e16/sims_maf/sims_maf_contrib/sims_maf_contrib/data/baseline2018a.db"
#         try:
#             hash_value = md5(entity_name)
#             is_there_hash = data.find(hash_value)
#             if is_there_hash == -1:
#                 print('hash value not found, searching name instead')
#                 entity_locs = find_all_locs(data, entity_name)
#                 for x in entity_locs:
#                     line_end = data[x:].find('\n')
#                     entity_line = data[x:x + line_end]
#                     insert_loc = find_all(entity_line, ']\)')[-1]
#                     new_line = entity_line[:insert_loc] + quality_string + entity_line[insert_loc:]
#                     data = data[:x] + new_line + data[x + line_end:]
#
#             else:
#                 print('hash value not yet enabled')
#         except:
#             print('Unable to generate hash, searching with value')
#             entity_locs = find_all_locs(data, entity_name)
#             for x in entity_locs:
#                 line_end = data[x:].find('\n')
#                 entity_line = data[x:x + line_end]
#                 insert_loc = find_all(entity_line, ']\)')[-1]
#                 new_line = entity_line[:insert_loc] + quality_string + entity_line[insert_loc:]
#                 data = data[:x] + new_line + data[x + line_end:]
#
#     with open(prov_file, 'w') as f:
#         f.write(data)