.. provenance_generation documentation master file, created by
   sphinx-quickstart on Mon Jul 10 10:37:22 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to provenance_generation's documentation!
=================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:  

================
Convert to PROVN
================

.. automodule:: convert2prov
   :members:

.. currentmodule:: convert2prov
.. autosummary::
   :toctree: _autosummary

========
Decorate
========

.. automodule:: decorate
   :members:

.. currentmodule:: decorate
.. autosummary::
   :toctree: _autosummary

=================
Generate Bindings
=================

.. automodule:: genBindings
   :members:

.. currentmodule:: genBindings
.. autosummary::
   :toctree: _autosummary

=======================================
Python Builtin Open Template Generation
=======================================

.. automodule:: python_builtin_wrap_templateGen
   :members:

.. currentmodule:: python_builtin_wrap_templateGen
.. autosummary::
   :toctree: _autosummary

=======
Quality
=======

.. automodule:: quality
   :members:

.. currentmodule:: quality
.. autosummary::
   :toctree: _autosummary

===========
Vampira Run 
===========

.. automodule:: vampira_run
   :members:

.. currentmodule:: vampira_run
.. autosummary::
   :toctree: _autosummary

=================
Vampira Templates
=================

.. automodule:: vampiraTemplates
   :members:

.. currentmodule:: vampiraTemplates
.. autosummary::
   :toctree: _autosummary

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
