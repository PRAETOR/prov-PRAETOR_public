import subprocess

py2_dir = '/home/mj1e16/vampira_gitlab/praetor_public/prov-PRAETOR_public/prov-PRAETOR/provenance_generation/python2/'
py2_pack_dir = '/home/mj1e16/vampira_gitlab/praetor_public/prov-PRAETOR_public/prov-PRAETOR/provenance_generation/python_package/praetor/praetor2/'
py2_pack_bin = '/home/mj1e16/vampira_gitlab/praetor_public/prov-PRAETOR_public/prov-PRAETOR/provenance_generation/python_package/praetor/bin/'

py3_dir = '/home/mj1e16/vampira_gitlab/praetor_public/prov-PRAETOR_public/prov-PRAETOR/provenance_generation/python3/'
py3_pack_dir = '/home/mj1e16/vampira_gitlab/praetor_public/prov-PRAETOR_public/prov-PRAETOR/provenance_generation/python3_package/praetor/praetor/'
py3_pack_bin = '/home/mj1e16/vampira_gitlab/praetor_public/prov-PRAETOR_public/prov-PRAETOR/provenance_generation/python3_package/praetor/bin/'



destinations = [py2_dir, py2_pack_dir, py3_dir, py3_pack_dir]

simple_copies = ['praetorTemplates.py', 'praetor_settings.py']

change_imports_2 = ['python_builtin_wrap_templateGen.py', 'genBindings_py2.py', 'convert2prov.py'] #, 'get_import_versions_py2.py']
change_imports_3 = ['python_builtin_wrap_templateGen.py', 'genBindings_py3.py', 'convert2prov.py'] #, 'get_import_versions_py3.py']


decorate = 'decorate.py'

for location in destinations:
    for py_file in simple_copies:
        subprocess.call(['cp', py_file, location])


for py_file in change_imports_2:
    with open(py_file, 'r') as f:
        data  = f.read()
    for location in [py2_dir, py2_pack_dir]:
        if location == py2_dir:
            write_data = data.replace('from praetor ', '')
        else:
            write_data = data

        out_name = py_file.replace('_py2','')
        with open(location + out_name, 'w') as f:
            f.write(write_data)

for py_file in change_imports_3:
    with open(py_file, 'r') as f:
        data = f.read()
    for location in [py3_dir, py3_pack_dir]:
        if location == py3_dir:
            write_data = data.replace('from praetor ', '')
        else:
            write_data = data

        out_name = py_file.replace('_py3', '')
        with open(location + out_name, 'w') as f:
            f.write(write_data)



# for location in [py2_dir, py3_dir]:
subprocess.call(['cp', decorate, py2_dir + decorate])

with open(decorate, 'r') as f:
    data = f.read()

# py3_shebang = data.replace('#!/usr/bin/env python', '#!/usr/bin/env python3')
# with open(py3_dir + decorate, 'w') as f:
#     f.write(py3_shebang)

new_dec = data.replace('import genBindings', 'from praetor import genBindings')

for location in [py3_pack_dir]:
    new_dec = new_dec.replace('#!/usr/bin/env python', '#!/usr/bin/env python3')
    with open(location+decorate, 'w') as f:
        f.write(new_dec)

new_dec = data.replace('import genBindings', 'from praetor2 import genBindings')

for location in [py2_pack_dir]:
    with open(location+decorate, 'w') as f:
        f.write(new_dec)

with open('praetor_run.py', 'r') as f:
    data = f.read()

pack = data.replace('# from praetor import convert2prov, decorate, genBindings, praetorTemplates',
                    'from praetor import convert2prov, decorate, genBindings, praetorTemplates')
pack = pack.replace('# from praetor import praetor_settings', 'from praetor import praetor_settings')
pack = pack.replace('# from praetor.python_builtin_wrap_templateGen import createOpenTemplate',
                    'from praetor.python_builtin_wrap_templateGen import createOpenTemplate')

pack_2 = data.replace('# from praetor import convert2prov, decorate, genBindings, praetorTemplates',
                    'from praetor2 import convert2prov, decorate, genBindings, praetorTemplates')
pack_2 = pack_2.replace('# from praetor import praetor_settings', 'from praetor2 import praetor_settings')
pack_2 = pack_2.replace('# from praetor.python_builtin_wrap_templateGen import createOpenTemplate',
                    'from praetor2.python_builtin_wrap_templateGen import createOpenTemplate')

py_data = data.replace('# import convert2prov, decorate, praetorTemplates',
                       'import convert2prov, decorate, praetorTemplates')
py_data = py_data.replace('# import praetor_settings', 'import praetor_settings')
py_data = py_data.replace('# from python_builtin_wrap_templateGen import createOpenTemplate',
                          'from python_builtin_wrap_templateGen import createOpenTemplate')

py2_pack = '#!/usr/bin/env python2\n' + pack_2
py2_pack = py2_pack.replace(", 'show', 'praetor']", ", 'show', 'praetor2']")
py3_pack = '#!/usr/bin/env python3\n' + pack

with open(py2_pack_bin+'praetor_run.py', 'w') as f:
    f.write(py2_pack)

# py3_pack = py3_pack.replace("'python'", "'python3'")
# py3_pack = py3_pack.replace("'pip',", "'pip3',")
with open(py3_pack_bin+'praetor_run.py', 'w') as f:
    f.write(py3_pack)

for py_dir in [py2_dir, py3_dir]:
    # py_data = py_data.replace("'python'", "'python3'")
    # py_data = py_data.replace("'pip',", "'pip3',")
    with open(py_dir+'praetor_run.py', 'w') as f:
        f.write(py_data)

