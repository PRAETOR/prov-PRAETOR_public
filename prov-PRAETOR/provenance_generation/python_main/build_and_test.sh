#!/bin/bash

export VAMPIRA="/home/mj1e16/testing_vamp3/fresh_dir/"

source activate astroconda397
cd /home/mj1e16/vampira_gitlab/provenance_generation/provenance-generation/python3_package/vampira
pip install .

cd $VAMPIRA
./cleanProv.sh
cd /home/mj1e16/testing_vamp3/
vampira_run.py exampleScript.py

cd $VAMPIRA
./cleanProv.sh
cd /home/mj1e16/vampira_gitlab/provenance_generation/provenance-generation/python3
python vampira_run.py exampleScript.py

source activate astroconda
cd /home/mj1e16/vampira_gitlab/provenance_generation/provenance-generation/python_package/vampira
pip install .

cd $VAMPIRA
./cleanProv.sh
cd /home/mj1e16/testing_vamp/
vampira_run.py exampleScript.py

cd $VAMPIRA
./cleanProv.sh
cd /home/mj1e16/vampira_gitlab/provenance_generation/provenance-generation/python2
python vampira_run.py exampleScript.py
