import sys
import subprocess

fileName = sys.argv[1]

with open(fileName,'r') as f:
    data = f.read()

#data = data.replace('prefix cx <xa>','prefix cx <http://example.org/>')

#with open(fileName,'w') as f:
#    f.write(data)
outfile = fileName.replace('.provn','.ttl')

subprocess.call(['provconvert','-infile',fileName,'-index','-flatten','-outfile',outfile])
