# Quality Handling


## Usage

The script can be run using:
```
python add_quality.py <optional arguments>
```
However, add_quality.py must be contained in the run directory

Alternatively, a directory independant option is available if you have installed the Vampira python packaged, you can simply use:

```
add_quality.py <optional arguments>
```

Either option provides the same results, for the remainder of the documentation, we will use the former but the latter may also be used in place in exactly the same fashion.

### Command line options

Command line usage has a number of optional parameters:

- `-f` `--provfile` Name of the provenance file to attach the provenance to
- `-q` `--quality` The name of the quality measure to be applied
- `-v` `--value` The value of the quality metric to be applied
- `-i` `--provid` Identifier of an object within the provenance
- `-e` `--entity` Tthe quality will be attached to all entities with this value
- `-p` `--preserve` Preserves the format of your quality name and value
- `-w` `--workflow` Provenance file from which the quality was derrived
- `-t` `--type` The type of the object to attach to (entity, activity, or agent)
- `-h` `--help` help 

### Examples

The only attributes that always need to be set are `--provfile`, `--quality`, and `--value`. If only these are used then the given quality metric will be assigned to the pipeline itself (the agent in the provenance). The following command will attach a completeness of 1 to a pipeline:

```
python add_quality.py -f prov_file.provn -q completeness -v 1
```

To attach the quality to a different object in the provenance, the `--provid` attribute in combination with `--type` should be used. The `--provid` attribute should be set as the id of the object in the provenance and the `--type` should designate whether the id refers to an activity, entity, or agent.

For instance, to attach the quality from the previous example to an activity with id - urn_uuid:bdsf_cb736d6b-0153-46a0-8882-09bb74dd58c2, use the following command:

```
python add_quality.py -f new_prov.provn -q completeness -v 1 -i urn_uuid:bdsf_cb736d6b-0153-46a0-8882-09bb74dd58c2 -t activity
```

To attach the same quality to an entity with id - run:globals_imageName_praetor_1:

```
python add_quality.py -f new_prov.provn -q completeness -v 1 -i run:globals_imageName_praetor_1 -t entity
```

An alternative for attaching qualities to entities is to make use of the `--entity` attribute. This command can be set as a value and the quality will be attached to all entities which have that value. For instance to attach the same quality metric to all entities with a value "file_name.txt", use the following command:

```
python add_quality.py -f new_prov.provn -q completeness -v 1 -e file_name.txt
```

 Note, this is not efficient and should not be used in combination with large provenance datasets. In this case it is better to used the ID directly.

Finally, if the quality metric was generated within a second pipeline, you can use the `--workflow` attribute to specify the provenance from this second pipeline and attach both the quality and second workflow to the original, adding relations between all the quality and both pipelines. Example usage is as follows:


```
python add_quality.py -f new_prov.provn -q completeness -v 1 -e file_name.txt -w
other_prov_file.provn
``` 

Special characters are escaped to conform to prov format within this code. To diable this, use the optional argument "-p True". Use of this is not advised as it may cause problems with building if the provenance is not prov compatible. 


### Output

The output is an updated version of the input provn file which contains the addition of the quality metric. The original provn file is unchanged, the new file has the same name as the original but with _quality appended.

For instance, if your input provenance file was called ~/PROV/flatten_file.provn, the ouptut would be ~/PROV/flatten_file_quality.provn 
