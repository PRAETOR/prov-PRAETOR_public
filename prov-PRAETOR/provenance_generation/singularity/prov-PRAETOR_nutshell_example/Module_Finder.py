
###
#
# HRK and MJ 2024
#
# prov_PRAETOR tool to get a list of the modules loaded by a package
#
###

# switch off the warning
import warnings ; warnings.warn = lambda *args,**kwargs: None

import types
import sys
import importlib

from optparse import OptionParser

def main():

    parser       = new_argument_parser()
    (opts, args) = parser.parse_args()

    if opts.mod_name == None:
        parser.print_help()
        sys.exit()

    mod_name                 = opts.mod_name.replace('.py','')
    reduce_depth             = opts.reduce_depth
    prov_setting_file        = opts.ps_file
    prov_setting_file_dir    = opts.ps_file_dir

    # import the module
    imp_lib  = importlib.import_module(mod_name,package=None)

    # get it done
    #
    mods           = RecursiveModuleWrapping()
    submodules     = mods.get_all_modules_recursive(imp_lib,mod_name)

    # clean up the mod name
    submodule_list = [x.replace(mod_name+'.','') for x in submodules]

    # determine module depth
    #
    depth      = 0
    for sm in submodule_list:
        if depth < str(sm).count('.'):
            depth = str(sm).count('.')

    # reduce list of modules
    #
    if reduce_depth > 0:
        reduced_submodule_list = []
        for sm in submodule_list:
            if str(sm).count('.') <= reduce_depth:
                reduced_submodule_list.append(sm)

        submodule_list = reduced_submodule_list


    if len(prov_setting_file) == 0:
        # need to place this into to praetor_settings_user.py file 
        # the easiest is to pipe it with >> 
        #
        print('\nmax module depth: ',depth,'\n\n')
        print('modules = ',submodule_list)

    else:
        filename = prov_setting_file
        if len(prov_setting_file_dir) !=0:
            filename = prov_setting_file_dir+'/'+prov_setting_file

        f = open(filename,'r')
        fdata = f.read()
        f.close()

        new_fdata = fdata.replace('modules              = []','modules = '+str(submodule_list))

        f = open(filename,'w')
        f.write(new_fdata)
        f.close()

# check all the submodules

class RecursiveModuleWrapping:

    recursion_stopper = []

    def get_all_modules_recursive(self, og_import, og_import_string):
        to_decorate = []
        self.recursion_stopper.append(og_import)
        level_one = dir(og_import)

        level_one_clean = [x for x in level_one if x[0] != '_']

        for pot_mod in level_one_clean:
            try:
                mod_attr = getattr(og_import, pot_mod)
                
            except (ModuleNotFoundError, AttributeError, TypeError): 
                mod_attr = 'None'
            try:
                if mod_attr not in self.recursion_stopper:
                    if isinstance(mod_attr, types.ModuleType): 
                        full_mod_name = '{}.{}'.format(og_import_string,pot_mod)
                        to_decorate.append(full_mod_name)
                        to_decorate.extend(self.get_all_modules_recursive(mod_attr, full_mod_name))
            except (ValueError, TypeError):
                pass
        return to_decorate



def new_argument_parser():

    #
    # some input for better playing around with the example
    #
    usage = "list of python modules to record provenance usage: %prog [options]"
    parser = OptionParser(usage=usage)


    parser.add_option('--MODULE_FILENAME', dest='mod_name', type=str,
                      help='Filename of your python script/pipeline/workflow.')


    parser.add_option('--MODULE_DEPTH', dest='reduce_depth', type=float,default=0,
                      help='Set the module depth to be recorded.[default = 0]')

    parser.add_option('--SETTINGS_FILE', dest='ps_file', type=str,default='',
                      help='praetor settings file name to add the modules.')

    parser.add_option('--SETTINGS_FILE_DIR', dest='ps_file_dir', type=str,default='',
                      help='Directory were the praetor settings file is located.')

    return parser


if __name__ == "__main__":
    main()

#
#


