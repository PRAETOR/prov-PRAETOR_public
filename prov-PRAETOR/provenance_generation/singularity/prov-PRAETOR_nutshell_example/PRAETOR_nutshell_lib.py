#
# prov-PRAETOR nutshell example
#
# example library
#
# HRK 2024
#
# What do you want to take away from this
# exersise 
#

import numpy as lib_np

def random_value_from_example_lib(howmany=1):
    """
    uses numpy to provide a random number out of a distribution 
    https://numpy.org/doc/stable/reference/random/generated/numpy.random.normal.html
    """

    mu, sigma = 42, 2.2 # mean and standard deviation
    rvariable = lib_np.random.normal(mu, sigma,howmany)

    return rvariable[0]

def plane_function(value):
    """
    return a consatnt value
    """

    value = 15

    return value

