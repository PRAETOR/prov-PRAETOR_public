#
# prov-PRAETOR nutshell example
#
# What do you want to take away from this
# exersise.
#
# 1. How to address functions to be documented within PREATOR
# 
#
import os
import numpy as np
import PRAETOR_nutshell_lib as PNLIB
from numpy.random  import uniform
from Nutshell import PRAETOR_nutshell_mod as NTMOD


def random_value(mu,sigma):
    """
    uses numpy to provide a random number out of a gaussian distribution 
    mu, sigma = mean and standard deviation
    https://numpy.org/doc/stable/reference/random/generated/numpy.random.normal.html
    """
    howmany   = 1
    rvariable = np.random.normal(mu, sigma, howmany)

    return rvariable[0]

def main():
    #
    # Example workflow 
    #

    #
    # 1. example of a system call function  
    #
    homedir  = '/PROV_DATA'
    filename = 'praetor_settings_user.py'
    info     = os.popen('grep "modules" '+homedir+'/'+filename)
    getinfo  = info.read().split()

    #
    #  2. example of calling function 
    #
    nsteps = np.arange(5) # executing the functions 5 times
    #
    # some variables
    #
    sigma  = 0.1
    build_a_list_of_random_variables     = []
    build_a_list_of_lib_random_variables = []
    build_a_list_of_mod_random_variables = []
    build_a_list_of_uniform_variables    = []
    #
    for n in nsteps:
        #
        # calling of a internal function
        #
        rv_value = random_value(n,sigma)
        build_a_list_of_random_variables.append(rv_value)
        #
        # calling out of the library (that is located in your directory)
        #
        build_a_list_of_lib_random_variables.append(PNLIB.random_value_from_example_lib(1))
        #
        # calling out of the Nutshell module (that is located in a sub-directory)
        #
        build_a_list_of_mod_random_variables.append(NTMOD.random_value_from_example_mod(1))
        #
        # calling out of the numpy library
        #
        build_a_list_of_uniform_variables.append(uniform(0,42,1)[0])


    #
    # 3. call plane function
    #
    value = 40
    #
    plane_value = PNLIB.plane_function(value)

    # 
    # provide the output
    #
    print(info)
    print(getinfo)
    print(build_a_list_of_random_variables)
    print(build_a_list_of_lib_random_variables)
    print(build_a_list_of_mod_random_variables)
    print(build_a_list_of_uniform_variables)
    print(plane_value)


if __name__ == "__main__":
    main()
