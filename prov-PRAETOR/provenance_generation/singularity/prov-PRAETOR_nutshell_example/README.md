# prov-PRAETOR Singularity n2n nutshell example 

This example shows how to generate provenance, setup the settings file, check some info via the user interface and finally how to extract information via the RDF/fuseki example.  



---
---


## 1. Generate provenance with prov-PRAETOR and the Singularity image

- download the depository
  ```
  git clone https://gitlab.mpcdf.mpg.de/PRAETOR/prov-PRAETOR_public.git
  ```

- produce the singularity image
  ```
  cd prov-PRAETOR_public/prov-PRAETOR/provenance_generation/singularity/
  ```
  ```
  singularity build --fakeroot CONTAINER_NAME.simg singularity.praetor.recipe
  ```

- setup your environment and get a copy of the praetor_settings_user.py file
  ```
  cd prov-PRAETOR_nutshell_example
  ```
  ```
  mkdir PROV_DATA
  ```
  ```
  singularity exec --bind ${PWD}/PROV_DATA:/PROV_DATA ../CONTAINER_NAME.simg cp /opt/requirements/prov_praetor/praetor_settings.py /PROV_DATA/praetor_settings_user.py
  ```


- edit the praetor_settings_user.py file to define the modules for which provenance should be collected

	Now generally you do not always know which modules are loaded. If you would like to have some specific information 
	you may to execute the following and look at the list of modules.

	```
	singularity exec --bind ${PWD}/PROV_DATA:/PROV_DATA --bind ${PWD} ../CONTAINER_NAME.simg python -Wignore Module_Finder.py --MODULE_FILENAME=PRAETOR_nutshell_example.py
	```

	The output can be quite large and the Module_Finder.py provides
    you with the following output:

	max module depth:  7
		
	modules =  ['NTMOD', 'NTMOD.lib_np', 'NTMOD.lib_np.char', 'NTMOD.lib_np.char.functools', ... ,'NTMOD.lib_np.testing.extbuild.sysconfig', 'NTMOD.lib_np.version', 'PNLIB']


	Edit the praetor_settings_user.py with your favoried editor,
	and define the modules you would like to capture the provenance
    for. Let's be lazy and just collect all the info to the depth of 2.

	```
	singularity exec --bind ${PWD}/PROV_DATA:/PROV_DATA --bind ${PWD} ../CONTAINER_NAME.simg python -Wignore Module_Finder.py --MODULE_FILENAME=PRAETOR_nutshell_example.py --SETTINGS_FILE=praetor_settings_user.py --SETTINGS_FILE_DIR=PROV_DATA --MODULE_DEPTH=2
	```

	Now the modules should be included the settings file.

- execute the program 

	```
	singularity exec --bind ${PWD}/PROV_DATA:/PROV_DATA --bind ${PWD} ../CONTAINER_NAME.simg praetor_run.py python PRAETOR_nutshell_example.py
	```

	This is the console line output:


	> <os._wrap_close object at 0x7f114f1f2d00>
	>
	> ['#', 'list', 'of', 'python', 'modules/functions', 'to', 'record', 'provenance', 'for', '-', 'use', 'the', 'name', 'assigned', 'to', 'it', 'in', 'python', 'modules', '=', "['NTMOD',", "'NTMOD.lib_np',", "'NTMOD.lib_np.char',", "'NTMOD.lib_np.compat',", "'NTMOD.lib_np.ctypeslib',", "'NTMOD.lib_np.emath',", "'NTMOD.lib_np.fft',", "'NTMOD.lib_np.lib',", "'NTMOD.lib_np.linalg',", "'NTMOD.lib_np.ma',", "'NTMOD.lib_np.polynomial',", "'NTMOD.lib_np.random',", "'NTMOD.lib_np.rec',", "'NTMOD.lib_np.testing',", "'NTMOD.lib_np.version',", "'PNLIB']"]
	>
	> [-0.2253530476235168, 1.040557757923806, 2.031195311921274, 2.895840551709298, 3.7681074052055754]
	>
	> [42.82523029131445, 41.54680498893624, 38.93387014751603, 42.829427166422775, 41.05873710633103]
	>
	> [43.58474700518763, 39.10781942065631, 40.41301036178844, 43.554977650974415, 42.74411374237226]
	>
	> [15.914348345147639, 35.80985632112591, 31.319366553785787, 29.922781150007456, 27.149788073866898]
	>
	> 15
	>
	> !!!!!!!!! function_end: main.main
	>
	>   provconvert -infile mergey.provn -index -flatten -outfile flatten_file.provn



	The **captured provenance** of this execution is stored in a directory in PROV_DATA/praetor_VERSION_UNIQUE_STRING.

	In the directory you will find the following:

	```
	big_entities/
	console_output.log
	flatten_file.provn
	function_store/
	json/
	Makefile
	mergey.provn
	PRAETOR_nutshell_example_decorated.py
	prov/
	prtr_timing_praetor_3.6.4_f158dc81-250e-43ee-9b17-4b7f5f3c36f1.txt
	__pycache__/
	templateDictionary.py
	templates/
	```



## 2. Use the user interface to have a first glimse on the provenance data

We assume the [user_interface](https://gitlab.mpcdf.mpg.de/PRAETOR/prov-PRAETOR_public/-/tree/main/prov-PRAETOR/user_interface?ref_type=heads) - GUI to explore the workflows provenance
 has been installed and is running.

start the frontend via browser (on [http://localhost:80](http://localhost:80))

load the mergey.provn file (in general you are also able to load the flatten_file.provn)

- UI overview of the provenance and the functions 
![alt text](output_information/PRAETOR_nutshell_example_UI_1.png "UI overview information"){width=25%}


- UI provenance information of plane function 
![alt text](output_information/PRAETOR_nutshell_example_UI_plane_function.png "provenance plane function"){width=25%}

- UI provenance information of random_value function 
![alt text](output_information/PRAETOR_nutshell_example_UI_random_value.png "provenance random value function"){width=25%}

- UI provenance information of example lib random_value function 
![alt text](output_information/PRAETOR_nutshell_example_UI_random_value_from_example_lib.png "provenance random value function"){width=25%}

- UI provenance information of example mod random_value function 
![alt text](output_information/PRAETOR_nutshell_example_UI_random_value_from_example_mod.png "provenance random value function"){width=25%}


## 3. Example to use the database query to optain information of the nutshell function random_value 

We are following the [RDF/fuseki](https://gitlab.mpcdf.mpg.de/PRAETOR/prov-PRAETOR_public/-/tree/main/prov-PRAETOR/provenance_queries/RDF/fuseki)
example.

For that we first need to convert the provn files into a ttl format.

Assuming your are still in the following directory prov-PRAETOR_public/prov-PRAETOR/provenance_generation/singularity/prov-PRAETOR_nutshell_example/PROV_DATA/praetor_3.X.X_XXXXXXXXXXXXXXX

- convert provn file format to ttl file format

	```
	singularity exec --bind ${PWD} ../../../CONTAINER_NAME.simg provconvert -infile mergey.provn -index -flatten -outfile mergey_nutshell_example.ttl
	```

	Either you have already installed praetor in your system or for testing purposes use the conda envoronment to do so. We are 
	setting up a conda environment here.

- optional test environment

	```
	conda create -n TEST_PRAETOR pip
	conda activate TEST_PRAETOR
	pip install praetor
	```

- load the provenance into a RDF/fuseki

	```
	python3
	import praetor.praetor_analysis_rdf as praetor_analysis
	prov_file_ttl = 'mergey_nutshell_example.ttl'
	pipeline_name = praetor_analysis.upload_provenance(prov_file_ttl)
	results = praetor_analysis.query_activity(pipeline_name)
	results[results['functionSource.value'] == 'main']
	```

	> Output is:
	> 
	> results[results['functionSource.value'] == 'main']
	> activityID.value  duration (s) functionName.value           startTime.value             endTime.value memory.value functionSource.value
	>
	>
	> 7    urn:uuid:random_value_2cf6ba06-8b93-41c7-b95f-...         0.249  ...  57.80859375                 main
	>
	> 29   urn:uuid:random_value_163b6def-7aa2-45e2-b43b-...         0.218  ...  59.06640625                 main
	>	
	> 51   urn:uuid:random_value_4e332dba-e9fc-40ad-8774-...         0.217  ...     59.09375                 main
	>
	> 73   urn:uuid:random_value_6f5c5145-8e6b-4db6-ac40-...         0.215  ...  59.11328125                 main
	>
	> 95   urn:uuid:random_value_f99f7a73-7c08-49d4-b746-...         0.267  ...   59.1484375                 main
	>
	> 112  urn:uuid:main_c899e402-3b3c-43b7-a7f4-8b1708f3...         3.828  ...    59.171875                 main

- Now let investigate the output of the function random_value_from_example_lib

	```
	activity = '"random_value_from_example_lib"'
	results = praetor_analysis.query_output_individual(pipeline_name, activity)
	print(results)
	```

	> Output is:
	>
	> 0  urn:uuid:random_value_from_example_lib_99a30d2...  run:random_value_from_example_lib_outEntity_0_...   42.82523029131445
	> 1  urn:uuid:random_value_from_example_lib_da62db8...  run:random_value_from_example_lib_outEntity_0_...   41.54680498893624
	> 2  urn:uuid:random_value_from_example_lib_e76588b...  run:random_value_from_example_lib_outEntity_0_...   38.93387014751603
	> 3  urn:uuid:random_value_from_example_lib_d344a70...  run:random_value_from_example_lib_outEntity_0_...  42.829427166422775
	> 4  urn:uuid:random_value_from_example_lib_091bf13...  run:random_value_from_example_lib_outEntity_0_...   41.05873710633103

- An example code shows how to extract in- and output information of the plane function using the PRAETOR module
for the RDF/fuseki database to pandas. The extracted values (print(inputs_plane[’value.value’]) = 40 and
the outputs = 15) are consistent with those seen in the UI figure above.

	```
	import praetor.praetor_analysis_rdf as praetor_analysis
	prov_file_ttl = ’mergey_nutshell_example.ttl’
	activity      = ’plane_function’

	pipeline_name = praetor_analysis.upload_provenance(prov_file_ttl)
	activity_info = praetor_analysis.query_activity(pipeline_name)
	activity_id   = activity_info [’activityID.value’][activity_info[’activityID.value’].str.contains(activity)]
	activity_ID   = activity_id.values[0]
	inputs        = praetor_analysis.query_input(pipeline_name)
	inputs_plane  = inputs.loc[inputs[’activityID.value’] == activity_ID]

	outputs = praetor_analysis.query_output(pipeline_name)
	outputs_plane = outputs.loc[ outputs [’activityID.value’] == activity_ID]

	print(inputs_plane[’value.value’])
	print(outputs_plane[’value.value’])
	```



	For more examples please look into the
    [RDF/fuseki](https://gitlab.mpcdf.mpg.de/PRAETOR/prov-PRAETOR_public/-/tree/main/prov-PRAETOR/provenance_queries/RDF/fuseki)
    tutorials.



- Delete the optional test environment after testing

	```
	conda deactivate
	conda remove -n TEST_PRAETOR --all
	```

