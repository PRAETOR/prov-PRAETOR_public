#
# praetor setting for the singularity container build
#
import os

memory               = True           # record allocated RAM per code segment - True/False
wrap_open            = True           # record provenance for pythons open function - True/False
build_prov           = True           # merge all recorded provenance files - True/False
flatten_prov         = True           # requires provtoolbox installation, removes duplicates and shrinks
global_tracking      = True           # option to record global variables throughout the code

# location for generated provenance storage
#
provenance_directory = '/PROV_DATA/'   

# list of python modules/functions to record provenance for - use the name assigned to it in python
#
modules              = []  

# list of function to not include provenance for
#
blacklist            = [] 

# name of json file. No need to change, it's defined here as many files need access to it
#
json_file            = 'full_json.json' 

# leave blank unless there are complaints about static methods 
#
staticmethods        = [] 

# identifies func start/end in logs, must not be present in the logs otherwise
#
unique_string        = '!!!!!!!!!' 

# maximum no go chars for a value before it is stored separately from the provenance
#
max_value_length     = 128 

# do not edit after this
#
run_dir              = os.path.dirname(os.path.realpath(__file__)) # os.getcwd()

if provenance_directory[-1] != '/':
    provenance_directory += '/'


