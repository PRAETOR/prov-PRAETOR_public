# prov-PRAETOR Singularity 

---

## Standalone prov-PRAETOR container

Maybe the easiest way to get going is to run a container based
installation. For this you need to get
['singularity'](https://sylabs.io/docs/#singularity)
on your machine first.

For this 3 files are needed in the directory when building the container.

- requirements_add_packages_via_pip.txt 
      you can add here the packages you may need for your workflow/pipeline
- requirements_praetor.txt 
      define the packages that are needed for PRAETOR and should not be edited at all
- praetor_settings.py
      defines all the settings that are needed for PRAETOR, see step 2 you need to edit.

0) Just copy the entire git depository 

```
git clone https://gitlab.mpcdf.mpg.de/PRAETOR/prov-PRAETOR_public.git

cd prov-PRAETOR_public/prov-PRAETOR/provenance_generation/singularity
```

1) Build the singularity image with (note you need fakeroot permission):

```
singularity build --fakeroot CONTAINER_NAME.simg singularity.praetor.recipe
```

2) setup the working space 

- generate a new working directory
```
mkdir PROV_DATA
```

 - execute your script via the singularity to copy the settings file into your working directory

```
singularity exec --bind ${PWD}/PROV_DATA:/PROV_DATA CONTAINER_NAME.simg cp /opt/requirements/prov_praetor/praetor_settings.py /PROV_DATA/praetor_settings_user.py
```

```
cd PROV_DATA
```

      edit praetor_settings_user.py to define the modules for which provenience should be collected

```
cd ..
```


3) run prov_praetor

```
singularity exec --bind ${PWD},${PWD}/PROV_DATA:/PROV_DATA CONTAINER_NAME.simg praetor_run.py python YOUR_PYTHON_CODE.py
```

### A full example

[Nutshell example](https://gitlab.mpcdf.mpg.de/PRAETOR/prov-PRAETOR_public/-/tree/main/prov-PRAETOR/provenance_generation/singularity/prov-PRAETOR_nutshell_example)



## Adding prov-PRAETOR to an exsisting container

In case you already have a singularity container and would like to install prov-PRAETOR into that. Either use your receipe file to build the container and copy most of the singularity.praetor.recipe into that or just use the sandbox option of singularity and install the software step by step.

```
singularity build --sandbox workflow_sb/ YOUR_CONTAINER.simg
```

and use the installation singularity.praetor.recipe to add all the required packages. 

e.g.

```
singularity exec --writable workflow_sb/ pip install praetor
```
 and add all the nessesary requirements.

Produce a new container with

```
singularity build YOUR_CONTAINER_PRAETOR.simg  workflow_sb/
```

