# Praetor Provenance Model

Ontology for the Praetor Provenance Model.

https://praetor.pages.mpcdf.de/prov-PRAETOR_public/

# Note on decisions

A singular entity may be used as both a parameter and a dataset depending on context. Therefore, the quality of whether it is a parameter or a piece of data is defined within the relation to an activity, rather than within the entity itself. 

An entity is considered to be dynamic if it is stored in memory, whereas a entity written to disc is considered as persistant. 
