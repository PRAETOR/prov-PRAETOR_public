# Run backend

## venv and pip
```bash
python -m venv env # create venv on Windows

. env/bin/activate  # On Windows use `env\Scripts\activate`

pip install -r /path/to/requirements.txt # install dependencies
# For Windows installation of uswgi may fail. fix: https://debugah.com/solved-windows-installation-uwsgi-error-attributeerror-moduleos-has-no-attributeuname-15498/
```

## run local development server
```bash
# cd radioastronomie because it's not containing manage.py 
python3 manage.py runserver
```

NOTE: don't use the dev server in production :)

## demo request
```bash
curl -H "Accept: application/json" http://127.0.0.1:8000/api/ 
```

## backend structure 

### /radioastronomie
+ Django Project


+ contains
    + [urls.py](radioastronomie/urls.py) (Url Mapping/ REST END-POINTS)
    + [settings.py](radioastronomie/settings.py) (project configuraions)


### /radioastronomie/provnhandling
+ Django app


+ contains
    + [views.py](radioastronomie/provnhandling/views.py) (most logic behind the rest requests, urls are mapped to functions inside this file)
    + [StaticUtils/StaticUtils.py](radioastronomie/provnhandling/StaticUtils/pipelineHelperFunctions.py) (functions, that get called from the views.py, to reduce redundancy)
    + [StaticUtils/pipelineService.py](radioastronomie/provnhandling/StaticUtils/pipelineService.py) (pipelines-related functions, that get called from the views.py containing the SPARQL queries and some pre-processing)
    + [StaticUtils/graphService.py](radioastronomie/provnhandling/StaticUtils/graphService.py) (graphs-related functions, that get called from the views.py, to reduce redundancy)



## api calls
+ ```api/getPipelines/```
    + GET
+ ```api/getAgents```
    + GET 
    + (url) param 'pipeline' requiered
+ ```upload/```
    + POST 
    + receives a .ttl file as mulitpart/form-data
+ ```api/getEntities```
    + GET
    + (url) param 'pipeline' requiered
    + (url) param 'type' requiered ('in' or 'out')
+ ```api/deletePipeline```
    + GET
    + (url) param 'pipeline' requiered
+ ```api/getPipelineMemoryConsumption```
    + GET
    + (url) param 'pipeline' requiered
+ ```api/getPipelineSummary```
    + GET
    + (url) param 'pipeline' requiered

    


### Example Responses


```api/getPipelines```
```json
{
    "data": [
        {
            "displayName": "NEW_swep_prov",
            "fullName": "http://NEW_swep_prov_py2"
        }
}
```

```json
{
    "agents": [
        "lifeline"
    ],
    "associations": [
        [
            "http://www.w3.org/1999/02/22-rdf-syntax-ns#type",
            "http://www.w3.org/ns/prov#Agent"
        ],
        [
            ...
        ]
    ]
}

```


```api/getEntities``` type = "in"
```json
{
    "entities": [
        [
            "sextractor_inEntity_0_praetor_1",
            "value",
            "kplr2009114174833_ffi\\\\\\\\\\\\\\\\-cal_TEMPLATE_PLUS5000.fits"
        ],
        [
            ...
        ]
    ]
}
```

```api/getEntities``` type = "out"
```json
{
    "entities_value": [
        [
            "sextractor_outEntity_0_praetor_1",
            "value",
            "sextractor_outEntity_0_praetor_1.txt"
        ],
        [
            ...
        ]
    ],
    "entities_wasDerivedFrom": [
        [
            "sextractor_outEntity_0_praetor_1",
            "wasDerivedFrom",
            "sextractor_inEntity_0_praetor_1"
        ],
        [
            ...
        ]
    ]
}
```

```api/getPipelineMemoryConsumption```
```json
[
    {
        "uuid": "generateHistogram_d4ac1adf-1166-4d95-9461-7755d8b69917",
        "type": "generateHistogram",
        "startedAtTime": "2021-10-27T14:51:30.000Z",
        "endedAtTime": "2021-10-27T14:55:06.000Z",
        "memory": "155.76953125",
        "qualifiedUsage": "_:genid-0ac5e06521984ad2a004ddfe86cb9255-blank8",
        "wasAssociatedWith": "lifeline"
    },
    {
        ...
    }
]
```

```api/getPipelineSummary```
```json
{
    "data": {
        "uploadTime": "1643561227671",
        "countInEntities": 17,
        "numberOfOperations": 3
    }
}
```
