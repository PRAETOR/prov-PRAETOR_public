#!/bin/sh

set -e

uwsgi --socket :8001 --master --enable-threads --module radioastronomie.wsgi