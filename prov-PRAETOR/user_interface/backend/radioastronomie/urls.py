"""radioastronomie URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from rest_framework import routers
from radioastronomie.provnhandling import views

# router = routers.DefaultRouter()
# router.register()

urlpatterns = [
    path('admin/', admin.site.urls),
    # path('api/', include('rest_framework.urls', namespace='rest_framework'))
    path('test/', views.hello_world),
    path('api/getPipelines/',                 views.get_pipelines),
    path('api/getAgents',                     views.get_agent_of_pipeline),
    path('api/upload/',                       views.FileUploadView.as_view()),
    path('upload/',                           views.FileUploadView.as_view()),
    path('api/getEntities',                   views.get_entities),
    path('api/deletePipeline',                views.delete_pipeline_by_name),
    path('api/getPipelineMemoryConsumption',  views.get_pipeline_memory_consumption),
    path('api/getPipelineSummary',            views.get_pipeline_summary),
    path('api/getAccessedFiles',              views.get_accessed_files),
    path('api/compare',                       views.get_difference),
    path('api/recommend',                     views.get_recommendation),
    path('api/getFunctions',                  views.get_functions),
    path('api/getRuntime',                    views.get_pipeline_time)
]
