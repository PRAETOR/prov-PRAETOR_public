"""contains helper functions that are used by several instances in pipelineService.py """
import logging
import requests
import os
import re
from radioastronomie.settings import BASE_DIR
from radioastronomie.provnhandling.config import SPARQL_REST_URL



def clear_response(arr):
    '''when querying the database, there is also information return, which is not needed: 's,p', 's,p,o', '''
    logging.debug('StaticUtils: clearing given array')
    unwanted_strings = ['s', '', ' ', 's,p', 's,p,o', 'p,o', 'o', 'p', 'g', 'g,s,p,o', 's,p,o,r', 'activity,entity,p,dt,value,role,startTime,endTime', 'activity,entity,p,dt,value,startTime,endTime', 's,s_count', 's,s_count,graphs', 's_count',
                        's,s_count,p,o,o_count', 'uuid,s,p,o', 's,g,s_count', 's,s_count,roles', 's,ts,te', 's,u','o_count']

    for string in unwanted_strings:
        if string in arr:
            arr.remove(string)

    logging.debug('StaticUtils: cleared list: ' + str(arr))

    return arr


def query_handler(query):
    '''sends the query and returns the cleared result'''
    response = requests.post(SPARQL_REST_URL,
                             data={'query': query},
                             headers={ 'Accept': 'text/csv' },
                             timeout=86400)
    response_as_string = clear_response(response.text.split("\r\n"))
    return response_as_string


def handle_split_data(current_index, data):
    '''helper function for taking care of incorrectly split values. for instance, if value is [\\\\\\val, val\\\\\\], this function will return just [val, val]'''
    # if the value already in correct cell, simply clean it
    if any(c in [')', ']'] for c in data[current_index]):
        data[current_index] = re.sub('[^a-zA-Z0-9 \n\.]', '', data[current_index]) if len(data[current_index])>2 else data[current_index]
        return data
    # remove the quote to avoid early exit
    if '"' in data[current_index]:
        data[current_index] = data[current_index].replace('"', '')
    for x in range(current_index+1, len(data)):
        data[current_index] += ' ' + data[x]
        data[current_index] = data[current_index].replace('\'', '')
        # exit when closing char reached
        if any(c in [')', '"', ']'] for c in data[current_index]):
            if '"' in data[current_index]:
                data[current_index] = data[current_index].replace('"', '')
            #update the data to remove joined values
            data = [data[y] for y in range(0, len(data)) if (y<current_index+1 or y>x)]
            break
    return data

def same_diff_splitting(data, type):
    '''transforms the data structure of a dataset to make presentation on the UI easier.
       Applicable in get_difference.view so that grouping on details page is less computational.'''
    # TODO: check if there can be other agents
    entry= {}
    same_data = []
    diff_data = []
    for item in data:
        if len(item) > 2: # agents
            key=item[2] + " " + item[3]
            value=item[4]
        else: # activities
            key=item[0]
            value=item[1]
        same_data.append({key:value}) if int(value) > 1 else diff_data.append({key:value})

    entry["same"] = same_data
    entry["diff"] = diff_data

    return entry

def adjust_file_access_data(dataset, uuids, mode):
    '''transforms spo and uuidspo content for client'''
    data = []
    for uuid in uuids:
        entry = {'uuid': uuid}
        entry["files"] = {}
        for record in dataset:
            record = record.split(",")
            access_uuid = record[0].split(':')[-1]  # s/uuid
            if (uuid == access_uuid):
                if(mode == "spo"):
                    key = record[1].split('#').pop()  # p
                    value = record[2].split('/').pop()  # o
                    # keys and values might still look "unclear" (http://example.org/memory": "146.5625")
                    if 'http' in key:
                        key = key.split('/').pop()
                    if 'http' in value:
                        value = value.split('/').pop()

                    entry[key] = value
                if(mode == "uuidspo"):
                    entry["files"]["id"] = record[1].split('/').pop()  # s
                    key = record[2].split('/').pop()  # p
                    value = record[3].split('/').pop()  # o
                    entry["files"][key] = value

        data.append(entry)

    return data

def type_converter(data, string):
    '''converts value to int or string depending on user's need'''
    if string:
        result = ""
    else: result = 0

    for i in data:
        if string:
            result = str(i.split("/")[-1])
        else: result = int(i)

    return result

def compile_docstrings():
    '''creates a dictionary with function names and their docstrings for the recommendation module.
    If no docstring given, a placeholder is set.
    The data is collected from functionStore folder.'''
    function_store_path = BASE_DIR / 'radioastronomie/provnhandling/functionStore'

    files = os.listdir(function_store_path)
    functions = {}
    docstring_id= '"""' #this is how we know a docstring line started
    for file in files:
        key = file.replace("function", "").replace(".py", "")
        if os.path.isfile(os.path.join(function_store_path,file)) and not "__init__" in file:
            f=open(os.path.join(function_store_path,file),'r') #read file that exists into f
            count = 0
            value = ""
            for x in f:
                if count>0 and count<=2: # we only want first two lines of docstring
                    value = value + x
                    count = count +1
                if count>2:
                    break
                if docstring_id in x:
                    count = count + 1

            if count == 0: value="no documentation available"
            functions[key] = value.replace("/'","").replace('"""', '')
            f.close()

    return functions
