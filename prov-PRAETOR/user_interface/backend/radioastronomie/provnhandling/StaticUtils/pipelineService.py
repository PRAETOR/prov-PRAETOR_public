"""contains services that are relevant for lower-level operations, such as retrieval of entities for a specific function of a pipeline or consumption data for a given pipeline"""
from radioastronomie.provnhandling.StaticUtils import pipelineHelperFunctions
from radioastronomie.provnhandling.StaticUtils import recommendationHelperQueries
from radioastronomie.provnhandling.StaticUtils import pipelineHelperQueries
import re
import logging
import statistics

def get_pipelines():
    '''fetches all pipelines existing in the database'''
    response = pipelineHelperFunctions.query_handler(pipelineHelperQueries.get_pipelines())
    return response

def check_model(pipeline):
    praetor_relations = pipelineHelperFunctions.query_handler(pipelineHelperQueries.check_model(pipeline))[0]
    return "prov" if praetor_relations == "0" else "praetor"

def find_pipeline_name(pipelines, pipeline_name):
    '''find the full name of a pipeline'''
    pipeline_found = "404"

    for pipeline in pipelines:
        # remove http and time stamp
        tmp = (pipeline.replace('http://', '')).replace('_' + pipeline.split('_')[-1], '')

        if pipeline_name == tmp:
            pipeline_found = pipeline
            return pipeline_found

    return pipeline_found

# logic behind the recommendation module
def get_recommendation(pipeline):
    '''finds recommendations for next activity to extend a given pipeline with'''

    functions = pipelineHelperFunctions.compile_docstrings() #get function docstrings for usage later on

    antecedent = recommendationHelperQueries.find_antecedent(pipeline)# retrieve the latest activity in user pipeline
    logging.info("GET: recommendation for antecedent ", antecedent)
    edges = recommendationHelperQueries.count_edges(pipeline) # check number of edges (#startedBy relations?) excluding user pipeline
    logging.info("total edges in the DB ", edges)
    count_x = recommendationHelperQueries.count_antecedent(antecedent, pipeline) # check support(X) in entire db (#X/#edges)

    logging.info("count(x) ", count_x)
    if count_x > 0:
        support_x = (count_x/edges)
    else: support_x = 0
    logging.info("support(x) ", support_x)
    # go through all other pipelines and collect candidate_components (Y - startedBy -> X) including their counts which is #X->Y
    candidate_rules = recommendationHelperQueries.find_candidate_rules(antecedent, pipeline)

    result = []
    candidate_components = []
    result.append(antecedent)
    if len(candidate_rules) == 0: #do not proceed if no candidate rules found
        result.append("Not found")
        logging.info("candidate rules not found ")
        return result
    # calculate support & confidence for each candidate
    for c in candidate_rules:
        c = c.split(",") # c[0] = name, c[1] = count, c[2] = sources, c[3] = supportC, c[4] = conf
        c[0] = c[0].split("/")[-1]
        c[1] = int(c[1])

        count_c = recommendationHelperQueries.count_consequent(c[0], pipeline) # count(c)
        c.append(count_c/edges)          # support(c) = count(c)/#edges //append
        support_rule = c[1]/edges        # support(X->c) = count(X->Y)/#edges
        c.append(support_rule/support_x) # conf(X->c) = sup(X->c)/sup(X)
        candidate_components.append(c)

    # prune candidate_components by removing those that don't meet minconf
    conf = [c[4] for c in candidate_components]
    minconf = statistics.mean(conf)
    candidate_components = [c for c in candidate_components if c[4] >= minconf]

    # check lift
    for c in candidate_components:
        if c[3] == 0: # do not divide by zero
            lift = 0
        else:
            lift = c[4]/c[3] #lift(X->c) = conf/sup(c)
        if lift > 1: # only lift>1 interesting as it indicates complementarity (<1 is substitution)
            if str(c[0]) in functions:
                c.append(functions[str(c[0])])
            else:
                c.append("no documentation available") #sometimes functions are not in functionStore
            logging.info("recommendation found ", c[0])
            result.append(c)

    if len(result) == 1: result.append("Not found") #there is no candidate that meets the lift requirements
    return result

def get_agent_data(pipelines, order):
    '''retrieves agents of a given pipeline and their value information. Used by details page with tools/sw information'''
    response = pipelineHelperFunctions.query_handler(pipelineHelperQueries.get_agent_data(pipelines, order))

    data = []
    for string in response:
        entity = string.split(",")
        if len(entity) < 5:
            continue
        entity[0] = entity[0].split('#')[-1]  # ?s
        entity[1] = entity[1]  # ?s_count
        entity[2] = entity[2].split('/')[-1]  # ?p
        entity[3] = entity[3].split('/')[-1]  # ?o
        entity[4] = entity[4]  # ?o_count

        data.append(entity)

    return data

def get_activity_data(pipelines):
    '''retrieves activities, their type, memory, time and module data'''
    response = pipelineHelperFunctions.query_handler(pipelineHelperQueries.get_activity_data(pipelines))
    # example response: urn_uuid:bdsf_4acabe5d-957d-4db8-a22c-4f2a2273f8d9  "1"^^xsd:integer 	rdf:type  	u2p:bdsf   "1"^^xsd:integer

    # get detailed information of an activity, data array will be returned
    activity_list = []
    full_list = []
    for entity in response:
        entity = entity.split(",")
        activity_list.append(entity[0].split(':')[-1])
        activity = entity[0].split(':')[-1]
        predicate = entity[2].split('/')[-1]
        predicate = predicate.split('#')[-1]
        object_ = entity[3].split('/')[-1]
        full_list.append([activity, predicate, object_])

    act_dict = {}
    for x in full_list:
        key = x[0]
        value = [x[1], x[2]]
        if key not in act_dict:
            act_dict[key] = []
        act_dict[key].append(value)


    data = []
    for key, value in act_dict.items():
        list_item = {}
        list_item['uuid'] = key
        for x in value:
            list_item[x[0]] = x[1]
        data.append(list_item)
    print(data)
    for data_dict in data:
        try:
            data_dict['type'] = data_dict.pop('activityName')
            data_dict['memory'] = data_dict.pop('hadMemoryUsage')
        except:
            pass
    return data


def get_summary_on_pipeline(pipeline):
    '''retrieves data on entities and returns some basic information on pipeline: Date of last upload, number of files used (inputs), number of operations'''
    summary = {}
    # upload time
    upload_time = (pipeline.split('_')).pop()
    summary["uploadTime"] = upload_time

    # number of input files
    entities = get_entity_data(pipeline, "", "in")
    summary["countInEntities"] = len(entities["entities"])
    # number of operations
    entities = get_entity_data(pipeline, "", "out")
    summary["numberOfOperations"] = len({x["activity"] for x in entities["entities_value"]})

    return summary

def get_entity_data(pipeline, function, entity_type):
    '''retrieves entities and their relations (value, derivedFrom, time)'''
    model = check_model(pipeline)    #we needed datatypes at some point. the query still contains datatype info to speed up histogram generation for entity details page
    response = pipelineHelperFunctions.query_handler(pipelineHelperQueries.get_entity_data(pipeline, function, entity_type, model))
    entities_value = []
    entities_wasDerivedFrom = []
    for string in response:
        object = {}
        entity = string.replace('\\', '')
        entity = entity.split(",") # some data may already contain commas.
        # print(entity)
        object["activity"] = entity[0].split(':')[-1]  # ?a (activity)
        object["entity"] = entity[1].split('/')[-1]  # ?s (entity)
        object["type"] = entity[2].split('#')[-1]  # ?p (edge)
        if any(c in ['(', '"', '['] for c in entity[4]): #value doesn't need processing so skipping to role
            entity = pipelineHelperFunctions.handle_split_data(4, entity)
        if object["type"] == "value":
            object["datatype"] = entity[3].split('#')[-1] #?o (datatype)
            if any(c in ['(', '"', '['] for c in entity[4]):  # value doesn't need processing so skipping to role
                entity = pipelineHelperFunctions.handle_split_data(4, entity)
            # object["value"] = entity[4].split("/")[-1].split(":")[-1].split(".")[-1] if object["datatype"] == "string" else entity[4]
            object["value"] = entity[4]
        else:
            object["datatype"] = entity[3].split('/')[-1]
            # object["value"] = entity[4].split('/')[-1]
            object["value"] = entity[4]
        if any(c in ['(', '"', '['] for c in entity[5]): #value doesn't need processing so skipping to role
            entity = pipelineHelperFunctions.handle_split_data(5, entity)
        object["role"] = entity[5].split('/')[-1]  # ?r (role), times need no processing
        if entity_type == "in":
            object["startTime"] = entity[6]
            object["endTime"] = entity[7]

        if not "wasDerivedFrom" in entity[2]:
            entities_value.append(object)
        else:
            entities_wasDerivedFrom.append(object)

    if entity_type == "in":
        return {"entities": entities_value}
    else:
        return {"entities_value": entities_value, "entities_wasDerivedFrom": entities_wasDerivedFrom}

def get_file_access_data(pipeline):
    '''retrieves accessed files for given pipeline'''
    model = check_model(pipeline)
    response = pipelineHelperFunctions.query_handler(pipelineHelperQueries.get_file_access_data_1(pipeline)) #contains data on FA activities, time, agent

    #process activity data
    unique_file_access = []
    for entity in response:
        entity = entity.split(",")
        unique_file_access.append(entity[0].split(':')[-1])
    unique_file_access = set(unique_file_access)

    processed_uuid_data = pipelineHelperFunctions.adjust_file_access_data(response, unique_file_access, "spo")
    # get linked entity data
    response = pipelineHelperFunctions.query_handler(pipelineHelperQueries.get_file_access_data_2(pipeline, model)) # contains data on FA activities, entities and their vals

    #process entity data
    processed_entity_data = pipelineHelperFunctions.adjust_file_access_data(response, unique_file_access, "uuidspo")
    for record in processed_uuid_data:
       record["files"] = {key:value for var in processed_entity_data for (key,value) in var["files"].items() if var["uuid"] == record["uuid"]}

    return processed_uuid_data

def compare_pipeline_data(pipelines):
    '''a generic service to retrieve pipeline(s) data'''
    agentData = get_agent_data(pipelines, "ORDER BY desc(?s_count) desc(?o_count)")
    # get activity info (activity+count, nothing else)
    response = pipelineHelperFunctions.query_handler(pipelineHelperQueries.compare_pipeline_data(pipelines))

    activityData=[]
    for string in response:
        entity = string.split(",")
        entity[0] = (re.findall(r'\w+',entity[0]))[-1]  # ?s
        entity[1] = entity[1].split("/")[-1]
        entity[2] = int(entity[2])  # ?s_count
        activityData.append(entity)

    result = {"agents": agentData, "functions": activityData}
    return result

def get_activity_overview(pipeline):
    '''returns data on functions in the pipeline and ONLY roles of inEntities (+count) used by them.'''
    model = check_model(pipeline)
    response = pipelineHelperFunctions.query_handler(pipelineHelperQueries.get_activity_overview(pipeline, model))

    data = []
    for entry in response:
        entry = entry.split(",")
        entity = {'function': entry[0].split('/').pop()}# s
        entity["count"] = int(entry[1])  # count
        entity["consumed"] = [e.split("/")[-1] for e in entry[2].split(' ')]  # role
        data.append(entity)

    return data

def get_runtime(pipeline):
    '''retrieves start and end times for passed function.'''
    model = check_model(pipeline)
    response = pipelineHelperFunctions.query_handler(pipelineHelperQueries.get_runtime(pipeline, model))

    data = []
    for entry in response:
        entry = entry.split(",")
        data.append({"function": entry[0].split(":")[-1], "start": entry[1], "end": entry[2]})

    return data
