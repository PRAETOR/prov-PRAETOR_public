"""contains services that are relevant for lower-level operations, such as retrieval of entities for a specific function of a pipeline or consumption data for a given pipeline"""
PROV_DECLARATION = '''
        PREFIX prov: <http://www.w3.org/ns/prov#>
        PREFIX u2p: <http://uml2prov.org/>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
'''

PRAETOR_DECLARATION = '''
        PREFIX prov: <http://www.w3.org/ns/prov#>
        PREFIX u2p: <http://uml2prov.org/>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX run: <http://example.org/>
        PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
        PREFIX var: <http://openprovenance.org/var#>
        PREFIX prtr: <https://praetor.pages.mpcdf.de/praetor_provenance/>
'''

def createNamed(pipelines):
    named = " "
    if not (isinstance(pipelines, list)):
        named = ''' FROM NAMED <http://''' + pipelines + '''> '''
    else:
        for pipeline in pipelines:
            named += ''' FROM NAMED <http://''' + pipeline + '''> '''
    return named

def check_model(pipeline):
    named = createNamed(pipeline)
    querySelect= '''SELECT (count(?o) as ?o_count)'''
    queryGraph = ''' WHERE {
                    GRAPH ?g {
                        ?s prtr:activityName ?o.
                    }
                }'''
    return PRAETOR_DECLARATION + querySelect + named + queryGraph

def get_pipelines():
    '''fetches all pipelines existing in the database'''
    query = """
        SELECT distinct ?g
            WHERE {
                GRAPH ?g {
                    ?s ?p ?o
                }
            }ORDER BY desc(?g)"""
    return query

def get_agent_data(pipelines, order):
    '''retrieves agents of a given pipeline and their value information. Used by details page with tools/sw information'''
    named = createNamed(pipelines)

    top = PRAETOR_DECLARATION

    query = """
        SELECT distinct ?s (count(?s) as ?s_count) ?p ?o (count(?o) as ?o_count)""" + named + """
        WHERE {
            GRAPH ?g {
                ?s a prov:Agent ;
                    ?p ?o .
                filter(?o != prov:Agent && ?p != rdf:type)
            }
        } GROUP BY ?s ?p ?o
    """

    return top + query + order

def get_activity_data(pipelines):
    '''retrieves activities, their type, memory, time and module data'''
    named = createNamed(pipelines)

    top = PRAETOR_DECLARATION

    # write&sigma is removed temporarily
    query= '''
    SELECT distinct ?s (count(?s) as ?s_count) ?p ?o (count(?o) as ?o_count)''' + named + '''
    WHERE {
        GRAPH ?g {
                ?s ?p ?o ;
                    a prov:Activity .
                filter(?o != prov:Activity && !contains(str(?p), "qualified")
                && !contains(str(?p), "With") && !contains(str(?s), "pythonBuiltinFileAccess")
                && !contains(str(?s), "write")  && !contains(str(?s), "sigma")
                )
        }
    } GROUP BY ?s ?p ?o
    '''

    return top + query


def get_entity_data(pipeline, function, entity_type, model):
    '''retrieves entities and their relations (value, derivedFrom, time)'''
    #we needed datatypes at some point. the query still contains datatype info to speed up histogram generation for entity details page
    top = PRAETOR_DECLARATION
    selector = '''SELECT distinct  (?a as ?activity) (?s as ?entity) ?p (if(?p = prov:value, datatype(?datatype), "no data type") AS ?dt) (?datatype as ?value) (?r as ?role) (?t0 as ?startTime) (?t1 as ?endTime)'''
    # no role expected from inEntities
    if entity_type == "out":
        selector = selector.replace('(?r as ?role)', "") # TODO: maybe make this optional and keep in the output?

    querySelect = ''' FROM NAMED <http://''' + pipeline + '''>
            WHERE  {
                GRAPH ?g {'''

    if(entity_type == "in"):
        queryGraph = '''
                ?a prov:qualifiedUsage ?q ;
                    (prtr:activityName | rdf:type) ?v;
                    prov:startedAtTime ?t0;
                    prov:endedAtTime ?t1.
        		?q prov:entity ?s ;
        			prov:hadRole ?r .
        		?s ?p ?datatype .
                FILTER (?p=prov:value && !contains(str(?s), "pythonBuiltinFileAccess") && contains(str(?v), "''' + function + '''")) '''
    else:
        queryGraph = '''
                ?s ?p ?datatype ;
                  a	prov:Entity ;
                  prov:qualifiedGeneration ?q .
        		?q prov:activity ?a ;
              	    prov:hadRole ?r .
              	?a a ?v;
                    prov:startedAtTime ?t0;
                    prov:endedAtTime ?t1.
                FILTER ((?p = prov:value || ?p = prov:wasDerivedFrom)
                        && (!contains(str(?r), "None"))
                        && (!contains(str(?s), "pythonBuiltinFileAccess") && !contains(str(?datatype), "pythonBuiltinFileAccess"))
                        && contains(str(?v), "''' + function + '''")
                        )
        ''' if model == "prov" else '''
                  ?s ?p ?datatype ;
                      a	prov:Entity .
                  ?s prov:wasGeneratedBy ?a .
                  ?a prtr:activityName u2p:''' + function + ''' ;
                    prov:startedAtTime ?t0;
                    prov:endedAtTime ?t1.
                    FILTER ((?p = prov:value || ?p = prov:wasDerivedFrom)
                    && (!contains(str(?s), "pythonBuiltinFileAccess") && !contains(str(?datatype), "pythonBuiltinFileAccess"))
                    )
        '''

    ending = '''}} order by asc(?s)''' if entity_type == "in" else '''}} group by ?a ?s ?p ?datatype ?t0 ?t1
        order by asc(?s)
        '''
    return top + selector + querySelect + queryGraph + ending

def get_file_access_data_1(pipeline):
    '''retrieves accessed files for given pipeline'''
    top = PRAETOR_DECLARATION

    query = '''
        SELECT distinct ?s ?p ?o
        FROM NAMED <http://''' + pipeline + '''>
            WHERE {
                GRAPH ?g {
                     ?s (rdf:type | prtr:activityName) ?a;
                        ?p ?o ;
                     filter((?p = prov:startedAtTime || ?p = prov:endedAtTime || ?p = prov:wasAssociatedWith)
                        && (contains(str(?a), "pythonBuiltinFileAccess")))
                }
            } GROUP BY ?s ?p ?o
            ORDER BY desc(?s)
    '''
    return top + query

def get_file_access_data_2(pipeline, model):
    top = PROV_DECLARATION if model == "prov" else PRAETOR_DECLARATION
    #TODO: check if here we need to sub a with activityName
    query = '''
                SELECT distinct ?uuid ?s ?p ?o
                FROM NAMED <http://''' + pipeline + '''>
                    WHERE {
                        GRAPH ?g {
                             ?uuid prtr:activityName "pythonBuiltinFileAccess";
                                prov:qualifiedUsage ?genid .
                            ?genid prov:hadRole ?p;
                                            ?is ?s .
                            ?s a prov:Entity;
                                ?has ?o .
                            filter(?has !=rdf:type)
                        }
                    } GROUP BY ?uuid ?s ?p ?o
                    ORDER BY asc(?uuid) desc(?p) desc(?s)
        '''

    return top + query

def compare_pipeline_data(pipelines):
 # get activity info (activity+count, nothing else)
    top = PRAETOR_DECLARATION # includes parts from prov anyway
    named = createNamed(pipelines)
    query='''
    SELECT distinct ?s ?g (count(?s) as ?s_count) ''' + named + '''
        WHERE {
            GRAPH ?g {
                ?a a prov:Activity;
                   (prtr:activityName | rdf:type) ?s .
                filter(?s!= prov:Activity && !contains(str(?s), "pythonBuiltinFileAccess") && !contains(str(?s), "write")
                )
            }
        } GROUP BY ?s ?g
        ORDER BY desc(?s_count)
    '''
    return top + query

def get_activity_overview(pipeline, model):
    '''returns data on functions in the pipeline and ONLY roles of inEntities (+count) used by them.'''
    top = PROV_DECLARATION if model == "prov" else PRAETOR_DECLARATION

    query = '''
        SELECT distinct ?s (count(distinct ?uuid) as ?s_count) (group_concat(distinct ?r) as ?roles)
        FROM NAMED <http://''' + pipeline + '''>
        WHERE {
            GRAPH ?g {
                ?uuid a prov:Activity;
                    (prtr:activityName | rdf:type) ?s;
                    prov:qualifiedUsage ?q.
                ?q prov:entity ?o;	# used entities
                    prov:hadRole ?r. # and roles
                filter(?s != prov:Activity && !contains(str(?s), "pythonBuiltinFileAccess"))
                }
            } GROUP BY ?s
        order by asc(?s) desc(?s_count)
        '''

    return top + query

def get_runtime(pipeline, model):
    '''retrieves start and end times for passed function.'''
    top = PROV_DECLARATION if model == "prov" else PRAETOR_DECLARATION

    query = '''
           SELECT ?s ?ts ?te
           FROM NAMED <http://''' + pipeline + '''>
           WHERE {
            GRAPH ?g {
                ?s prov:startedAtTime ?ts ;
                      prov:endedAtTime ?te.
        }
    } order by asc(?ts)
    '''

    return top + query
