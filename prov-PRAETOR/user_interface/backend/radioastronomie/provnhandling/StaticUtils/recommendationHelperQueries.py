"""contains queries used by the recommendation module in pipelineService.py """
from radioastronomie.provnhandling.StaticUtils import pipelineHelperFunctions

def find_antecedent(pipeline):
    '''retrieves the last component from given pipeline. Last component is found by checking the activity timestamps in the graph.'''
    query="""PREFIX prov: <http://www.w3.org/ns/prov#>
            SELECT distinct ?s (?uuid as ?u)
            FROM NAMED <http://""" + pipeline + """>
            WHERE {
                GRAPH ?g { 
                    ?uuid prov:startedAtTime ?time;
                       	a ?s.
                    FILTER(!contains(str(?s), "write") && !contains(str(?s), "fileAccess") && !contains(str(?s), "read"))
                }
            }ORDER BY desc(?time)
            LIMIT 1"""

    x = pipelineHelperFunctions.query_handler(query) #user component, e.g. u2p:cmds --> strip from u2p:
    x = pipelineHelperFunctions.type_converter([x[0].split(",")[0]], True)
    return x

def count_edges(pipeline):
    '''counts number of startedBy and used-generated edges outside given pipeline'''
    query = """
            PREFIX prov: <http://www.w3.org/ns/prov#>
            SELECT (count(*) as ?s_count)
                WHERE {
                    GRAPH ?g { 
                        {
                            ?s prov:qualifiedStart ?o. # a1-[startedBy]-> a2
                        }
                        UNION {
                            ?y prov:qualifiedUsage ?q1. # y - [used] -> ent
                            ?q1 prov:entity ?ent .
                            ?ent prov:qualifiedGeneration ?q2 . # ent - [wasGeneratedBy] -> x
                        }
                    }
                FILTER(!contains(str(?g), '""" + pipeline + """'))
            }
        """
    edges = pipelineHelperFunctions.query_handler(query)
    edges = pipelineHelperFunctions.type_converter(edges, False)
    return edges

def count_antecedent(antecedent, pipeline):
    '''counts occurrences of X outside given pipeline'''
    query = """
           PREFIX prov: <http://www.w3.org/ns/prov#>
           SELECT (count(*) as ?s_count)
               WHERE {
                   GRAPH ?g { 
                    {
                        ?ent prov:qualifiedGeneration ?q . # ent - [wasGeneratedBy] -> x
                        ?q prov:activity ?a . 
                        ?a a ?s;
                        FILTER (contains(str(?s), '""" + antecedent + """')) 
                    }
                    UNION
                    {              			
                        ?uuid prov:qualifiedStart ?q. # y - [startedBy]-> x
                		?q prov:hadActivity ?a.
            			?a a ?s.
                        FILTER(contains(str(?s), '""" + antecedent + """'))
                    }
                   }
               FILTER(!contains(str(?g), '""" + pipeline + """'))
           }
       """
    count_x = pipelineHelperFunctions.query_handler(query)
    count_x = pipelineHelperFunctions.type_converter(count_x, False)
    return count_x

def find_candidate_rules(antecedent, pipeline):
    '''retrieves X->Y candidates with their occurrences and sources outside given pipeline'''
    query = """
            PREFIX prov: <http://www.w3.org/ns/prov#>
            SELECT distinct ?s (count(?s) as ?s_count) (group_concat(distinct ?g) as ?graphs)
            WHERE {
                GRAPH ?g {
                    { 
                        ?y prov:qualifiedUsage ?q1; # y - [used] -> ent
                            a ?s.
                        ?q1 prov:entity ?ent .
                        ?ent prov:qualifiedGeneration ?q2 . # ent - [wasGeneratedBy] -> x
                        ?q2 prov:activity ?a . 
                        ?a a ?user_val
                        FILTER (?s != prov:Activity && contains(str(?user_val), '""" + antecedent + """')) 
                    }
                    UNION
                    {
                        ?uuid prov:qualifiedStart ?q; # y - [startedBy]-> x
                			  a ?s.
                        ?q prov:hadActivity ?a.
                        ?a a ?user_val
                        FILTER(?s != prov:Activity && (contains(str(?user_val), '""" + antecedent + """')))
                    }
                }
                FILTER(!contains(str(?g), '""" + pipeline + """'))
            } GROUP BY ?s
            ORDER BY desc(?s_count)"""
    candidate_rules = pipelineHelperFunctions.query_handler(query)

    return candidate_rules

def count_consequent(consequent, pipeline):
    '''counts occurrences of Y outside given pipeline'''
    query = """
                PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                PREFIX prov: <http://www.w3.org/ns/prov#>
                SELECT (count(*) as ?s_count)
                WHERE {
                    GRAPH ?g { 
                        ?uuid ?p ?q; # y - [used] -> ent
                            	a ?s.
                        FILTER (contains(str(?s), '""" + consequent + """') 
                                && (?p = prov:qualifiedUsage || ?p = prov:qualifiedStart)) 
                    }
                    FILTER(!contains(str(?g), '""" + pipeline + """'))
                }
            """
    total_c = pipelineHelperFunctions.query_handler(query)
    total_c = pipelineHelperFunctions.type_converter(total_c, False)
    return total_c


