"""contains services that relate to graph creation and checking"""

import requests
import logging
from radioastronomie.provnhandling.config import SPARQL_REST_URL
from radioastronomie.provnhandling.StaticUtils import pipelineHelperFunctions


def does_named_graph_exist( pipeline ):
    '''checks if a named graph exists'''

    logging.debug('StaticUtils: checking if given named graph exists')
    select_named_graphs = pipelineHelperFunctions.query_handler("""
    SELECT DISTINCT ?g
    WHERE {
        GRAPH ?g { ?s ?p ?o }
    }""")

    return (pipeline in select_named_graphs)


def prepare_named_graph(filename):
    '''when uploading a file, the named graph should start with http:// and the file extension should be removed'''
    filename = filename.replace('.ttl', '')
    s = 'http://' + filename

    logging.debug("prepare_named_graph: s="+s)

    return s
