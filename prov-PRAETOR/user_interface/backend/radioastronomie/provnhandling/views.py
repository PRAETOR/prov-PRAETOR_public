from django.http import response
from rest_framework.parsers import FileUploadParser, MultiPartParser, FormParser
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.views import APIView
import requests
import logging
import os
from radioastronomie.provnhandling.StaticUtils import graphService
from radioastronomie.provnhandling.StaticUtils import pipelineService
from radioastronomie.provnhandling.config import SPARQL_REST_URL
from radioastronomie.settings import BASE_DIR

# django root dir
file_path = os.path.join(BASE_DIR)

# Create your views here.
@api_view(['GET', 'POST'])
def hello_world(request):
    if request.method == 'POST':
        return Response({"message": "Got some data!", "data": request.data})
    return Response({"message": "Hello, world!"})

@api_view(['GET'])
def get_pipeline_time(request):
    '''finds function's start and end times.'''
    if request.method != "GET":
        logging.error("Not a GET request")
        return Response(status=400)

    pipeline = request.query_params.get('pipeline')
    if 'http://' in pipeline:
        pipeline = pipeline.split('/').pop()

    response = pipelineService.get_runtime(pipeline)

    return Response(status=200, data=response)

@api_view(['GET'])
def get_functions(request):
    '''finds general data on functions in the pipeline'''
    if request.method != "GET":
        logging.error("Not a GET request")
        return Response(status=400)

    pipeline = request.query_params.get('pipeline')
    if 'http://' in pipeline:
        pipeline = pipeline.split('/').pop()

    response = pipelineService.get_activity_overview(pipeline)

    return Response(status=200, data=response)

@api_view(['GET'])
def get_recommendation(request):
    '''finds a recommendation for next activity based on other pipelines'''
    if request.method != "GET":
        logging.error("Not a GET request")
        return Response(status=400)

    pipeline = request.query_params.get('pipeline')
    if 'http://' in pipeline:
        pipeline = pipeline.split('/').pop()

    response = pipelineService.get_recommendation(pipeline)

    return Response(status=200, data=response)


@api_view(['GET'])
def get_accessed_files(request):
    '''finds data on file access operations'''
    if request.method != "GET":
        logging.error("Not a GET request")
        return Response(status=400)

    pipeline_name = request.query_params.get('pipeline').replace("http://", "")

    data = pipelineService.get_file_access_data(pipeline_name)

    return Response(status=200, data=data)

@api_view(['GET'])
def get_difference(request):
    '''finds same and different structural data in pipelines of interest.'''
    if request.method != "GET":
        logging.error("Not a GET request")
        return Response(status=400)

    pipelines = request.query_params.get('pipelines').replace('http://', "").split(" ")
    # query the db for required data
    response = pipelineService.compare_pipeline_data(pipelines)

    return Response(status=200, data=response)

@api_view(['GET'])
def get_pipelines(request):
    '''extracts pipelines. Called by the ListFiles class on the home view'''
    if request.method != "GET":
        logging.error("Not a GET request")
        return Response(status=400)

    arr = pipelineService.get_pipelines()

    # divide strings into display name and full name
    data = []
    for string in arr:
        fullName = string
        temp = string.split("_")
        if len(temp) > 1:
            displayName = ('_'.join(temp[0:-1])).replace('http://', '')
        else:
            displayName = (''.join(temp)).replace('http://', '')

        dict = {'displayName': displayName, 'fullName': fullName}
        data.append(dict)
    return Response({"data": data}) #why a dict?

@api_view(['GET'])
def get_pipeline_summary(request):
    '''extracts pipeline summary'''
    if request.method != "GET":
        logging.error("Not a GET request")
        return Response(status=400)

    pipeline_name = request.query_params.get('pipeline')
    if 'http://' in pipeline_name:
        pipeline_name = pipeline_name.split('/').pop()

    data = pipelineService.get_summary_on_pipeline(pipeline=pipeline_name)
    return Response({"data": data})

@api_view(['GET'])
def get_agent_of_pipeline(request):
    '''extracts data on agent name, tools used and their versioning data'''
    if request.method != "GET":
        logging.error("Not a GET request")
        return Response(status=400)

    logging.info("GET: get_agents_of_pipeline()")
    pipeline_name = request.query_params.get('pipeline')

    if 'http://' in pipeline_name:
        pipeline_name = pipeline_name.split('/').pop()

    data = pipelineService.get_agent_data(pipeline_name, " ORDER BY asc(?s) asc(?p)")

    return Response(status=200, data=data)

@api_view(['GET'])
def get_entities(request):
    '''extracts in/outEntities from a pipeline for given function.'''
    if request.method != "GET":
        logging.error("Not a GET request")
        return Response(status=400)

    pipeline_name = request.query_params.get('pipeline')
    if 'http://' in pipeline_name:
        pipeline_name = pipeline_name.split('/').pop()

    function = request.query_params.get('function')
    entity_type = request.query_params.get('type')

    if not (entity_type == "in" or entity_type == "out"):
        logging.error("The type parameter should be one of the following [in, out]")
        return Response(status=400)

    data = pipelineService.get_entity_data(pipeline_name, function, entity_type)

    return Response(status=200, data=data)

# current behavior: if filename already present, delete first occurrence
# note: the pipeline here is the filename (no timestamp)
@api_view(['DELETE'])
def delete_pipeline_by_name(request):
    '''deletes indicated pipeline'''
    if request.method != "DELETE":
        logging.error("Not a DELETE request")
        return Response(status=400)

    pipeline_name = request.query_params.get('pipeline')
    if 'http://' in pipeline_name:
        pipeline_name = pipeline_name.replace('http://', '')

    pipeline_name = pipeline_name.replace('_'+pipeline_name.split('_')[-1], '')

    arr = pipelineService.get_pipelines()
    graph_name = pipelineService.find_pipeline_name(arr, pipeline_name)

    if(graph_name == "404"):
        logging.warning("Pipeline was not found ... not deleted")
        return Response(status=400)

    response = requests.delete(SPARQL_REST_URL, params={'graph': graph_name}) #TODO:check the point of response
    logging.debug('---> graph_name: ' + graph_name)
    return Response(status=response.status_code)

@api_view(['GET'])
def get_pipeline_memory_consumption(request):
    if request.method != "GET":
        logging.error("Not a GET request")
        return Response(status=400)

    pipeline_name = request.query_params.get('pipeline')
    if 'http://' in pipeline_name:
        pipeline_name = pipeline_name.replace('http://', '')

    graph_name = 'http://' + pipeline_name

    if not (graphService.does_named_graph_exist( graph_name )):
        logging.warning("Pipeline was not found ...")
        return Response(status=400)

    # get all activities of given pipeline
    data = pipelineService.get_activity_data(pipeline_name)

    return Response(status=200, data=data)

class FileUploadView(APIView):
    parser_classes = [MultiPartParser, FormParser]

    # receives a .ttl file as multipart/form-data, which is then send to the database
    def post(self, request, format="application/x-turtle"):
        file_obj = request.data['file']
        append = request.data['append']

        if append == 'true':
            arr = pipelineService.get_pipelines()
            pipeline_to_append = ''

            logging.debug(file_obj.name)
            for a in arr:
                # remove http and time stamp
                tmp = (a.replace('http://', '')).replace('_'+a.split('_')[-1], '')
                logging.debug('tmp: ' + tmp)

                if file_obj.name.replace('_'+file_obj.name.split('_')[-1], '') == tmp:
                    pipeline_to_append = a
                    logging.debug('pipeline_to_append: '+ pipeline_to_append)
                    break
            graph_name = pipeline_to_append
        else:
            graph_name = graphService.prepare_named_graph(str(file_obj.name))

        #print(graph_name)
        logging.debug(graph_name)

        # creates named graph based on the filename
        url = SPARQL_REST_URL+'/data'

        response = requests.post(
            url,
            params={'graph': graph_name},
            files={'file': ( file_obj.name, file_obj, 'application/octet-stream' ) } # need to set the content-type here; otherwise fuseki crashes
        )

        return Response(status=response.status_code)
