from django.apps import AppConfig


class ProvnhandlingConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'provnhandling'
