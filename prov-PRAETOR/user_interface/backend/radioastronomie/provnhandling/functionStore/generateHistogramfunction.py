def generateHistogram(table,histogramName='histogram.png'):

    """computes the mathematical histogram that represents bins and the corresponding frequencies"""
    
    magnitudes = table['MAG_BEST'].tolist()
    plt.hist(magnitudes,bins='auto')
    plt.xlabel('Magnitude')
    plt.ylabel('Number')
    plt.title('Histogram of Object Magnitudes')
    plt.show()
    plt.savefig(histogramName)
    
    return histogramName
