def bdsf(imageName):#,sextractoryDir=sextractoryDir,cataloguename=catalogue):
    """
    decomposes radio interferometry images into sources and makes their properties available for further use.
    Can decompose an image into a set of Gaussians, shapelets, or wavelets as well as calculate spectral indices and polarization properties of sources and measure the psf variation across an image.
    """
    #img = bdsf.process_image(imageName,beam=(0.06, 0.02,13.3))
    #outTable = ascii.read('out_bdsf')
    os.chdir(sextractoryDir)
    bdsf_table = Table.read(catalogue,format='ascii.sextractor')
    return bdsf_table
