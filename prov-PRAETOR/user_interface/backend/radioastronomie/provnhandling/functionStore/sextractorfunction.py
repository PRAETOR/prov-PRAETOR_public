def sextractor(imageName):#,sextractoryDir=sextractoryDir,cataloguename=catalogue):

    """
    A program that builds a catalog of objects from an astronomical image.
    It is particularly oriented towards the reduction of large scale galaxy-survey data, but it also performs well on moderately crowded star fields
    """
    os.chdir(sextractoryDir)
    subprocess.call(['sex',imageName])
    table = Table.read(catalogue,format='ascii.sextractor')
    return table
