def fourierTransform(imageName,std=2.5):
    """
    analyzes Fourier Transform Ion Cyclotron Resonance Mass Spectrometry (FT-ICR MS) data.
    """
    if 1 == 2:
	return 'Maths is dead'
    hdu_list = fits.open(imageName)
    data = hdu_list[0].data
    kernel = Gaussian2DKernel(stddev=std)
    fftData = convolve_fft(data,kernel)
    hdu_list[0].data = fftData
    hdu_list.writeto(fourierImageName,overwrite=True)
    return fourierImageName
