# prov-PRAETOR - Frontend


## Installing the Docker

```
git clone https://gitlab.mpcdf.mpg.de/PRAETOR/prov-PRAETOR_public.git
```

```
cd prov-PRAETOR_public/prov-PRAETOR/user_interface/deploy
docker-compose build
docker-compose up
```

(Note docker-compose may need sudo privileges, e.g.:

```
cd prov-PRAETOR_public/prov-PRAETOR/user_interface/deploy
sudo docker-compose build
sudo docker-compose up
```

    In case you run into a problem by building the thing try (rm  ~/.docker/config.json).

This will start up 4 docker containers that exchange information. 

Note, requires root privilege & please note that the docker RAM should be greater than 5 GB (see docker settings).

## Running the Praetor Browser

- start the frontend (on [http://localhost:80](http://localhost:80))
- import provenance (prov) files via the user interface


## Additional Information

For completeness the additional 3 docker containers can be accessed via:

Prov converter [http://localhost:8090](http://localhost:8090)

Database [http://localhost:3030](http://localhost:3030) you need a password admin

Backend  [http://localhost:8001](http://localhost:8001)

## Example Usage

Screenshots of loading and example outputs from UI

![User_interface import](Upload_file.png "Here the file can be uploaded."){width=45%}

![User_interface open pipeline](Open_file.png "Here the more information about the uploaded pipelines can be found."){width=45%}

![Example Output: Time consumption per function over time](Time_consumption_per_function_over_time.png "Example Output: Time consumption per function over time."){width=45%}

![Example Output: Memory consumption over time](Memory_consumption_over_time.png "Example Output: Memory consumption over time."){width=45%}

![Example Output: Occurence in pipeline](Occurence_in_pipeline.png "Example Output: Occurence in pipeline."){width=45%}

Link to queries in SPARQL/Cypher that are analogous to those from th UI
