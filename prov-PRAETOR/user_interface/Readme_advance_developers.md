## Advance Information for developers - Available Scripts

This folder contains the frontend code of the project. This
part is built with [React](https://reactjs.org/) and further contains the
frameworks [Bootstrap](https://getbootstrap.com/) and
[ReactStrap](https://github.com/reactstrap/reactstrap).

- install npm

```console
sudo apt install npm
```

- install npm packages
```console
npm install
```

- start the frontend in developers mode (on [http://localhost:3000](http://localhost:3000))
```console
npm start
```

- start integration tests
```console
npm run cy:open (or cy:run)
```

## Overall architecture

![alt text](doc/swep.png "Architeture")

## Frontend Structure
### /public
- location [public](public)
- contains: 
    - [favicon.ico](public/favicon.ico) (Browser Icon in the Top Bar)
    - [index.html](public/index.html) (HTML Template file)
    - [manifest.json](public/manifest.json) (Description file of the application)
    - [robots.txt](public/robots.txt)

### /src
- location [src](src)
- contains: 
    - [App.css](src/App.css) (every CSS property for the project)    
    - [App.js](src/App.js) (definition of the header links (base structure for every page))    
    - [config.js](src/config.js) (Configuration file for routes to the other docker containers)
    - [index.js](src/index.js) (Entry point of the application (displays of the [App.js](src/App.js)))
    - [components](src/components):
        - contains smallest units of displayable HTML Content
            - [button](src/components/button.jsx) (Clickable buttons with icons and tooltip)
            - [activity-chain](src/components/activity-chain.component.jsx)  (Shows main and adjacent invocations)
            - [bidirectional-chart](src/components/bidirectional-chart.jsx)  (Compares two pipelines in their structure)
            - [clickable-header](src/components/clickable-table-header.component.jsx)  (Expandable table header)
            - [barchart](src/components/column-chart.jsx)  (Google column chart)
            - [pipeline-title](src/components/pipeline-title.component.jsx)  (Pipeline name and button to open its details page)
            - [histogram](src/components/plotly-histogram.component.jsx)  (Plotly histogram)
            - [range-slider](src/components/range-slider.component.jsx)  (Material UI range slider)
            - [sort-arrows](src/components/sort-arrows.component.jsx)  (SVG down/up arrows for sorting)
            - [stacked-barchart](src/components/stacked-bar-chart.component.jsx)  (Google stacked column chart)
            - [timeline](src/components/timeline.component.jsx)  (Pipeline start/end times with horizontal ruler)
            - [Loading.js](src/components/Loading.js) (Loading animation)
            - [summary](src/components/summary.jsx) (Show button that toggles to summary)
        - detail-table-* (components with details on pipelines)
            - [detail-table-entity](src/components/detail-table-entity.component.jsx) (In- and Out-Entities)
            - [detail-table-wrapper-entity](src/components/detail-table-wrapper-entity.component.jsx) (Combines tables and graphs on entity page)
            - [detail-table-graph](src/components/detail-table-entity.component.jsx) (Graph for time and memory usage per function)
            - [detail-table-statistics](src/components/detail-table-statistics.component.jsx) (Statistics table for memory and time usage per function)
            - [detail-table-SVG](src/components/detail-table-SVG.component.jsx) (SVG of the pipeline)
            - [detail-table-comparison](src/components/detail-table-comparison.component.jsx) (Tabular presentation of comparison between selected pipelines)
            - [detail-table-empty](src/components/detail-table-empty.component.jsx) (Replaces tabular data if empty)
            - [detail-table-file-access](src/components/detail-table-file-access.component.jsx) (Shows files accessed within pipeline)
            - [detail-table-function](src/components/detail-table-function.component.jsx) (Overview on distinct function types, their invocations and input parameters)
            - [detail-table-list-files](src/components/detail-table-list-files.component.jsx) (Handles interaction with pipeline list on home page)
            - [detail-table-recommendation](src/components/detail-table-recommendation.component.jsx) (Tabular presentation of data on activity recommendations)
        - higher level components 
            - [controls](src/components/controls.jsx) (Container for buttons on the home page when pipeline(-s) selected)
            - [header.component.js](src/components/header.component.js) (Navbar implementation)
            - [list-files.component.js](src/components/list-files.component.js) (Homepage definition (List of pipelines))
            - [upload-files.component.js](src/components/upload-files.component.js) (Upload area and file handling for the input files)
            - [yasgui.component.js](src/components/yasgui.component.js) (yasgui tab for direct SPARQL queries)
    - [services](src/services):
        - [upload-files.service.js](src/services/upload-files.service.js) (Backend communication for the uploaded files)
        - [visualization-helper](src/services/visualization-helper.service.js) (Reusable data transformation functions)
    - [sites](src/sites):
        - [comparison.js](src/sites/comparison.js) (HTML wrapper for the comparison page)
        - [details.js](src/sites/details.js) (HTML wrapper for the pipeline details view)
        - [entities.js](src/sites/entities.js) (HTML wrapper for function entities page)
        - [home.js](src/sites/home.js) (HTML wrapper for the home page)
        - [import.js](src/sites/import.js) (HTML wrapper for the import page)
        - [recommender.js](src/sites/recommender.js)(HTML wrapper for the recommendation page)
- [Dockerfile](Dockerfile) (Docker configuration for the frontend)
- [default.conf](default.conf) (nginx configurations)
