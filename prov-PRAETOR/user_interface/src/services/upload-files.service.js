import axios from "axios"
import { apiUrl } from "../config"

/**
 * Backend communication for the uploaded files.
 * The upload-files.component relies on this service for actual uplaod.
 */
class UploadFilesService {
	upload(flag, file, onUploadProgress) {
		let formData = new FormData()
		let append = Boolean(flag) // if 0 -> new named graph

		formData.append("file", file)
		formData.append("append", append)

		return axios.post(apiUrl.provnToolBox, formData, {
			headers: {
				"Content-Type": "multipart/form-data",
			},
			onUploadProgress,
		})
	}
}

export default new UploadFilesService()
