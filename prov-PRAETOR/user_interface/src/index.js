import ReactDOM from "react-dom"

import App from "./App"
import React from "react"
import reportWebVitals from "./reportWebVitals"

// Plotly throws calendar related errors on stacked histogram. there doesn't seem to be a way to set calendar to none/disable it.
console.trace = () => {}

ReactDOM.render(
	<React.StrictMode>
		<App />
	</React.StrictMode>,
	document.getElementById("root")
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
