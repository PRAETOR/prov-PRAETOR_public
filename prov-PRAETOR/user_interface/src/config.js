/**
 * Configuration file for running outside docker-container
 *
 */

const backendURL = "http://localhost:8000"
const databaseURL = "http://localhost:3030/ds"
const provnToolBoxURL = "http://localhost:8090/"

export const apiUrl = {
	getPipelines:                   backendURL + "/api/getPipelines",
	deletePipeline:                 backendURL + "/api/deletePipeline?pipeline=",
	getAgents:                      backendURL + "/api/getAgents?pipeline=",
	getRuntime:                     backendURL + "/api/getRuntime?pipeline=",
	getFunctions:                   backendURL + "/api/getFunctions?pipeline=",
	getEntities:                    backendURL + "/api/getEntities?pipeline=",
	getAccessedFiles:               backendURL + "/api/getAccessedFiles?pipeline=",
	getPipelineSummary:             backendURL + "/api/getPipelineSummary?pipeline=",
	getPipelineMemoryConsumption:   backendURL + "/api/getPipelineMemoryConsumption?pipeline=",
	getComparedData:                backendURL + "/api/compare?pipelines=",
	getRecommendation:              backendURL + "/api/recommend?pipeline=",
	getProvnFile:                   provnToolBoxURL + "files/",
	database:                       databaseURL,
	provnToolBox:                   provnToolBoxURL,
}
