import React, {Component} from "react"
import axios from "axios"
import {apiUrl} from "../config"
import Loading from "../components/Loading"
import DetailTableEmptyComponent from "../components/detail-table-empty.component"
import ListFiles from "../components/list-files.component"

/**
 * Presented to user by default. Provides an overview on the list of available pipelines.
 */
export default class Home extends Component {
	constructor(props) {
		super(props)

		this.state = {
			files: [],
			isLoading: true,
		}
		this.handleUpdatedFiles = this.handleUpdatedFiles.bind(this)
	}

	async fetchData() {
		return axios
			.get(apiUrl.getPipelines)
			.catch((err) => {
				console.error(err)
			})
	}
	componentDidMount() {
		this.fetchData().then(response => {
			this.setState({
				files: response.data.data,
				isLoading: false
			})
		})
	}

	componentWillUnmount() {
		this.setState({files: []})
	}

	/**
	 * method that updates files once child reports deletion
	 * @param filesFromComponent updated list of files
	 */
	handleUpdatedFiles(filesFromComponent) {
		this.setState({
			files: filesFromComponent,
		})
	}
	render() {
		let load = this.state.isLoading
		let isEmpty = this.state.files.length === 0
		return (
			(load || !this.state.files)
				? <Loading />
				: <div>
					<div className="row">
						<div className="col-md-3"></div>
						<div className="col-md-6 ">
							<p className="h3 mt-5 container">Available Pipelines</p>
							{isEmpty
								? <table className="table table-hover container ">
									<DetailTableEmptyComponent content=" uploaded pipelines" />
								</table>
								: <ListFiles
									isHome={true}
									parentFiles={this.state.files}
									updatedFiles={(updatedFiles)=>this.handleUpdatedFiles(updatedFiles)} //inform component if file deleted
								/>
							}
						</div>
						<div className="col-md-3"></div>
					</div>
				</div>
		)
	}
}
