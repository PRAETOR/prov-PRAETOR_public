import React from "react"
import UploadFiles from "../components/upload-files.component"

/**
 * Presented when user selects "Import". Provides an upload box to add provenance files.
 */
function UploadDownload() {
	return (
		<div className="container">
			<div className="row">
				<div className="col-md-1"></div>
				<div className="col-md-10">
					<UploadFiles />
				</div>
				<div className="col-md-1"></div>
			</div>
		</div>
	)
}

export default UploadDownload
