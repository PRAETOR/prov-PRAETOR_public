import React, { Component } from "react"
import axios from "axios"
import { apiUrl } from "../config"
import Loading from "../components/Loading"
import DetailTableComponent from "../components/detail-table.component"
import DetailTableStatisticsComponent from "../components/detail-table-statistics.component"
import DetailTableGraphComponent from "../components/detail-table-graph.component"
//import DetailTableSVGComponent from "../components/detail-table-SVG.component"
import DetailTableFileAccessComponent from "../components/detail-table-file-access.component"
import DetailTableFunctionComponent from "../components/detail-table-function.component"
import VisualizationHelper from "../services/visualization-helper.service"
import ErrorMessage from "../components/error"

/**
 * Presented when user opens selected pipeline.
 * User gets an overview on details, such as tools used, files accessed, functions used, and stats on time/memory consumption along with the prov SVG graph.
 */
export default class Details extends Component {
	constructor(props) {
		super(props)

		this.state = {
			details: {},
			inEntities: {},
			outEntities: {},
			memoryConsumption: {},
			//svgData: {},
			accessedFiles: {},
			pipeline: window.location.pathname.split("/").pop(),
			isLoading: true,
			selectedFunctions: [],
			hasError: false
		}

		this.handleSelection = this.handleSelection.bind(this)
	}

	getAgentDetails() {
		return axios({
			method: "get",
			url: apiUrl.getAgents + this.state.pipeline,
		})
	}

	getFunctions() {
		return axios({
			method: "get",
			url: apiUrl.getFunctions + this.state.pipeline,
		})
	}

	getPipelineMemoryConsumption() {
		return axios({
			method: "get",
			url: apiUrl.getPipelineMemoryConsumption + this.state.pipeline,
		})
	}

	getAccessedFiles() {
		return axios({
			method: "get",
			url: apiUrl.getAccessedFiles + this.state.pipeline,
		})
	}

	componentDidMount() {
		Promise.all([
			this.getAgentDetails(),
			this.getPipelineMemoryConsumption(),
			//this.getSvgData(),
			this.getAccessedFiles(),
			this.getFunctions()
		]).then((res) => {
			this.setState({
				details: res[0].data,
				memoryConsumption: res[1].data,
				//svgData: res[2],
				accessedFiles: res[2].data,
				functions: res[3].data,
				isLoading: false,
			})
		}).catch(() => {
			this.setState({
				isLoading: false,
				hasError: true
			})
		})
	}

	/**
	 * method that updates selected functions for component based on the data from child. Stats table and charts will reflect selection.
	 * @param selectedFunctionTypes list of functions selected by user (passed from child component)
	 */
	handleSelection(selectedFunctionTypes) {
		this.setState({selectedFunctions: selectedFunctionTypes})
	}

	render() {
		if (this.state.isLoading) {
			return <Loading />
		}else {
			let displayPipelineName = VisualizationHelper.removeTimestamp(this.state.pipeline)
			let timeStamp = this.state.pipeline.split("_").pop()
			let uploadDate = new Date(parseInt(timeStamp)).toUTCString()

			if(this.state.hasError) {
				return (
					<ErrorMessage title={"Pipeline: " + displayPipelineName}/>
				)
			}

			const processedData = VisualizationHelper.sortTimeMemoryForDetails(this.state.memoryConsumption)
			let selectedFunctions = this.state.selectedFunctions

			return (
				<div className="mt-4 container">
					<p className="h5 container mb-4">Pipeline: <strong>{displayPipelineName}</strong> uploaded on {uploadDate}</p>
					<div>
						{/* tools and libraries used */}
						<DetailTableComponent
							pipeline={this.state.pipeline}
							details={this.state.details}
							content="Packages/Tools/Software"
						/>
						{/* files accessed */}
						<DetailTableFileAccessComponent
							pipeline={this.state.pipeline}
							details={this.state.accessedFiles}
							content="Accessed Files"
						/>
						{/* function overview */}
						<DetailTableFunctionComponent
							pipeline={this.state.pipeline}
							content="Function Overview"
							details={this.state.functions}
							selectedFunctions={(selection)=>this.handleSelection(selection)} //if user clicks on function in this table, let the component know about it
						/>

						{/* consumption tables */}
						<DetailTableStatisticsComponent
							details={this.state.memoryConsumption}
							total={processedData.total_memory}
							attribute="memory"
							content="Function Memory Statistics"
							merged={processedData.merged}
							selected={selectedFunctions} //adjust background based on selection
							download={true}
						/>
						<DetailTableStatisticsComponent
							details={this.state.memoryConsumption}
							total={processedData.total_time}
							attribute="time"
							content="Function Time Statistics"
							merged={processedData.merged}
							selected={selectedFunctions}//adjust background based on selection
							download={true}
						/>
						{/* consumption charts */}
						<DetailTableGraphComponent
							sorted={processedData.sorted}
							merged={processedData.merged}
							selected={this.state.selectedFunctions}//adjust chart colors based on selection
						/>
						{/* pipeline's SVG image */}
						{/*<DetailTableSVGComponent data={this.state.svgData.data} />*/}
					</div>
				</div>
			)
		}
	}
}
