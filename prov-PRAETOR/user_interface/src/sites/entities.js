import React, {Component} from "react"
import axios from "axios"
import {apiUrl} from "../config"
import Loading from "../components/Loading"
import PipelineTitleComponent from "../components/pipeline-title.component"
import DetailTableWrapperEntityComponent from "../components/detail-table-wrapper-entity.component"
import DetailTableEmptyComponent from "../components/detail-table-empty.component"
import HistogramSliderComponent from "../components/histogram-slider.component"
import TimelineComponent from "../components/timeline.component"
import VisualizationHelper from "../services/visualization-helper.service"
import ErrorMessage from "../components/error"
import EntityActivityGraph from "../components/entity-activity-graph"

/***
 * Shows data for chosen function type.
 * Contains the histogram with sliders, pipeline with invocations highlighted, data on in/outEntities and their adjacent activities
 */
export default class Entities extends Component {
	constructor(props) {
		super(props)

		this.state = {
			inEntities: {},
			outEntities: {},
			function: window.location.pathname.split("/").pop(),
			pipeline: window.location.pathname.split("/")[2],
			isLoading: true,
			hasError: false,
			selection: [],
			runtime: {}
		}

		this.handleSelection = this.handleSelection.bind(this)
	}

	getInEntities() {
		return axios({
			method: "get",
			url: apiUrl.getEntities + this.state.pipeline + "&function=" + this.state.function + "&type=in",
		})
	}

	getOutEntities() {
		return axios({
			method: "get",
			url: apiUrl.getEntities + this.state.pipeline +  "&function=" + this.state.function + "&type=out",
		})
	}

	/**
	 * this request will give uuids, their start and endtimes, ordered in ascending way. Used in timeline.
	 * @returns {AxiosPromise}
	 */
	getPipelineRuntime(){
		return axios({
			method: "get",
			url: apiUrl.getRuntime + this.state.pipeline,
		})
	}
	componentDidMount(){
		Promise.all([
			this.getInEntities(),
			this.getOutEntities(),
			this.getPipelineRuntime()
		]).then(res => {
			this.setState({
				inEntities: res[0].data,
				outEntities: res[1].data,
				runtime: res[2].data,
				isLoading: false,
			})
		}).catch(() => {
			this.setState({
				isLoading: false,
				hasError: true
			})
		})
	}

	/**
	 * Function that takes care of updating documented selection received from Sliders (child). The update here will trigger update in values presented below/above sliders.
	 * @param selection
	 */
	handleSelection(selection) {
		if(selection === undefined) { return }

		let updatedParameters = this.state.selection
		const currentParameters = updatedParameters.map(i => i.parameter)

		if (currentParameters.includes(selection.parameter)){
			updatedParameters = updatedParameters.filter(f => f.parameter !== selection.parameter) //if selected slider is contained, it's value will be overwritten later
		}
		updatedParameters.push({"parameter": selection.parameter, "values": selection.values})

		this.setState({ selection: updatedParameters })
	}

	render() {
		if (this.state.isLoading) {
			return <Loading />
		} else {
			if(this.state.hasError) {
				return (
					<ErrorMessage title={"Entities for " + this.state.pipeline}/>
				)
			}

			const entities = this.state.inEntities
						
			const data = VisualizationHelper.reorganizeEntityData(this.state.inEntities, this.state.outEntities, this.state.selection) // data has the latest selection (based on filtering constraints)
			const histData = VisualizationHelper.numberSiever(this.state.inEntities)// default data for histogram (no filtering)
			const sliderData =  VisualizationHelper.rangeSiever(data)

			return (
				<div className="mt-4 container">
					<PipelineTitleComponent fileNames={[this.state.pipeline]} isPipeline={true} />
					<p className="h5 container mt-4">Function: <strong>{this.state.function}</strong></p>
					{/* histogram and sliders for all number function parameters */}
					{histData.length > 0
						?<HistogramSliderComponent data={histData} content="hist-charts" allSliders={this.state.selection} affectedBySelection={sliderData} userSelection={(selection)=> this.handleSelection(selection)} />
						//:<div className="container mt-4">
						//	<DetailTableEmptyComponent content="histograms available" />
						//</div>
						: <></>
					}
					{/* plot for number of times an activity used certain value */}
					<EntityActivityGraph entities={entities} />
					{data.length > 0
						? <>
							{/* timeline with invocations */}
							<TimelineComponent functionData={data} pipelineData={this.state.runtime} title="Occurrence in pipeline"/>
							{/* in/outEntities and activity-chains */}
							<DetailTableWrapperEntityComponent pipelineData={this.state.runtime} data={data} selection={this.state.selection} pipeline={this.state.pipeline}/>
						</>
						: <div className="container mt-4">
							<DetailTableEmptyComponent content="data on invocations available"/>
						</div>
					}
				</div>
			)
		}
	}
}
