import React, { Component } from "react"
import axios from "axios"
import { apiUrl } from "../config"
import Loading from "../components/Loading"
import method from "../images/method.png"
import ClickableTableHeader from "../components/clickable-table-header.component"
import ComparisonVisualizationHelper from "../services/visualization-helper.service"
import DetailTableRecommendationComponent from "../components/detail-table-recommendation.component"
import DetailTableEmptyComponent from "../components/detail-table-empty.component"
import PipelineTitleComponent from "../components/pipeline-title.component"
import ListFiles from "../components/list-files.component"
import ErrorMessage from "../components/error"

/**
 * Details for recommendation to extend the selected pipeline with.
 */
export default class Recommender extends Component{
	constructor(props){
		super(props)

		this.state = {
			data: [],
			pipeline: window.location.pathname.split("/").pop(),
			isLoading: true,
			hasError: false,
			showMethod: true,
			showSource: true,
			showSuggestion: true
		}
	}

	getRecommendation() {
		return axios({
			method: "get",
			url: apiUrl.getRecommendation + this.state.pipeline,
		})
	}

	componentDidMount(){
		Promise.all([this.getRecommendation()])
			.then(res => {
				this.setState({
					data: res[0].data,
					isLoading: false
				})
			}).catch(() => {
				this.setState({
					isLoading: false,
					hasError: true
				})
			})
	}

	/**
	 * method to extract pipelines used for recommended activity and pass it to ListFiles in required format
	 * @returns array of pipelines by fullName, displayName
	 */
	aggregatePipelines() {
		let result = []
		this.state.data.forEach((i,index) => {
			if(index === 0) {
				return
			}
			let pipelines = i[2].split(" ")
			pipelines.forEach(p => {
				//convert to desired format
				let obj = {fullName: p,	displayName: ComparisonVisualizationHelper.removeTimestamp(p).replace("http://","")}
				//check if already exists
				let isContained = result.filter(i => i.displayName === obj.displayName)
				if(isContained.length > 0) {
					return
				}
				//push only unique
				result.push(obj)

			})
		})
		return result
	}

	// methods to toggle state of headings
	handleSuggestionClick(status) {
		this.setState({
			showSuggestion: status
		})
	}

	handleSourceClick(status) {
		this.setState({
			showSource: status
		})
	}

	handleMethodClick(status) {
		this.setState({
			showMethod: status
		})
	}

	render(){
		if(this.state.isLoading){
			return (
				<Loading />
			)
		}else{
			if(this.state.hasError) {
				return (
					<ErrorMessage title={"Recommendation for " + this.state.pipeline}/>
				)
			}
						
			const fileName = [this.state.pipeline]
			let files = []
			const isEmpty = this.state.data[1].includes("Not found")
			if(!isEmpty) {
				files = this.aggregatePipelines()
			}
			return (
				<div className="mt-4 container recommender">
					<div className="recommender-table-header">
						{/* pipeline title*/}
						<PipelineTitleComponent fileNames={fileName} isPipeline={true} />
						{/* antecedent */}
						<div>
							<p className="h5 mt-4 container antecedent">Last component: <strong>{this.state.data[0]}</strong></p>
						</div>
					</div>
					{/* suggestion*/}
					<ClickableTableHeader content="Suggested next step" isOpen={(status)=>this.handleSuggestionClick(status)}/>
					{this.state.showSuggestion &&
						<div className="container">
							<table className="table table-hover ">
								{isEmpty
									? <DetailTableEmptyComponent content=" recommendation found"/>
									:<DetailTableRecommendationComponent details={this.state.data}/>}
							</table>
						</div>
					}
					{/* source pipelines*/}
					<ClickableTableHeader content="Source pipelines" isOpen={(status)=>this.handleSourceClick(status)}/>
					{this.state.showSource &&
						<div className=" container ">
							<table className="table table-hover ">
								{isEmpty
									? <DetailTableEmptyComponent content=" pipelines found"/>
									: <ListFiles parentFiles={files} isHome={false}/>}
							</table>
						</div>
					}
					{/* methodology */}
					<ClickableTableHeader content="Methodology" isOpen={(status)=>this.handleMethodClick(status)}/>
					{this.state.showMethod &&
						<div className= " container ">
							<p>Recommendations are derived using association rule mining according to < a href="https://www-users.cse.umn.edu/~kumar001/dmbook/index.php">Tan and colleagues</a> (Chapter 5). </p>
							<img src={method} alt="png image. icon by Icons8 at icons8.com" width="85%" />
						</div>
					}
				</div>
			)
		}
	}
}
