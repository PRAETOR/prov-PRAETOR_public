import React from "react"
import { Chart } from "react-google-charts"

/**
 * Stacked barchart (google)
 * @param props
 */
export default function StackedBarChart(props) {
	const data = props.data
	const title = props.title
	const y = props.y
	const content = props.content

	const optionsColumn = {
		hAxis: { title: y, position: "top", tickInterval: 5 },
		tooltip: { isHtml: true, trigger: "visible" },
		legend: { position: "bottom" },
		isStacked: true,
		bar: { groupWidth: "40%" }
	}

	return (
		<div className={"mt-4 " + content + " charts stacked-chart"}>
			<div className={" " + content + "-graph"}>
				<div className="row ">
					<p><strong>{title}</strong></p>
					<Chart
						chartType="BarChart"
						width="100%"
						height="500px"
						data={data}
						options={optionsColumn}
					/>
				</div>
			</div>
			<div className={"mt-4 "} ></div>
		</div>
	)
}
