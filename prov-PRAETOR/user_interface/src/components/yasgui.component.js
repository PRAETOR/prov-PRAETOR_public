import React, { useEffect } from "react"
import Yasgui from "@triply/yasgui"
import "@triply/yasgui/build/yasgui.min.css"
import { apiUrl } from "../config"

/**
 * Presented when user enters the "Yasgui" page. Provides space to query the database directly from the ui.
 * To query uploaded files: /db
 * To query other dbs, enter new endpoint, e.g. https://dbpedia.org/sparql
 *
 * See docs: https://triply.cc/docs/yasgui-api
 * @returns A gui to directly query the database.
 */
function YasguiSparql() {
	useEffect(() => {
		new Yasgui(document.getElementById("yasgui"), {
			requestConfig: { endpoint: apiUrl.database },
			copyEndpointOnNewTab: false,
		})
		return () => {}
	}, [])

	return (
		<div>
			<div id="yasgui" />
		</div>
	)
}

export default YasguiSparql
