import React from "react"
import Tooltip from "@mui/material/Tooltip"
import VisualizationHelper from "../services/visualization-helper.service"

/**
 * Changes data structure for easier plotting
 * @param functionData
 * @param pipelineData
 * @returns {[[string,string,{role: string}]]}
 */
function prepareData(functionData, pipelineData) {
	let dataColumn = [["function", "time", { role: "style" }]]	//data: [{"function_uuid", "start", "end"},...] and sorted
	const relevantInvocations = functionData.map(i => i.function) //we only need uuids to id them in entire set
	pipelineData.forEach(i => {
		dataColumn.push([i.function, new Date(i.end) - new Date(i.start), relevantInvocations.includes(i.function) ? "#007E33" : "#dcdcdc"])
	})

	return dataColumn
}
/**
 * Horizontal line that shows the invocations of interest and their location in the pipeline.
 * Displayed on entity details page.
 * @param props
 */
export default function TimelineComponent(props) {
	const functionData = props.functionData
	const pipelineData = props.pipelineData
	const title = props.title

	const dataLine = prepareData(functionData, pipelineData)
	const totalTime = dataLine.filter((i, index) => i[index] !== i[0]).reduce((prev, cur) => prev + cur[1], 0)

	return (
		<div className="mt-4 container " >
			<div className=" timeline mt-4">
				<p className="h5 ">{title}</p>
				<div className="row">
					{/* Horizontal line that represents a timeline */}
					<div className="customline-container mt-4">
						<div className="customline">
							{dataLine.filter((i, index) => i[index] !== i[0]).map((i, index) => {
								return (
									<Tooltip title={VisualizationHelper.removeTimestamp(i[0])} key={index}>
										<hr style={{
											height: "25px",
											width: i[1] * 100 / totalTime + "%",
											backgroundColor: i[2]
										}} />
									</Tooltip>
								)
							})}
						</div>
						<div className="tags">
							<p className="start-tag" >start</p>
							<p className="end-tag">end</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	)
}
