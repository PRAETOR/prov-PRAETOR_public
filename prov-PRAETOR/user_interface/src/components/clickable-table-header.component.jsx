import { useState, useEffect } from "react"
import Button from "./button"

export default function ClickableTableHeader(props) {
	const content = props.content
	const [isOpen, setIsOpen] = useState(true)

	/**
	 * hook to toggle expansion on click
	 */
	useEffect(() => {
		props.isOpen(isOpen)
	}, [isOpen])

	return (
		<div className="table-header mt-4" onClick={() => setIsOpen(!isOpen)}>
			<Button name="" icon={(isOpen ? "remove" : "add")} />
			<p className="h5 " >{content}</p>
		</div>
	)
}
