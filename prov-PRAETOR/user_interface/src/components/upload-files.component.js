import React, { Component } from "react"
import { apiUrl } from "../config"

import Dropzone from "react-dropzone"
import UploadService from "../services/upload-files.service"
import axios from "axios"
import Loading from "./Loading"

/**
 * Frontend methods to handle upload, deletion and duplication of files.
 * Used by the import page on the frontend and the urls on the backend.
 */
export default class UploadFiles extends Component {
	constructor(props) {
		super(props)
		this.upload = this.upload.bind(this)
		this.uploadFiles = this.uploadFiles.bind(this)
		this.onDrop = this.onDrop.bind(this)

		this.state = {
			selectedFiles: undefined,
			progressInfos: [],
			message: [],
			optionsEntryProgressInfos: [],
			uploadStatusFlag: false,
			disableButton: false,
		}
	}

	async getPipelines() {
		return await axios({
			method: "get",
			url: apiUrl.getPipelines,
		})
	}

	async deletePipeline(pipelineName) {
		return await axios.delete(apiUrl.deletePipeline + pipelineName)
	}

	/**
   * handle the upload of one specific file to the server
   * @param {Integer} idx
   * @param {File} file
   * @param uploadType
   * @param flag 1 append, if 0 just upload/ create new named graph
   */
	async upload(idx, file, uploadType, flag) {
		this.setState({uploadStatusFlag: true})
		this.setState({disableButton: true})
		let _progressInfos = uploadType === 0 ? [...this.state.progressInfos] : [...this.state.optionsEntryProgressInfos]

		await UploadService.upload(flag, file, (event) => {
			_progressInfos[idx].percentage = Math.round((100 * event.loaded) / event.total)

			this.setState({_progressInfos,	}) })
			.then(() => {
				this.setState((prev) => {
					let nextMessage = [
						...prev.message,
						"Uploaded the file " + file.name + " successfully",
					]
					return {
						message: nextMessage,
						uploadStatusFlag: false,
						disableButton: false,
					}
				})
			})
			.catch((error) => {
				_progressInfos[idx].percentage = 0
				this.setState((prev) => {
					let nextMessage
					switch (error.response.status) {
					case 422:
						nextMessage = [
							...prev.message,
							"Wrong file format of file: " + file.name,
						]
						break
					case 400:
						nextMessage = [
							...prev.message,
							"Could not parse the file: " + file.name + " (check its content)",
						]
						break
					case 409:
						nextMessage = [
							...prev.message,
							"File: " + file.name + " is empty",
						]
						break
					default:
						nextMessage = [
							...prev.message,
							"Could not upload the file: " + file.name,
						]
						break
					}
					if (uploadType === 0) {
						return {
							progressInfos: _progressInfos,
							message: nextMessage,
						}
					}
					return {
						optionsEntryProgressInfos: _progressInfos,
						message: nextMessage,
					}
				})
			})
		if (uploadType !== 0) {
			for (let i = 0; i < this.state.optionsEntryProgressInfos.length; i++) {
				if (
					this.state.optionsEntryProgressInfos[i].fileName === _progressInfos[0].fileName
				) {
					this.state.optionsEntryProgressInfos.splice(i, 1)
				}
			}
			this.setState({
				optionsEntryProgressInfos: this.state.optionsEntryProgressInfos,
			})
		}
	}

	cancel(file) {
		for (let i = 0; i < this.state.optionsEntryProgressInfos.length; i++) {
			if (this.state.optionsEntryProgressInfos[i].fileName === file.name) {
				this.state.optionsEntryProgressInfos.splice(i, 1)
			}
		}
		this.setState({
			optionsEntryProgressInfos: this.state.optionsEntryProgressInfos,
		})
	}

	replace(idx, file, uploadType) {
		let pipelineName = "http://" + file.name.split(".")[0]
		this.deletePipeline(pipelineName)
		// upload the file
		this.upload(idx, file, uploadType, 0)
	}

	/**
   * wrapper for the upload of all the selected files
   */
	async uploadFiles() {
		const selectedFiles = this.state.selectedFiles

		let _progressInfos = []
		let _optionsEntryProgressInfos = []
		let storedPipelines = await this.getPipelines()

		let displayFilenames = []
		storedPipelines.data.data.forEach((element) => {
			displayFilenames.push("http://" + element.displayName)
		})

		for (let i = 0; i < selectedFiles.length; i++) {
			let normalizedFilename = "http://" + selectedFiles[i].name.replace(/\.[^/.]+$/, "")
			let filenameWithMetaData = (
				normalizedFilename + "_" + Date.now() + "." + selectedFiles[i].name.split(".").pop()
			).replace("http://", "")

			selectedFiles[i] = new File([selectedFiles[i]], filenameWithMetaData, {
				type: "application/x-turtle",
			})

			if (displayFilenames.includes(normalizedFilename)) {
				// user has to decide how the duplicate is handled
				_optionsEntryProgressInfos.push({
					percentage: 0,
					fileName: selectedFiles[i].name,
					file: selectedFiles[i],
				})
			} else {
				// file will be uploaded normally
				_progressInfos.push({
					percentage: 0,
					fileName: selectedFiles[i].name,
					file: selectedFiles[i],
				})
			}
		}

		// upload the "normal" files
		this.setState(
			{
				progressInfos: _progressInfos,
				message: [],
				optionsEntryProgressInfos: _optionsEntryProgressInfos,
			},
			async () => {
				for (let i = 0; i < _progressInfos.length; i++) {
					await this.upload(i, _progressInfos[i].file, 0)
				}
			}
		)
	}

	/**
   * helper method to get the selected files from the Dropzone
   * @param {Array} files
   */

	onDrop(files) {
		if (files.length > 0) {
			this.setState({ selectedFiles: files })
		}
	}

	//showUploadStatus() {
	//	(<div className="alert alert-secondary d-flex justify-content-center" role="alert">
	//		<h6>Waiting for file uploading...</h6>
	//	</div>) && <Loading />
	//}

	render() {
		const { selectedFiles, progressInfos, message, optionsEntryProgressInfos } = this.state

		console.log(this.state.optionsEntryProgressInfos)

		return (
			<div>
				{progressInfos && progressInfos.map((progressInfo, index) => (
					<div className="mb-2" key={index}>
						<span>{progressInfo.fileName}</span>
						<div className="progress">
							<div
								className="progress-bar progress-bar-info"
								role="progressbar"
								aria-valuenow={progressInfo.percentage}
								aria-valuemin="0"
								aria-valuemax="100"
								style={{ width: progressInfo.percentage + "%" }}
							>
								{progressInfo.percentage}%
							</div>
						</div>
					</div>
				))}
				{
					optionsEntryProgressInfos && optionsEntryProgressInfos.map((progressInfos, index) => (
						<div className="col" key={index}>
							<div className="row alert alert-danger" role="alert">
								There is already a pipeline with the name &#34;{progressInfos.fileName}&#34; in the database.
							</div>
							<div className="row justify-content-md-center">
								<div className="row justify-content-md-center">
									<p className="h5 text-center">How to continue?</p>
									<hr style={{ width: 75 + "%" }}></hr>
								</div>
								<div className="col-md-auto mt-2">
									<button
										type="button"
										className="btn btn-primary"
										onClick={() => this.upload(index, progressInfos.file, 1, 0)}
									>
									Create new named Graph
									</button>
								</div>
								<div className="col-md-auto mt-2">
									<button
										type="button"
										className="btn btn-primary"
										onClick={() => this.upload(index, progressInfos.file, 1, 1)}
									>
									Append to existing pipeline
									</button>
								</div>
								<div className="col-md-auto mt-2">
									<button
										type="button"
										className="btn btn-primary"
										onClick={() => this.replace(index, progressInfos.file)}
									>
									Replace existing pipeline
									</button>
								</div>
								<div className="col-md-auto mt-2">
									<button
										type="button"
										className="btn btn-danger"
										onClick={() => this.cancel(progressInfos.file)}
									>
									Cancel
									</button>
								</div>
							</div>
						</div>
					))}
				<div className="my-3">
					<Dropzone onDrop={this.onDrop} accept=".provn, .xml, .ttl, .json">
						{({ getRootProps, getInputProps }) => (
							<section>
								<div {...getRootProps({ className: "dropzone" })}>
									<input {...getInputProps()} />
									{
										selectedFiles && Array.isArray(selectedFiles) && selectedFiles.length
											? (
												<div className="selected-file">
													{selectedFiles.length > 3
														? `${selectedFiles.length} files`
														: selectedFiles.map((file) => file.name).join(", ")}
												</div>
											) : (
												"Drag and drop [*.provn] files here, or click to select files"
											)}
								</div>
								<aside className="selected-file-wrapper">
									<button
										className= {this.state.uploadStatusFlag ? "btn btn-secondary " : "btn btn-success "}
										disabled={this.state.disableButton}
										onClick={this.uploadFiles}
									>
										Upload
									</button>
								</aside>
							</section>
						)}
					</Dropzone>
				</div>

				{/*{message.length > 0 && (
					<div className="alert alert-secondary" role="alert">
						<ul>
							{message.map((item, i) => {
								return <li key={i}>{item}</li>
							})}
						</ul>
					</div>
				)}*/}
				{/*{console.log(this.state)}*/}

				{message.length > 0 ? (
					<div className="alert alert-secondary" role="alert">
						<ul>
							{message.map((item, i) => {
								return <li key={i}>{item}</li>
							})}
						</ul>
					</div>
				) : this.state.uploadStatusFlag && <Loading />
				}
			</div>
		)
	}
}
