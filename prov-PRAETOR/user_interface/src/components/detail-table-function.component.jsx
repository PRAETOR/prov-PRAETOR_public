import React, { useEffect, useState } from "react"
import ClickableTableHeader from "./clickable-table-header.component"
import DetailTableEmptyComponent from "./detail-table-empty.component"
import PipelineTitleComponent from "./pipeline-title.component"
import SortArrowsComponent from "./sort-arrows.component"
import VisualizationHelper from "../services/visualization-helper.service"
import DownloadCsv from "./download-csv.component"

/**
 * Function list together with input parameters and invocations.
 */
export default function DetailTableFunctionComponent(props) {
	const details = props.details
	const content = props.content
	const pipeline = props.pipeline
	const isEmpty = (props.details.length <= 0)

	const [isOpen, setIsOpen] = useState(!isEmpty)
	const { items, requestSort } = VisualizationHelper.useSortableData(details) // sorted values
	const [selectedFunctions, setSelectedFunctions] = useState([]) //selected functions

	/**
	 * method that updates the functionset after a function was clicked.
	 * @param functionType function clicked
	 */
	function handleClickedFile(functionType) {
		const currentState = selectedFunctions

		setSelectedFunctions(
			currentState.includes(functionType)
				? selectedFunctions.filter(i => { return i !== functionType })
				: [...selectedFunctions, functionType]
		)
	}

	/**
	 * hook to update parent component once user selection changes
	 */
	useEffect(() => {
		props.selectedFunctions(selectedFunctions)
	}, [selectedFunctions])

	const csvHeaders = []
	const csvData = []

	csvHeaders.push(
		{ label: "Function", key: "Function" },
		{ label: "Invocations", key: "Invocations" },
		{ label: "Input", key: "Input" }
	)

	items.map(item => {
		csvData.push(
			{
				"Function": item["function"],
				"Invocations": item["count"],
				"Input": item["consumed"]
			}
		)
	})

	return (
		<div className="mt-4 function-overview ">
			<div className="d-flex justify-content-start">
				<ClickableTableHeader content={content} isOpen={(status) => setIsOpen(status)} />
				<DownloadCsv csvHeader={csvHeaders} csvData={csvData} filename={content} />
			</div>
			<div>
				{isOpen &&
					<div className="container">
						<table className="table table-hover">
							{isEmpty
								? <DetailTableEmptyComponent content={content} />
								: <>
									<thead>
										<tr>
											<th className=" col-md-2" onClick={() => requestSort("function")}>Function
												<SortArrowsComponent />
											</th>
											<th className="text-center col-md-2" onClick={() => requestSort("count")}>Invocations
												<SortArrowsComponent />
											</th>
											<th>Input</th>
										</tr>
									</thead>
									<tbody className={content.split(" ").join("_").toLowerCase()}>
										{isEmpty
											? <DetailTableEmptyComponent content="functions" />
											: items.map((item, index) => {
												return (
													<tr key={index} onClick={() => handleClickedFile(item.function)} className={selectedFunctions.includes(item.function) ? " selected " : ""}> {/* if selected, change background color */}
														<td>
															<PipelineTitleComponent isPipeline={false} fileNames={[item.function]} pipeline={pipeline} />
														</td>
														<td className="text-center">{item.count}</td>
														<td>{Object.values(item.consumed).map((i, index, array) => {
															return (
																index === array.length - 1
																	? i
																	: i + ", "
															)
														})
														}</td>
													</tr>
												)
											})
										}
									</tbody>
								</>
							}
						</table>
					</div>
				}
			</div>
		</div>
	)
}