import React, { useState } from "react"
import DetailTableEmptyComponent from "./detail-table-empty.component"
import ClickableTableHeader from "./clickable-table-header.component"
import DownloadCsv from "./download-csv.component"

/**
 * In- and outEntities with their derivation relations presented on details view.
 */
export default function DetailTableComparisonComponent(props) {
	const details = props.details
	const content = props.content
	const isEmpty = (props.details.length <= 0)
	const [isOpen, setIsOpen] = useState(!isEmpty)

	const csvHeaders = []
	const csvData = []
	const headers = []

	headers.push(content.includes("tool") ? "Tool" : "Function")
	headers.push(content.includes("tools") ? "Details" : "")
	headers.push(content.includes("Repeating") ? "Invocations" : "")

	headers.map(head => {
		if (head !== "") {
			csvHeaders.push({ label: head, key: head })
		}
	})

	details.map(item => {
		const rowData = {}
		if (content.includes("tools")) {
			rowData[headers[0]] = item[0]
			rowData[headers[1]] = item[2]
		}
		else {
			rowData[headers[0]] = item[0]
		}
		if (content.includes("Repeating")) {
			rowData[headers[2]] = item[1]
		}
		csvData.push(rowData)

	})

	return (
		<div className="mt-4 comparison">
			<div className="d-flex justify-content-start">
				<ClickableTableHeader content={content} isOpen={(status) => setIsOpen(status)} />
				{!isEmpty && <DownloadCsv csvHeaders={csvHeaders} csvData={csvData} filename={content} />}
			</div>
			<div>
				{isOpen &&
					<div className="container">
						<table className="table table-hover ">
							{isEmpty
								? <DetailTableEmptyComponent content={content} />
								: <>
									<thead>
										<tr>
											<th>{content.includes("tool") ? "Tool" : "Function"}</th>
											{content.includes("tools") //tools have details also mentioned
												? <th>Details</th>
												: <></>
											}
											{content.includes("Repeating") //if repeating, show count
												? <th className="text-center">Invocations</th>
												: <></>
											}
										</tr>
									</thead>
									<tbody className={content.split(" ").join("-").toLowerCase()}>
										{details.map((item, index) => {
											return (
												<tr key={index}>
													{content.includes("tools")
														? <>
															<td>{item[0]}</td> {/* identifier */}
															<td>{item[2].replace(/[A-Za-z_]/g, "")}</td>{/* details */}
														</>
														: <td>{item[0]}</td> //just identifier for functions
													}
													{content.includes("Repeating")
														? <td className="text-center">{item[1]}</td> //invocations for repeating functions
														: <></>
													}
												</tr>
											)
										})
										}
									</tbody>
								</>
							}
						</table>
					</div>
				}
			</div>
		</div>
	)
}
