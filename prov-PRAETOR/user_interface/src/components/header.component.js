import { Component, React } from "react"

/**
 * Navigation bar on top of the main page.
 */
export default class Header extends Component {
	render() {
		return (
			<nav className="navbar navbar-expand-sm navbar-light bg-light">
				<a className="navbar-brand ml-4" href="/">
					{" "}Home{" "}
				</a>
				<button
					className="navbar-toggler"
					type="button"
					data-toggle="collapse"
					data-target="#navbarNav"
					aria-controls="navbarNav"
					aria-expanded="false"
					aria-label="Toggle navigation"
				>
					<span className="navbar-toggler-icon"></span>
				</button>
				<div className="collapse navbar-collapse" id="navbarNav">
					<ul className="navbar-nav">
						<li className="nav-item">
							<a className="nav-link" href="/import">
								Import
							</a>
						</li>
					</ul>
					<ul className="navbar-nav">
						<li className="nav-item">
							<a className="nav-link" href="/yasgui">
								Yasgui
							</a>
						</li>
					</ul>
				</div>
			</nav>
		)
	}
}
