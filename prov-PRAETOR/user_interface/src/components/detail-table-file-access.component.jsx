import React, { useState } from "react"
import ClickableTableHeader from "./clickable-table-header.component"
import DetailTableEmptyComponent from "./detail-table-empty.component"
import DownloadCsv from "./download-csv.component"

/**
 * File access table presented on details view.
 */
export default function DetailTableFileAccessComponent(props) {
	const details = props.details
	const content = props.content
	const isEmpty = (details.length <= 0)
	const [isOpen, setIsOpen] = useState(!isEmpty)

	const csvHeaders = []
	const csvData = []

	csvHeaders.push(
		{ label: "Identifier", key: "Identifier" },
		{ label: "Type", key: "Type" },
		{ label: "File Size", key: "File Size" },
		{ label: "Hash Code", key: "Hash Code" },
		{ label: "Accessed By", key: "Accessed By" },
		{ label: "Mode", key: "Mode" }
	)

	details.map(item => {
		csvData.push(
			{
				"Identifier": item.files.id,
				"Type": "." + item.files.fileName.split(".")[1],
				"File Size": "",
				"Hash Code": "",
				"Accessed By": "",
				"Mode": item.files.mode == "w" ? "write" : "read"
			}
		)
	})

	return (
		<div className="mt-4 file-access">
			<div className="d-flex justify-content-start">
				<ClickableTableHeader content={content} isOpen={(status) => setIsOpen(status)} />
				{!isEmpty && <DownloadCsv csvHeaders={csvHeaders} csvData={csvData} filename={content} />}
			</div>
			{isOpen &&
				<div className="container">
					<table className="table table-hover ">
						{isEmpty
							? <DetailTableEmptyComponent content={content} />
							: <>
								<thead>
									<tr>
										<th>Identifier</th>
										<th>Type</th> {/* file format */}
										<th>File Size</th> {/* placeholder */}
										<th>Hash Code</th> {/* placeholder */}
										<th>Accessed By</th> {/* placeholder */}
										<th>Mode</th> {/* read/write/write+ */}
									</tr>
								</thead>
								<tbody>
									{
										details.map((entry, index) => {
											return (
												<tr key={index}>
													<td>{entry.files.id}</td>
													<td>{"." + entry.files.fileName.split(".")[1]}</td>
													<td></td>
													<td></td>
													<td></td>
													<td>{entry.files.mode == "w" ? "write" : "read"}</td>
												</tr>
											)
										})
									}
								</tbody>
							</>
						}
					</table>
				</div>
			}
		</div>
	)
}
