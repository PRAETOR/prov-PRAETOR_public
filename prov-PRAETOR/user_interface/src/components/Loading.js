import React from "react"

/**
 * Loading animation in case response is not available.
 */
class Loading extends React.Component {
	render() {
		return (
			<div style={{ position: "absolute", top: "50%", left: "50%" }}>
				<div className="dot-fire"></div>
			</div>
		)
	}
}

export default Loading
