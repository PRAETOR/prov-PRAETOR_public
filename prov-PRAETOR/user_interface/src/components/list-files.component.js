import React, { Component } from "react"
import Controls from "./controls"
import ListFilesComponent from "./detail-table-list-files.component"

/**
 * Presents passed pipeline data as a list (for home and recommendation pages)
 */
export default class ListFiles extends Component {
	constructor(props) {
		super(props)

		this.state = {
			files: props.parentFiles,
			selectedFiles: [],
			isShowingSummary: false,
			isClickedAway: false,
			isHome: props.isHome,
		}

		this.resetControls = this.resetControls.bind(this)
		this.wrapperRef = React.createRef()
		this.handleClickOutside = this.handleClickOutside.bind(this)
		this.showSummary = this.showSummary.bind(this)
		this.handleFiles = this.handleFiles.bind(this)
		this.handleSelection = this.handleSelection.bind(this)
	}

	componentDidMount() {
		document.addEventListener("mousedown", this.handleClickOutside)
	}

	componentWillUnmount() {
		this.resetControls()
		this.setState({
			files: [],
		})
		document.removeEventListener("mousedown", this.handleClickOutside)
	}

	/**
	* from: https://stackoverflow.com/questions/32553158/detect-click-outside-react-component
    * Alert if clicked on outside of element.  Resets controls and data linked to it
    */
	handleClickOutside(event) {
		if (this.wrapperRef && !this.wrapperRef.current.contains(event.target)) {
			this.setState({isClickedAway: true})
			this.resetControls()
		}
	}

	/**
	 * method to reset the state after delete or clickaway happened
	 */
	resetControls(){
		this.setState({
			isShowingSummary: "",
			selectedFiles: [],
			isClickedAway: false
		})
	}

	/**
	 * method that updates the fileset after delete was clicked.
	 * @param filesFromControls files returned from the child component
	 */
	handleFiles(filesFromControls) {
		this.props.updatedFiles(filesFromControls)
		this.setState({
			files: filesFromControls,
		})
		this.resetControls()
	}

	/**
	 * method that updates selected files for component based on the data from child.
	 * @param selectedFiles list of pipelines selected by user passed from child component
	 */
	handleSelection(selectedFiles) {
		this.setState({selectedFiles: selectedFiles})
	}
	/**
	 * method that toggles the status of summary in state
	 * @param summaryClicked boolean returned from the child component
	 */
	showSummary(summaryClicked){
		this.setState({isShowingSummary: !summaryClicked})
	}

	render() {
		const files = this.state.files
		let selectedFiles = this.state.selectedFiles

		return (
			<div className="container">
				<table className="table table-hover list-files " ref={this.wrapperRef}>
					<thead>
						<tr >
							<th key="pipeline_name">Pipeline</th>
							<th key="Controls" className="controls">
								{selectedFiles.length>0 &&
								<Controls
									fileNames={selectedFiles}
									fileData={(fileData)=>this.handleFiles(fileData)}
									showSummary={(summaryClicked)=>this.showSummary(summaryClicked)}
									isHomePage={this.state.isHome}
								/>}
							</th>
						</tr>
					</thead>
					{files.length>0 &&<ListFilesComponent
						files={files}
						isShowingSummary={this.state.isShowingSummary}
						selectedFiles={(selection)=>this.handleSelection(selection)}
						isClickedAway={this.state.isClickedAway}
					/>}
				</table>
			</div>
		)
	}
}
