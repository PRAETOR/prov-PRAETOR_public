import axios from "axios"
import { Component } from "react"
import { apiUrl } from "../config"

/**
 * Displays pipeline summary on the home page.
 */
export default class Summary extends Component {
	constructor(props) {
		super(props)

		this.state = {
			summary: {},
			summary_as_string: "",
			pipeline: props.pipeline
		}

		this.formatSummaryAsString = this.formatSummaryAsString.bind(this)
	}

	formatSummaryAsString(obj){
		let str = ""

		Object.keys(obj).map(function(key){
			let val = obj[key]
			if(key === "uploadTime"){
				val = new Date(parseInt(val)).toUTCString()
			}
			str += key +": " + val + "_"
		})
		str = str.slice(0, -1) // remove last '_'

		return str
	}
  
	componentDidMount() {
		let pipeline = this.state.pipeline.map(i=> i = i.replace("https://", ""))

		axios.get(apiUrl.getPipelineSummary + pipeline)
			.then(({ data }) => {
				this.setState({ summary: data.data, summary_as_string: this.formatSummaryAsString(data.data)})
			})
			.catch((err) => { console.error(err) })
	}
  
	render(){
		return(
			<div className="summary-list">
				<div>
					<ul>
						{this.state.summary_as_string.split("_").map(val => {
							return <li key={val}>{val.replace(/([A-Z])/g, " $1").replace(/^./, function(str){return str.toUpperCase()})}</li>
						})}
					</ul>
				</div>

			</div>
		)
	}
  
}
