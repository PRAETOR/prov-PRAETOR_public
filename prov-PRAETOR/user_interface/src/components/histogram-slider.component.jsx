import React, {useState, useEffect} from "react"
import RangeSliderComponent from "./range-slider.component"
import PlotlyHistogramComponent from "./plotly-histogram.component"

export default function HistogramSliderComponent(props) {
	const data = props.data
	const content = props.content
	const allSliders = props.allSliders
	const affectedBySelection = props.affectedBySelection //data that fits into selected range on sliders
	const [selectedByUser, setSelectedByUser] = useState([data]) //slider range values

	/**
	 * hook that sends data on selected range to parent.
	 */
	useEffect(()=>{
		let prunedInput
		Array.isArray(selectedByUser)
			? prunedInput = selectedByUser.forEach(i => i !== Array.isArray(i)) //no complex arrays needed, just selected value
			: prunedInput = selectedByUser
		props.userSelection(prunedInput) //send data to parent
	},[selectedByUser])


	return (
		<div className={"table-header mt-4 mb-4 "+content}  >
			{data.map((i, index) => {
				//for histogram to be able to set colors properly:
				let selected = allSliders.filter(item => item.parameter === i.parameter).map(i => i.values).reduce((prev, cur) => prev.concat(cur), [])
				let effectiveLimits = affectedBySelection.filter(item => item.parameter === i.parameter).map(i => i.values).reduce((prev, cur) => prev.concat(cur), []).sort((a,b) => a - b)

				return (
					<div key={index}>
						{/* parameter histogram */}
						<PlotlyHistogramComponent key={index+ "_"+i.parameter} data={i.inputs} param={i} selected={selected} affected={i.parameter === selectedByUser.parameter ? [] : [effectiveLimits[0],effectiveLimits[effectiveLimits.length-1]]} title={i.parameter} content="hist-params"  />
						{/* slider under histogram */}
						<div className="container slider">
							<RangeSliderComponent key={index+ "-"+i.parameter} range={i.inputs} param={i.parameter} selectedVals={(selection)=>setSelectedByUser(selection)}/>
						</div>
					</div>
				)
			})
			}
		</div>
	)
}
