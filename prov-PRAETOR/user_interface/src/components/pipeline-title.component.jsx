import Button from "./button"
import React from "react"
import { useNavigate } from "react-router-dom"
import VisualizationHelper from "../services/visualization-helper.service"

/**
 * Component to show pipeline title or function name with open button to move to detail/entity pages
 * @param props
 * @returns {JSX.Element}
 * @constructor
 */
export default function PipelineTitleComponent(props){
	const titles = props.fileNames
	const isPipeline = props.isPipeline //can also be a function
	let navigate = useNavigate()

	/**
	 * method to switch to details or entity pages
	 */
	function detailsHandler(file) {
		isPipeline
			? navigate("/details/"+file) //if pipeline was clicked
			: navigate("/entities/"+ props.pipeline + "/" + titles[0]) //if function was clicked
	}

	return(
		<div className="pipeline-title container">
			{isPipeline
				?<p className="h5 ">{titles.length > 1 ? "Pipelines:" : "Pipeline:" }</p>
				: <></>}
			{
				titles.map((i, index) => {
					return(
						<div className="pipeline-name-button" key={index}>
							<div>
								{isPipeline
									?<p className="h5 ">{VisualizationHelper.removeTimestamp(i)}</p>
									:<p>{i}</p>
								}
							</div>
							<div className="details" onClick={()=>detailsHandler(i)}>
								<Button icon="open_in_new" name="open" />
							</div>
							{
								index === titles.length - 1
									? <></>
									: <div><p>, </p></div> //if more than one item is listed
							}
						</div>
					)
				})
			}
		</div>
	)
}
