import React, { useRef } from "react"
import { CSVLink } from "react-csv"
import VisualizationHelper from "../services/visualization-helper.service"
import Button from "./button"

/**
 * Download table data as csv file
 * @param props->data,headers,filename
 * returns csv file
 */
export default function DownloadCsv(props) {
	const headers = props.csvHeaders
	const data = props.csvData
	const filename = VisualizationHelper.removeTimestamp(props.filename).replaceAll(" ", "_")

	const csvLink = useRef()

	const csvReport = {
		data: data,
		headers: headers,
		filename: `${filename}.csv`,
		ref: csvLink,
		target: "_blank",
		className: "hidden"
	}

	const onClickCsvButton = () => {
		csvLink.current.link.click()
	}

	return (
		<div className="ms-4 d-flex flex-row-reverse">
			<div className="mt-4" onClick={onClickCsvButton}>
				<Button icon="download" name="download csv" />
			</div>
			<CSVLink {...csvReport} ></CSVLink>
		</div>
	)
}  
