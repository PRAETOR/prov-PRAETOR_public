import React from "react"
import { Chart } from "react-google-charts"
import VisualizationHelper from "../services/visualization-helper.service"

/**
 * Creates the barchart for function time consumption (aggregated). Changes color if selection includes a function
 * @param data contains data as an array
 * @param y value for y-axis name
 * @param x value for x-axis name
 * @param selectedData functions selected by user in table
 */
function createGraphDataColumnChart(data, x, y, selectedData) {
	let res = []
	res.push([x, y, { role: "style" }])

	if (y.includes("time")) {
		for (let i = 0; i < data.length; ++i) {
			let color = "#007E33" //default color
			//change default color if selection is not empty and color palette has enough colors
			if (selectedData?.includes(data[i].type) && VisualizationHelper.getColor(selectedData?.indexOf(data[i].type)) !== -1) {
				color = VisualizationHelper.getColor(selectedData.indexOf(data[i].type))
			}

			res.push([data[i].type, data[i][y.split(" ")[0]], color])
		}
	} else {
		for (let i = 0; i < data.length; ++i) {
			res.push([data[i][0], Number(data[i][1]), "#007E33"])
		}
	}

	return res
}


/**
 * Barchart for time and memory usage per function
 * @param props
 */
export default function ColumnChart(props) {
	const data = props.data //default data
	const selected = props.selected //functions selected in table
	const title = props.title //chart title
	const y = props.y //y-axis title
	const x = props.x //x-axis title
	const content = props.content //for integration tests
	const stackedBool = props.isStacked //if stacked, change the chart's mode

	let dataColumn = []
	stackedBool
		? dataColumn = data //no need to process data if stacked
		: dataColumn = createGraphDataColumnChart(data, x, y, selected)

	const optionsColumn = {
		title: title,
		hAxis: { title: x },
		vAxis: { title: y, tickInterval: 1 },
		tooltip: { isHtml: true, trigger: "visible" },
		isStacked: stackedBool,
		legend: { position: stackedBool ? "bottom" : "none" },
	}

	return (
		<div className={"mt-4 " + content + " charts"}>
			<div className={" " + content + "-graph"}>
				<div className="row ">
					<Chart
						chartType="ColumnChart"
						width="100%"
						height="500px"
						data={dataColumn}
						options={optionsColumn}
					/>
				</div>
			</div>
			<div className={"mt-4 "} ></div>
		</div>
	)
}
