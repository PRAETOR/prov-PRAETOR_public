module.exports = {
	env: {
		"cypress/globals": true,
	},
	extends: ["plugin:cypress/recommended"],
	parserOptions: {
		ecmaFeatures: {
			jsx: true,
		},
		ecmaVersion: "latest",
		sourceType: "module",
	},
	plugins: ["cypress"],
	rules: {
		"cypress/no-assigning-return-values": "error",
		"cypress/no-unnecessary-waiting": "error",
		"cypress/assertion-before-screenshot": "warn",
		"cypress/no-force": "warn",
		"cypress/no-async-tests": "error",
		"cypress/no-pause": "error",
	},
}
