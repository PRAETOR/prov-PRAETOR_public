describe("checks rendering of elements and presence of expected data for home", () => {
	beforeEach(() => {
		cy.visit("/")
	})

	//start page should have 2 test files: testfile and testfile2
	it("check list-files, pipeline controls, and pipeline summary", () => {
		cy.contains("testfile")
		cy.get("table > tbody > tr").should("have.length", 2) //2 files -> 2 trs in tbody

		//check correctness of output
		//click on pipeline and make sure the controls are visible
		cy.get("table > tbody > tr").first().click() //click on first file and make sure the controls are shown
		cy.get(".pipeline-controls").should("exist")
		cy.get(".pipeline-controls > div").should("have.length", 4) //when 1 file clicked, the basic set of controls is summary, details, recommendation, and delete

		//check if summary expand works
		cy.get(".summary > div").click()
		cy.contains("Count In Entities: 2").should("exist")
		cy.get(".table li")
			.should("have.length", 3)
			.last()
			.should("have.text", "Number Of Operations: 1")

		//check if summary collapse works
		cy.get(".summary > div").click()
		cy.contains("Count In Entities: 2").should("not.exist")

		//check if controls disapper correctly on click
		cy.get("table > tbody > tr").get(".pipeline-name").first().click()
		cy.get(".pipeline-controls").should("not.exist")

		//deletion excluded as automated upload is cumbersome for now

		//check compare
		cy.get("table > tbody > tr").get(".pipeline-name").first().click()
		cy.get("table > tbody > tr").get(".pipeline-name").last().click()
		cy.get(".pipeline-controls").should("exist")
		cy.get(".pipeline-controls > div").should("have.length", 1) //when >1 file clicked, only compare shown

		cy.get(".compare").should("exist")
		//now close all
		cy.get("table > tbody > tr").get(".pipeline-name").first().click()
		cy.get("table > tbody > tr").get(".pipeline-name").last().click()
		cy.get(".pipeline-controls").should("not.exist")
	})
})
