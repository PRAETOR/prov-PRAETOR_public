/* eslint-disable cypress/no-unnecessary-waiting */
describe("checks rendering of elements and presence of expected data for details", () => {
	beforeEach(() => {
		cy.visit("/")
		cy.wait(3000)
	})

	//get summary output
	it("check rendering of pipelineDetails", () => {
		cy.contains("testfile").click()
		cy.get(".details > div").click()

		cy.wait(7000)
		cy.url().should("include", "details/testfile")

		//check tools
		cy.get(".sw").should("contain", "Tools/Software").as("Tools")
		cy.get("@Tools")
			.find(".table tr")
			.should("have.length", 3)
			.last()
			.should("contain.text", "python")

		//check accessed files
		cy.get(".file-access").should("contain", "Accessed Files").as("Files")
		cy.get("@Files")
			.find(".table > tbody > tr")
			.should("have.length", 2)
			.should("contain", "pythonBuiltinFileAccess_inEntity_0_2")
		cy.get("@Files")
			.find(".table > tbody > tr")
			.should("have.length", 2)
			.should("contain", "pythonBuiltinFileAccess_inEntity_0_1")

		//check function overview
		cy.get(".function-overview")
			.should("contain", "Function Overview")
			.as("fo")
		cy.get("@fo")
			.find(".function_overview tr")
			.should("have.length", 1)
			.find("td")
			.first().should("contain", "generateHistogram")

		cy.get("@fo")
			.find(".function_overview tr td")
			.last().should("contain", "table, histogramName")

		cy.get("@fo")
			.find(".function_overview tr td")
			.eq(1).should("contain", "1").and("contain", "1")


		//check function memory
		cy.get(".memory")
			.should("contain", "Function Memory Statistics")
			.as("memory")
		cy.get("@memory")
			.find(".table tr")
			.should("have.length", 2)
			.last()
			.should("contain", "generateHistogram")
			.should("contain", "155.80")

		//check function time
		cy.get(".time").should("contain", "Function Time Statistics").as("time")
		cy.get("@time")
			.find(".table tr")
			.should("have.length", 2)
			.last()
			.should("contain", "generateHistogram")
			.should("contain", "216.00")

		cy.get(".time-memory-graph").should("exist")

		//check svg presence
		//cy.get(".svg").should("exist")
	})
})
