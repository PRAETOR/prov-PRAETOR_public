describe("checks status response and size of returned data", () => {
	beforeEach(() => {
		cy.visit("/")
	})

	it("checks agents", () => {
		cy.request("GET", Cypress.env("getAgents")+Cypress.env("fileName1")).should((response) => {
			expect(response.status).to.eq(200)

			expect(response.body)
				.to.not.be.oneOf([null, ""])
			expect(response.body[1])
				.contain("lifeline")
			expect(response.body[1])
				.contain("python")
			expect(response.body[1])
				.contain("2.7.13ContinuumAnalyticsInc")
			expect(response.body).to.be.length(2)
			expect(response.body[0])
				.contain("lifeline")
			expect(response.body[0])
				.contain("matplotlib")
			expect(response.body[0])
				.contain("2.2.2")
		})
	})

	it("checks in entities", () => {
		cy.request("GET", Cypress.env("getEntities")+Cypress.env("fileName1")+"&function=generateHistogram&type=in").should((response) => {
			expect(response.status).to.eq(200)

			expect(response.body).property("entities").to.not.be.oneOf([null, ""])
			expect(response.body).property("entities").to.be.length(2)
		})
	})

	it("checks out entities", () => {
		cy.request("GET", Cypress.env("getEntities")+Cypress.env("fileName1")+"&function=generateHistogram&type=out").should((response) => {
			expect(response.status).to.eq(200)

			expect(response.body)
				.property("entities_value")
				.to.not.be.oneOf([null, ""])
			expect(response.body).property("entities_value").to.be.length(1)

			expect(response.body)
				.property("entities_wasDerivedFrom")
				.to.not.be.oneOf([null, ""])
			expect(response.body).property("entities_wasDerivedFrom").to.be.length(2)
		})
	})

	it("checks consumption", () => {
		cy.request("GET", Cypress.env("getConsumption")+Cypress.env("fileName1")).should((response) => {
			expect(response.status).to.eq(200)
		})
	})

	//it("checks svg", () => {
	//	cy.request("GET", Cypress.env("getSVG")+Cypress.env("fileName1")+".svg").should((response) => {
	//		expect(response.status).to.eq(200)
	//	})
	//})

	it("checks accessed files", () => {
		cy.request("GET", Cypress.env("getAccessedFiles")+Cypress.env("fileName1")).should((response) => {
			expect(response.status).to.eq(200)

			expect(response.body).to.not.be.oneOf([null, ""])
			expect(response.body).to.be.length(2)

			expect(response.body[0].files.id).to.be.eq("pythonBuiltinFileAccess_inEntity_0_2")
			expect(response.body[0].files.fileName).to.be.eq("sextractor_hist.png")
			expect(response.body[0].files.mode).to.be.eq("w")

			// alt format:expect(Array(response.body)[0][1]["files"]["id"]).to.be.eq("pythonBuiltinFileAccess_inEntity_0_1")
			expect(response.body[1].files.id).to.be.eq("pythonBuiltinFileAccess_inEntity_0_1")
			expect(response.body[1].files.fileName).to.be.eq("genhist.txt")
			expect(response.body[1].files.mode).to.be.eq("w")
		})
	})

	it("deletes test file", ()=> {
		cy.request("DELETE", "http://localhost:8000/api/deletePipeline?pipeline="+Cypress.env("fileName1")).should((response) => {
			expect(response.status).to.eq(204)
		})
		cy.request("DELETE", "http://localhost:8000/api/deletePipeline?pipeline="+Cypress.env("fileName2")).should((response) => {
			expect(response.status).to.eq(204)
		})
	})

	after(()=> {
		cy.get(".home-pipeline-list").should("have.length", 1)
	})
})
