describe("checks status response and size of returned data", () => {
	beforeEach(() => {
		cy.visit("/")
	})

	it("checks recs for sextractor", () => {
		cy.request("GET", Cypress.env("getRecommendation")+Cypress.env("testfile_sextractor")).should((response) => {
			expect(response.status).to.eq(200)

			expect(response.body).to.not.be.oneOf([null, ""])

			expect(response.body).to.be.length(2)
			//check antecedent
			expect(response.body[0]).contain("sextractor")
			//check consequent
			expect(response.body[1]).contain("fourierTransform")
			//check source pipelines
			expect(response.body[1][2]).contain("case-file7")
			expect(response.body[1][2]).contain("case-file1")
			expect(response.body[1][2]).contain("case-file6")
		})
	})

	it("checks recs for ft", () => {
		cy.request("GET", Cypress.env("getRecommendation")+Cypress.env("testfile_ft")).should((response) => {
			expect(response.status).to.eq(200)

			expect(response.body).to.not.be.oneOf([null, ""])

			expect(response.body).to.be.length(2)
			//check antecedent
			expect(response.body[0]).contain("fourierTransform")
			//check consequent
			expect(response.body[1]).contain("sigma_clipped_stats")
			//check source pipelines
			expect(response.body[1][2]).contain("case-file6")
			expect(response.body[1][2]).contain("case-file7")
			expect(response.body[1][2]).contain("case-file1")
			expect(response.body[1][2]).contain("case-file2")
		})
	})

	it("checks recs for load", () => {
		cy.request("GET", Cypress.env("getRecommendation")+Cypress.env("testfile_load")).should((response) => {
			expect(response.status).to.eq(200)

			expect(response.body).to.not.be.oneOf([null, ""])

			expect(response.body).to.be.length(2)
			//check antecedent
			expect(response.body[0]).contain("load")
			//check consequent
			expect(response.body[1]).contain("sextractor")
			//check source pipelines
			expect(response.body[1][2]).contain("case-file7")
			expect(response.body[1][2]).contain("case-file6")
			expect(response.body[1][2]).contain("case-file3")
			//check docstring
			expect(response.body[1][5]).not.eq("no documentation available")
		})
	})

})
