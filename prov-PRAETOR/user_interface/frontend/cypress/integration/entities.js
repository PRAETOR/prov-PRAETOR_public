/* eslint-disable cypress/no-unnecessary-waiting */
describe("checks rendering of elements and presence of expected data for details", () => {
	beforeEach(() => {
		cy.visit("/")
		cy.wait(3000)
	})

	//get summary output
	it("check rendering of function entities", () => {
		cy.contains("testfile").click()
		cy.get(".details > div").click()

		cy.wait(3000)
		cy.url().should("include", "details/testfile")

		//check function overview
		cy.get(".function-overview")
			.should("contain", "Function Overview")
			.as("fo")
		cy.get("@fo")
			.find(".details span").click()

		cy.wait(3000)
		cy.url().should("include", "entities/testfile")

		cy.get(".pipeline-title").should("contain", "testfile")

		cy.get(".instance").should("contain", "generateHistogram")

		//check entities in
		cy.get(".In-Entities").should("contain", "In-Entities").as("inEnt")
		cy.get("@inEnt")
			.find(".table tr")
			.should("have.length", 3)
			.last()
			.should("contain", "histogramName") //checks role

		//check role/parameter
		cy.get("@inEnt")
			.find(".table > tbody > tr").first().should("contain.text", "table")
		cy.get("@inEnt")
			.find(".table > tbody > tr").last().should("contain.text", "histogramName")

		//check entities out
		cy.get(".Out-Entities").should("contain", "Out-Entities").as("outEnt")
		cy.get("@outEnt")
			.find(".table tr")
			.should("have.length", 2)
			.last()
			.should("contain", "outEntity3")
	})
})
