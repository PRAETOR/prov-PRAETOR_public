describe("checks rendering of elements and presence of expected data for compare", () => {
	beforeEach(() => {
		cy.visit("/")
	})

	//start page should have 2 test files: testfile and testfile2
	it("checks comparison", () => {
		cy.get("table > tbody > tr").should("have.length", 2) //2 files -> 2 trs in tbody

		//check compare
		cy.get("table > tbody > tr").get(".pipeline-name").first().click()
		cy.get("table > tbody > tr").get(".pipeline-name").last().click()
		cy.get(".pipeline-controls").should("exist")
		cy.get(".pipeline-controls > div").should("have.length", 1) //when >1 file clicked, only compare shown

		cy.get(".compare > div").click() //should go to compare page
		// eslint-disable-next-line cypress/no-unnecessary-waiting
		cy.wait(3000)

		//check title
		cy.get(".pipeline-name-button >div > p").first().should("contain.text", "testfile")
		cy.get(".pipeline-name-button >div > p").last().should("contain.text", "testfile2")

		//check rep tools
		//check tools
		//THIS will only work for >2 pipelines
		// cy.get(".repeating-tools").should("exist").as("RT")
		// cy.get("@RT")
		// 	.find("tr")
		// 	.first().should("contain.text", "matplotlib")
		// cy.get("@RT")
		// 	.find("tr")
		// 	.last().should("contain.text", "python")
		// //check rep functions
		// cy.get(".repeating-functions").should("exist").as("RA")
		// cy.get("@RA")
		// 	.find("tr")
		// 	.first().should("contain.text", "fileAccess")
		// cy.get("@RA")
		// 	.find("tr")
		// 	.last().should("contain.text", "generateHistogram")
		// //check unique tools
		// cy.get(".unique-tools").should("not.exist")
		// //check unique functions
		// cy.get(".unique-functions").should("exist").as("UA")
		// cy.get("@UA")
		// 	.find("tr").should("contain.text", "fourierTransform")
		cy.get(".bidirectional").should("exist").as("B")
		cy.get("@B")
			.find("thead tr th")
			.first().should("contain", "testfile")
		cy.get("@B")
			.find("thead tr th")
			.last().should("contain", "testfile2")
		cy.get("@B")
			.find("tbody tr")
			.last().should("contain", "fourierTransform")
		cy.get("@B")
			.find("tbody tr")
			.first().should("contain", "generateHistogram")
	})
})
