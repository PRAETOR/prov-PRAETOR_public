describe("checks rendering of elements and presence of expected data for recommendation", () => {
	beforeEach(() => {
		cy.visit("/")
	})

	//start page should have 10 test files: 3 testfiles and 7 case-base files
	it("checks presence of testfiles and case-base", () => {
		cy.contains("testfile_sextractor").should("exist")
		cy.contains("testfile_ft").should("exist")
		cy.contains("testfile_load").should("exist")

		cy.get(".home-pipeline-list > tr").should("have.length", 10)

		//check correctness of output
		//click on pipeline and make sure the controls are visible
		cy.get(".home-pipeline-list > tr").get(".pipeline-name").first().click() //click on first file and make sure the controls are shown
		cy.get(".pipeline-controls").should("exist")
		cy.get(".pipeline-controls > div").should("have.length", 4) //when 1 file clicked, the basic set of controls is summary, details and delete

		//check if controls disappear correctly on click
		cy.get("table > tbody > tr").get(".pipeline-name").first().click()
		cy.get(".pipeline-controls").should("not.exist")

		//deletion excluded as automated upload is cumbersome for now
	})

	it("checks recs for sextractor", ()=>{
		cy.contains("testfile_sextractor").click()
		cy.get(".suggest > div").click({force: true})
		cy.url().should("include", "suggest/testfile_sextractor") //move to recommendation page successful
		//check pipeline
		cy.get(".pipeline-name-button >div > p").first().should("contain.text", "testfile_sextractor")
		//check last component
		cy.get(".antecedent").should("contain.text", "sextractor")
		//check recommendation
		cy.get(".recommendations > tbody > tr > td").first().should("contain.text", "fourierTransform")
		cy.get(".list-files > tbody > tr").should("have.length", 3) //rec based on 3 pipelines
	})

	it("checks recs for ft", ()=>{
		cy.contains("testfile_ft").click()
		cy.get(".suggest > div").click({force: true})
		cy.url().should("include", "suggest/testfile_ft") //move to recommendation page successful
		//check pipeline
		cy.get(".pipeline-name-button >div > p").first().should("contain.text", "testfile_ft")
		//check last component
		cy.get(".antecedent").should("contain.text", "fourierTransform")
		//check recommendation
		cy.get(".recommendations > tbody > tr > td").first().should("contain.text", "sigma_clipped_stats")
		cy.get(".list-files > tbody > tr").should("have.length", 4) //rec based on 4 pipelines
	})

	it("checks recs for load", ()=>{
		cy.contains("testfile_load").click()
		cy.get(".suggest > div").click({force: true})
		cy.url().should("include", "suggest/testfile_load") //move to recommendation page successful
		//check pipeline
		cy.get(".pipeline-name-button >div > p").first().should("contain.text", "testfile_load")
		//check last component
		cy.get(".antecedent").should("contain.text", "load")
		//check recommendation
		cy.get(".recommendations > tbody > tr > td").first().should("contain.text", "sextractor")
		cy.get(".list-files > tbody > tr").should("have.length", 3) //rec based on 3 pipelines
	})
})
