# SWEP-Radioastronomie Cypress Tests

Integration tests are organized into two files:
- [home.js](./integration/home.js) checks for how elements and data are rendered on the home page
- [details.js](./integration/details.js) checks for how elements and data are rendered on the details page
- [entities.js](./integration/entities.js) checks the output on the entity page
- [comparison.js](./integration/comparison.js) checks for how elements and data are rendered on the compare page
- [requests-details-delete.js](./integration/requests-details-delete.js) checks whether responses to axios requests contains expected form/data for delete and details controls
- [suggest.js](./integration/suggest.js) checks for how elements and data are rendered on the recommendation page
- [requests-suggest.js](./integration/requests-suggest.js) checks whether responses to axios requests contains expected form/data for recommendations

It is recommended to run the tests in same order as presented above since requests-details-delete deletes some testfiles which may have an impact on the remaining tests, if present. 
## Scripts
To start the tests in the console:

```console
npm run cy:run --spec "cypress/integration/*.js"
```

For tests in browser:
```console
npm run cy:open
```

## Prerequisites
Due to the upload of files not happening directly to GraphDB but through the ProvnToolBox, before running the integration tests
0. start the docker containers like indicated in the main [README](../../README.md) file
1. setup the `TestDB` repository 
2. make sure [testfile](../../provn_data/testfile.provn) and [testfile2](../../provn_data/testfile2.provn) are uploaded for the first 4 tests
3. make sure [all files](../../provn_data/files_for_recom_test/) for recommendation feature are uploaded
4. adjust the file namings for `fileName_*` and `testfile_*` in [env](../cypress.json) so that they contain the full value, i.e. `file_name_date-of-upload` (see examples in env).

Should the frontend be run locally in a dev setting, the [env:baseURL](../cypress.json) should reflect the correct port number (80 for Docker/3000 for dev).
