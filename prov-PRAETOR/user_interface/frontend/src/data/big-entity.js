// Import the text files
import bdsf_outEntity_0_praetor_1 from "./bdsf_outEntity_0_praetor_1.txt"
import bdsf_outEntity_0_praetor_2 from "./bdsf_outEntity_0_praetor_2.txt"
import sextractor_outEntity_0_praetor_1 from "./sextractor_outEntity_0_praetor_1.txt"
import sextractor_outEntity_0_praetor_2 from "./sextractor_outEntity_0_praetor_2.txt"

const bdsf_outEntity_0_praetor_1_content = fetch(bdsf_outEntity_0_praetor_1).then(response => response.text())

const bdsf_outEntity_0_praetor_2_content = fetch(bdsf_outEntity_0_praetor_2).then(response => response.text())

const sextractor_outEntity_0_praetor_1_content = fetch(sextractor_outEntity_0_praetor_1).then(response => response.text())

const sextractor_outEntity_0_praetor_2_content = fetch(sextractor_outEntity_0_praetor_2).then(response => response.text())

export default {
	bdsf_outEntity_0_praetor_1: bdsf_outEntity_0_praetor_1_content,
	bdsf_outEntity_0_praetor_2: bdsf_outEntity_0_praetor_2_content,
	sextractor_outEntity_0_praetor_1: sextractor_outEntity_0_praetor_1_content,
	sextractor_outEntity_0_praetor_2: sextractor_outEntity_0_praetor_2_content,
}
