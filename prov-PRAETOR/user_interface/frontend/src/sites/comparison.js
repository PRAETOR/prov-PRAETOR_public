import React, { Component } from "react"
import axios from "axios"
import { apiUrl } from "../config"
import Loading from "../components/Loading"
import ComparisonGraphsWrapper from "../components/comparison-graph.component"
import DetailTableComparison from "../components/detail-table-comparison.component"
import PipelineTitleComponent from "../components/pipeline-title.component"
import VisualizationHelper from "../services/visualization-helper.service"
import ErrorMessage from "../components/error"

/**
 * Presented when user clicks on comparison button.
 * User gets a detailed overview on repeating and unique functions and tools in selected pipelines.
 * Data is presented as tables and charts.
 */
export default class Comparison extends Component{
	constructor(props){
		super(props)

		this.state = {
			data: {},
			pipelines: window.location.pathname.split("/").pop(),
			isLoading: true,
			hasError: false
		}
	}

	getComparedData() {
		return axios({
			method: "get",
			url: apiUrl.getComparedData + this.state.pipelines,
		})
	}

	componentDidMount(){
		Promise.all([this.getComparedData()])
			.then(res => {
				this.setState({
					data: res[0].data,
					isLoading: false})
			}).catch(() => {
				this.setState({
					isLoading: false,
					hasError: true
				})
			})

		this.modifyPipelines()
	}

	/**
	 * extracts pipeline names from window path
	 */
	modifyPipelines() {
		this.setState({
			pipelines: this.state.pipelines.split("%20")
		})
	}

	render(){
		if(this.state.isLoading){
			return (
				<Loading />
			)
		}else{
			if(this.state.hasError) {
				return (
					<ErrorMessage title={"Comparison for " + this.state.pipelines}/>
				)
			}
						
			const functionNames = []//function list
			this.state.data.functions.map(i => {functionNames.indexOf(i[0])<0 ? functionNames.push(i[0]) : "" })
			//data for tables
			//functions
			const splitData = VisualizationHelper.splitSameDiff(functionNames, this.state.data.functions)
			const sameForTable = VisualizationHelper.sameFunctionsForTable(splitData.same)
			const diffForTable = VisualizationHelper.diffFunctionsForTable(splitData.diff)
			//agents (for tools and libraries)
			const agent_count = this.state.data.agents.map(i => {return [i[2], i[4], i[3]]})
			const sameAgents = agent_count.filter(i => i[1] >1)
			const diffAgents = agent_count.filter(i => i[1] <=1)

			//data for graphs
			const graphFirst = VisualizationHelper.aggregateByType(splitData.same, splitData.diff, this.state.pipelines)
			const graphSecond = VisualizationHelper.aggregateByPipeline(functionNames, this.state.pipelines, this.state.data.functions)

			return (
				<div className="mt-4 container">
					{/* pipelines */}
					<PipelineTitleComponent fileNames={this.state.pipelines} isPipeline={true} />
					{/* table with repeating tools */}
					<DetailTableComparison pipelines={this.state.pipelines} details={sameAgents} content="Repeating tools"/>
					{/* table with repeating functions */}
					<DetailTableComparison pipelines={this.state.pipelines} details={sameForTable} content="Repeating functions"/>
					{/* table with unique tools */}
					<DetailTableComparison pipelines={this.state.pipelines} details={diffAgents} content="Unique tools"/>
					{/* table with unique functions */}
					<DetailTableComparison pipelines={this.state.pipelines} details={diffForTable} content="Unique functions"/>
					{/* charts with repeating and unique functions */}
					<ComparisonGraphsWrapper pipelines={this.state.pipelines} graphFirst={graphFirst} graphSecond={graphSecond} />
				</div>
			)
		}
	}
}
