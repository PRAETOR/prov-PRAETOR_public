import React, { useState } from "react"
import VisualizationHelper from "../services/visualization-helper.service"
import { ProgressBar } from "react-bootstrap"
import SortArrowsComponent from "./sort-arrows.component"
import ClickableTableHeader from "./clickable-table-header.component"
import DownloadCsv from "./download-csv.component"

/**
 * Bidirectional chart that displays progress on left and right sides of data.
 *
 * @param props pipelines: array os strings with full pipeline names; details: array of function data indicating count per function (same as graph-2)
 * @returns {JSX.Element}
 * @constructor
 */
export default function BidirectionalChart(props) {
	const data = props.details
	const pipelines = props.pipelines

	const formattedData = VisualizationHelper.aggregateByPipelineForTwo(data)
	const { items, requestSort } = VisualizationHelper.useSortableData(formattedData)

	const p1 = VisualizationHelper.removeTimestamp(pipelines[0])
	const p2 = VisualizationHelper.removeTimestamp(pipelines[1])

	const count_total_p1 = data.filter((i, index) => i[index] !== i[0]).map(i => i[1]).reduce((prev, cur) => prev + cur, 0)
	const count_total_p2 = data.filter((i, index) => i[index] !== i[0]).map(i => i[2]).reduce((prev, cur) => prev + cur, 0)

	const isEmpty = (props.details.length <= 0)
	const [isOpen, setIsOpen] = useState(!isEmpty)

	const csvHeaders = []
	const csvData = []
	const filename = "Functions by names"
	const slicedData = data.slice(1)

	csvHeaders.push(
		{ label: p1, key: p1 },
		{ label: "Function", key: "Function" },
		{ label: p2, key: p2 }
	)

	slicedData.map(item => {
		csvData.push(
			{
				[p1]: item[1],
				"Function": item[0],
				[p2]: item[2]
			}
		)
	})

	return (
		<div className="container bidirectional">

			<div className="d-flex inline">
				<ClickableTableHeader content={filename} isOpen={(status) => setIsOpen(status)} />
				<DownloadCsv csvHeaders={csvHeaders} csvData={csvData} filename={filename} />
			</div>



			{isOpen && <table className="table table-hover ">
				<thead>
					<tr>
						<th className="text-center" onClick={() => requestSort("p1")}>
							{p1}
							<SortArrowsComponent />
						</th>
						<th className="text-center" onClick={() => requestSort("function")}>Function
							<SortArrowsComponent />
						</th>
						<th className="text-center" onClick={() => requestSort("p2")}>
							{p2}
							<SortArrowsComponent />
						</th>
					</tr>
				</thead>
				<tbody >
					{items.map((i, index) => {
						return (
							<tr key={index}>
								<td>
									{/* progress on the left for pipeline 1*/}
									<div className="progress">
										{/* reverted direction */}
										<div className="progress-bar " style={{ width: (100 - ((i.p1 * 100) / count_total_p1)) + "%", backgroundColor: "#e9ecef" }}></div>
										<div className="progress-bar " style={{ width: (i.p1 * 100) / count_total_p1 + "%", backgroundColor: "#3366cc" }}>
											{i.p1} ({((i.p1 * 100) / count_total_p1).toFixed(1)}%)
										</div>
									</div>
								</td>
								{/* function name */}
								<td className="text-center md-auto" style={{ "word-break": "break-word" }}>{i.function}</td>
								{/*  progress on the right for pipeline 2*/}
								<td>
									<ProgressBar variant="danger" now={(i.p2 * 100) / count_total_p2} label={i.p2 + " (" + ((i.p2 * 100) / count_total_p2).toFixed(1) + "%)"} />
								</td>
							</tr>
						)
					})
					}
				</tbody>
			</table>}
		</div>
	)
}
