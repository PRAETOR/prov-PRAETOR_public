import React, { useState } from "react"
import ClickableTableHeader from "./clickable-table-header.component"
import PlotlyLineChartComponent from "./plotly-line-chart.component"
import PlotyBarChart from "./plotly-bar-chart"

/**
 * Graphs for time and memory usage with function types presented on details page.
 * @param props
 */
export default function DetailTableGraphComponent(props) {
	const merged = props.merged
	const sorted = props.sorted
	const selected = props.selected //function types selected by user

	const [isOpen, setIsOpen] = useState(true)

	return (
		<div className={"mt-4  " + " charts"}>
			<ClickableTableHeader content="Consumption charts" isOpen={(status) => setIsOpen(status)} />
			{isOpen && <div className={"container " + " time-memory-graph"}>
				{/*Column Chart */}
				{/*<ColumnChart data={merged} selected={selected} title="Time consumption per function" x="function" y="time [s]" content="Time consumption" isStacked={false} />*/}

				{/* Line Chart */}
				<PlotlyLineChartComponent data={sorted} selected={selected} title="Memory consumption over time" x="time [s]" y="memory [MB]" content="plotly-memory" />
				{/* Bar chart */}
				<PlotyBarChart data={merged} selected={selected} title="Time consumption per function" x="function" y="time [s]" content="Time consumption" />
			</div>
			}
		</div>
	)
}
