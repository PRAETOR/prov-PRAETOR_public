import React, { useState } from "react"
import DetailTableEmptyComponent from "./detail-table-empty.component"
import ClickableTableHeader from "./clickable-table-header.component"
import DownloadCsv from "./download-csv.component"

/**
 * Table with sw and tools presented on details view.
 */
export default function DetailTableComponent(props) {
	const details = props.details
	const content = props.content
	const isEmpty = (props.details.length <= 0)
	const [isOpen, setIsOpen] = useState(!isEmpty)

	const csvHeaders = []
	const csvData = []

	csvHeaders.push(
		{ label: "#", key: "#" },
		{ label: "Version", key: "Version" }
	)

	details.map(entry => {
		if (entry[0] !== "") {
			csvData.push(
				{
					"#": entry[2].split("/").pop(),
					"Version": entry[2].split("/").pop()=="hadQuality" ? entry[3].split("/").pop(): entry[3].split("/").pop().replace(/[A-Za-z]/g, "")

				}
			)
		}
	})

	return (
		<div className="mt-4  sw">
			<div className="d-flex justify-content-start">
				<ClickableTableHeader content={content} isOpen={(status) => setIsOpen(status)} />
				<DownloadCsv csvHeaders={csvHeaders} csvData={csvData} filename={content} />
			</div>
			{isOpen &&
				<div className="container">
					<table className="table table-hover ">
						{isEmpty
							? <DetailTableEmptyComponent content="packages, tools, or software" />
							: <>
								<thead>
									<tr>
										<th>#</th>
										<th>Version</th>
									</tr>
								</thead>
								<tbody>
									{
										details.map((entry, index) => {
											if (entry[0] !== "") {
												return (
													<tr key={index}>
														<td>{entry[2].split("/").pop()}</td>
														<td>{entry[2].split("/").pop()=="hadQuality" ? entry[3].split("/").pop(): entry[3].split("/").pop().replace(/[A-Za-z]/g, "")}</td>
													</tr>
												)
											}
										})
									}
								</tbody>
							</>
						}
					</table>
				</div>

			}
		</div>
	)
}
