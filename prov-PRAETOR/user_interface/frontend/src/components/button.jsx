import ReactTooltip from "react-tooltip"
/**
 * Button component with tooltip and icon
 * @param props
 */
export default function Button(props) {
	const name = props.name
	const icon = props.icon
	const filled = props.filled?? false
	return(
		<div className="button" data-tip data-for={name}>
			<span className={" button-icon " + (filled ? "material-icons" : "material-icons-outlined")} >{icon}</span>
			<ReactTooltip id={name} effect="solid">{name}</ReactTooltip>
		</div>
	)
}
