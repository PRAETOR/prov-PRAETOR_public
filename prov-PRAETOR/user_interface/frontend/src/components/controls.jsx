import Button from "./button"
import React, { useState } from "react"
import axios from "axios"
import { apiUrl } from "../config"
import { useNavigate } from "react-router-dom"
import Loading from "../components/Loading"

/**
 * Component that contains buttons for the table with pipelines on the home page or sources on recommendation page.
 * Content changes based on number of selected pipelines and location of the panel.
 * Triggers changes between pages.
 *
 * @param props
 * @returns {JSX.Element}
 * @constructor
 */
export default function Controls(props) {
	const fileNames = props.fileNames
	const multipleSelected = fileNames.length > 1
	const [isShowingSummary, setIsShowingSummary] = useState(false)
	const [isLoading, setIsLoading] = useState(false)
	const isHomePage = props.isHomePage
	let navigate = useNavigate()

	/**
	 * method to switch to compare page
	 */
	const compareHandler = () => {
		const files = fileNames.map(i => i.replace("http://", "")).reduce((prev, cur) => prev + " " + cur)
		navigate("/compare/" + files)
	}
	/**
	 * method to switch to recommendation page
	 */
	const suggestionHandler = () => {
		const file = fileNames.map(i => i.replace("http://", "")).reduce((prev, cur) => prev + " " + cur)
		navigate("/suggest/" + file)
	}
	/**
	 * method to switch to details page
	 */
	const detailsHandler = () => {
		const file = fileNames.map(i => i.replace("http://", "")).reduce((prev, cur) => prev + " " + cur)
		navigate("/details/" + file)
	}
	/**
	 * method that toggles visibility of pipeline summary (saved in state)
	 */
	const summaryHandler = () => {
		props.showSummary(isShowingSummary)
		setIsShowingSummary(!isShowingSummary)
	}
	/**
	 * method to delete a pipeline and refresh the set of presented files (only active on home page)
	 */
	const deleteHandler = () => {
		const pipeline = fileNames.map(i => i.replace("http://", "")).reduce((i) => i)
		setIsLoading(!isLoading)
		axios.delete(apiUrl.deletePipeline + pipeline)
			.then(() => {
				axios
					.get(apiUrl.getPipelines)
					.then(({ data }) => {
						props.fileData(data.data)
					})
			})
			.catch((err) => {
				console.error(err)
			})
		setIsLoading(!isLoading)
	}
	// handler function for downloading pipeline
	const downloadHandler = () => {
		const pipeline = fileNames.map(i => i.replace("http://", "")).reduce((prev, cur) => prev + " " + cur) //replace("http://", "")
		const pipelineUrl = apiUrl.getProvnFile + pipeline + ".ttl"
		axios.get(pipelineUrl, { responseType: "blob" })
			.then(blob => {
				const url = window.URL.createObjectURL(new Blob([blob]),)
				const link = document.createElement("a")
				link.href = url
				link.setAttribute("download", `${pipeline}.ttl`)
				document.body.appendChild(link)
				link.click()
				link.parentNode.removeChild(link)
			})
			.catch(err => console.log(err))
	}

	return (
		<div className="pipeline-controls">
			{isLoading
				? <Loading />
				: multipleSelected //if more than one pipeline selected, only compare button is shown
					? <div className="compare" onClick={compareHandler}>
						<Button icon="compare_arrows" name="compare pipelines" />
					</div>
					: <>
						<div className="summary" onClick={summaryHandler}>
							<Button icon={isShowingSummary ? "unfold_less" : "unfold_more"} name={isShowingSummary ? "hide summary" : "show summary"} />
						</div>
						<div className="details" onClick={detailsHandler}>
							<Button icon="open_in_new" name="open" />
						</div>
						{isHomePage //home page has more controls
							? <>
								<div className="suggest" onClick={suggestionHandler}>
									<Button icon="assistant" name="suggestions" />
								</div>
								<div className="delete" onClick={deleteHandler}>
									<Button icon="delete" name="delete pipeline" />
								</div>
								<div className="download" onClick={downloadHandler}>
									<Button icon="download" name="download pipeline" />
								</div>
							</>
							: <></>
						}
					</>
			}
		</div>
	)
}
