import React from "react"
import Plot from "react-plotly.js"


/**
 * Handles data for Plotly histogram as required by selected values by user.
 * @param data default hist data
 * @param selectedVals this histograms slider range
 * @param affectedVals min/max values affected by slider range changes in other histograms
 * @returns {({marker: {color: string}, x: [], name: string, type: string}|{ marker: {color: string}, x: [], name: string, opacity: number, type: string})[]}
 */
function transformForHistogram(data, selectedVals, affectedVals) {
	let selectedValues = []
	let notSelectedValues = []

	//filter values according to user's choice of range
	selectedVals.length > 1
		? data.forEach(i => {
			let isSelected = i.value >= selectedVals[0] && i.value <= selectedVals[1]

			isSelected
				? selectedValues.push(i.value)
				: notSelectedValues.push(i.value)
		})
		: data.forEach(i => {
			selectedValues.push(i.value)
		})

	//filter values for all those affected by moved slider range in other histogram (show data dependency)
	if (affectedVals.length > 1 && (affectedVals[0] !== undefined && affectedVals[1] !== undefined)) {
		let temp = []
		selectedValues.forEach(i => {
			let withinBounds = i >= affectedVals[0] && i <= affectedVals[1]

			if (!withinBounds) {
				temp.push(i)
			}
		})
		temp.forEach(i => {
			selectedValues = selectedValues.filter(item => item !== i)
			notSelectedValues.push(i)
		})
	}

	//save into form that is acceptable by the library
	// if overlay is used, you might need to set y to count of values in x for each trace along with {autobinx: true, histnorm: "count",}
	let selected = {
		x: selectedValues,
		name: "selected",
		marker: { color: "#007E33" },
		type: "histogram"
	}
	let notSelected = {
		x: notSelectedValues,
		name: "outside selection",
		marker: { color: "#bebebe" },
		opacity: 0.75,
		type: "histogram"
	}

	return [selected, notSelected]
}

/**
 * Histogram showing input parameters for given function. Reference: https://plotly.com/javascript/histograms/
 * @param props
 */
export default function PlotlyHistogramComponent(props) {
	const data = props.data // default values available irrespective of filters
	const title = props.title
	const content = props.content
	const selected = props.selected //selected by user
	const affected = props.affected //data narrowed down by selection on other histogram

	let dataColumn = transformForHistogram(data, selected, affected)

	const optionsColumn = {
		barmode: "stacked", //overlay would be an option for showing other runs
		title: { text: "<b>" + title + "</b>" },
		bargap: 0.05,
		width: 400,
		height: 300,
		showlegend: false,
		font: { family: "Raleway", size: 12 }
	}

	return (
		<div className={"mt-4 " + content + "-graph"}>
			<Plot data={dataColumn} layout={optionsColumn} displaylogo="false" />
		</div>
	)
}
