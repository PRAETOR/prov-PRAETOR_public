/**
 * Component to replace empty tables
 * @param props
 * @returns {JSX.Element}
 * @constructor
 */
export default function DetailTableEmptyComponent(props) {
	const content = props.content //string presented instead of empty table. "No" already included below

	return (
		<>
			<thead>
				<tr><th></th></tr>
			</thead>
			<tbody>
				<tr>
					<td>No {content.toLowerCase()} </td>
				</tr>
			</tbody>
		</>
	)
}
