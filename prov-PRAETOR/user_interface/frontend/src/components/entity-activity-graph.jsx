import Plotly from "plotly.js"
import createPlotlyComponent from "react-plotly.js/factory"
const Plot = createPlotlyComponent(Plotly)

const EntityActivityGraph = (entities) => {
	// Transform the input data
	const data = entities.entities.entities.reduce((acc, entity) => {
		const { entity: e, role, value } = entity
		const found = acc.find((el) => el.entity === e && el.role === role)
		if (found) {
			found.count += 1
			found.values.push(value.split("/").pop())
		} else {
			acc.push({ entity: e, role, count: 1, values: [value.split("/").pop()] })
		}
		return acc
	}, [])


	// Group the data by role
	const groups = data.reduce((acc, entity) => {
		const { role, values } = entity
		const found = acc.find((el) => el.role === role)
		if (found) {
			found.data.push(entity)
		} else {
			const roleValues = new Map([... new Set(values)].map(x => [x, values.filter(y => y === x).length]))
			acc.push({ role, data: [entity], values: { value: Array.from(roleValues.keys()), count: Array.from(roleValues.values()) } })
		}
		return acc
	}, [])


	// Create the chart data
	const chartData = groups.map(({ role, values }) => ({
		type: "bar",
		x: values.count, //Array.from(valuesPerRole.values())[0], //data.map(({ count }) => count),
		y: values.value, //Array.from(valuesPerRole.keys())[0], //data.map(({ entity }) => entity),
		name: role,
		orientation: "h",
	}))

	const title = "Value change count per Role in Entity"

	// Create the chart layout
	const chartLayout = {
		title: {
			text: `- <b>${title}</b>`,
			x: 0.02
		},
		barmode: "group",
		xaxis: {
			title: "Count",
			tickmode: "linear"
		},
		yaxis: {
			title: "Value",
			automargin: true,
		},

	}

	const chartConfig = {
		showLink: false,
		displaylogo: false,
		responsive: true,
		toImageButtonOptions: {
			format: "png",
			filename: title.replaceAll(" ", "_")
		},
	}

	return (
		data.length ? <Plot data={chartData} layout={chartLayout} config={chartConfig} /> : <></>
	)
}

export default EntityActivityGraph



