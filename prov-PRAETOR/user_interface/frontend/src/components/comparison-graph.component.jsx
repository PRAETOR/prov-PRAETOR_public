import React, { useState } from "react"
import BidirectionalChart from "./bidirectional-chart"
import Button from "./button"
import PlotyStackedChart from "./plotly-stacked-chart"

/**
 * Wrapper for graphs on compare page
 * @param props
 */
export default function ComparisonGraphsWrapper(props) {
	const graphFirst = props.graphFirst
	const graphSecond = props.graphSecond
	const pipelines = props.pipelines
	const [isOpen, setIsOpen] = useState(true)

	return (
		<div className="mt-4 svg">
			<div className="table-header" onClick={() => setIsOpen(!isOpen)}>
				<Button name="" icon={(isOpen ? "remove" : "add")} />
				<p className="h5">Charts</p>
			</div>
			{isOpen && <div className="container">
				{graphFirst.length > 1 &&
					<>
						<PlotyStackedChart title="Functions by group" content="function group" data={graphFirst} y="count" x="function group" />
						{pipelines.length === 2
							? <BidirectionalChart pipelines={pipelines} details={graphSecond} /> //if only 2 pipelines, show bidirectional chart
							: <PlotyStackedChart title="Functions by name" content="function" data={graphSecond} y="count" x="function" />
						}
					</>
				}
			</div>
			}
		</div>
	)
}
