import React from "react"

export default function ErrorMessage(props) {
	const title = props.title

	return (
		<div className="mt-4 container">
			<p className="h5 container mb-4">{title}</p>
			<p className="h5 container mb-4">Sorry I could not fetch data for this page</p>
		</div>
	)
}
