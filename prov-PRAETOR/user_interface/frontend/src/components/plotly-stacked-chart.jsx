import React from "react"
import Plotly from "react-plotly.js"

/**
 * Creates the barchart for function time consumption (aggregated). Changes color if selection includes a function
 * @param data contains data as an array
 * @param y value for y-axis name
 * @param x value for x-axis name
 * @param selectedData functions selected by user in table
 */

export default function PlotyStackedChart(props) {
	const data = props.data //default data
	const title = props.title //chart title
	const y = props.y //y-axis title
	//const x = props.x //x-axis title
	const content = props.content //for integration tests
	const pipelines = data[0].slice(1)

	const traces = []

	pipelines.map(pipeline => {
		traces.push(
			{
				x: [],
				y: [],
				name: pipeline,
				type: "bar",
				orientation: "h"
			}
		)
	})

	const sliceData = data.slice(1)
	sliceData.map((elem) => {
		let nextIndex = 1
		traces.map(trace => {
			trace.y.push(elem[0])
			trace.x.push(elem[nextIndex])
			nextIndex++
		})
	})

	const layout = {
		title: { text: "<b>" + title + "</b>", x: 0.14, xanchor: "left" },
		yref: "paper",
		xaxis: { title: "<i>" + y + "</i>", showgrid: true, zeroline: false, showline: false, font: "Raleway" },
		yaxis: {
			automargin: true,
			font: "Raleway"
		},
		font: { family: "Raleway", size: 12 },
		barmode: "stack",
		legend: {
			"orientation": "h",
			xanchor: "center",
			yanchor: "bottom",
			y: -0.5,
			x: 0.5
		}
	}

	const config = {
		showLink: false,
		displayModeBar: true,
		toImageButtonOptions: {
			format: "png", // one of png, svg, jpeg, webp
			filename: title.replaceAll(" ", "_"),
			height: 500,
			width: 700,
			scale: 1 // Multiply title/legend/axis/canvas sizes by this factor
		},
		responsive: true,
		"displaylogo": false
	}

	return (
		<div className={"mt-4 " + content + " charts"}>
			<div className={" " + content + "-graph"}>
				<div className="row ">
					<Plotly data={traces} layout={layout} config={config} />
				</div>
			</div>
			<div className={"mt-4 "} ></div>
		</div>
	)
}