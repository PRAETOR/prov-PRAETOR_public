import React, { useState } from "react"
import DetailTableEmptyComponent from "./detail-table-empty.component"
import ClickableTableHeader from "./clickable-table-header.component"
import DownloadCsv from "./download-csv.component"
import Chip from "@mui/material/Chip"
import storedBigEntities from "../data/big-entity"

/**
 * In- and outEntities with their values presented on entity page.
 */
export default function DetailTableEntityComponent(props) {
	const details = props.details
	const content = props.content //for integration tests
	const action = props.action //consumed or produced entity?
	const isEmpty = (action === "produced" ? props.details.entities_value.length <= 0 : props.details.entities.length <= 0)
	const [isOpen, setIsOpen] = useState(true)

	const bigEntities = ["bdsf_outEntity_0_praetor_1", "bdsf_outEntity_0_praetor_2", "sextractor_outEntity_0_praetor_1", "sextractor_outEntity_0_praetor_2"]

	async function handleOnClick(value) {
		const clickedValue = value.split("/").pop().split(".")[0]
		const entityName = bigEntities.find(entity => entity === clickedValue)
		if (!entityName) return alert(value)
		const bigTextData = await storedBigEntities[entityName]
		const blob = new Blob([bigTextData], { type: "text/plain" })
		const url = URL.createObjectURL(blob)
		const link = document.createElement("a")
		link.href = url
		link.download = value.split("/").pop()
		document.body.appendChild(link)
		link.click()
		document.body.removeChild(link)
	}

	const csvHeaders = []
	const csvData = []

	action == "consumed"
		? csvHeaders.push(
			{ label: "Identifier", key: "Identifier" },
			{ label: "Parameter", key: "Parameter" },
			{ label: "Value", key: "Value" }
		)
		: csvHeaders.push(
			{ label: "Identifier", key: "Identifier" },
			{ label: "Value", key: "Value" }
		)

	action == "consumed"
		? details.entities.map(item => {
			csvData.push(
				{
					"Identifier": item["entity"],
					"Parameter": item["role"],
					"Value": item["value"]
				}
			)
		})
		: details.entities_value.map(item => {
			csvData.push(
				{
					"Identifier": item["entity"],
					"Value": item["value"]
				}
			)
		})

	return (
		<div className={"mt-4 " + content}>
			<div className="d-flex justify-content-start">
				<ClickableTableHeader content={content} isOpen={(status) => setIsOpen(status)} />
				<DownloadCsv csvHeaders={csvHeaders} csvData={csvData} filename={content} />
			</div>
			{isOpen &&
				<div className="container ">
					<table className="table table-hover">
						{isEmpty
							? <DetailTableEmptyComponent content={content} />
							: <>
								<thead>
									<tr>
										<th className="col-md-4">Identifier</th>
										{
											action === "consumed" ? <th>Parameter</th> : <th></th> //only inEntities may have relevant data here
										}
										<th >Value</th>
									</tr>
								</thead>
								<tbody>
									{
										action === "produced" ? (
											details.entities_value.map((item, index) => {
												return (
													<tr key={index}>
														<td >{item.entity}</td>{/* out-entity name */}
														<td></td>
														<td>
															{item.value.length > 40 ? (
																<Chip label={item.value.split("/").pop()} variant="contained" color="success" onClick={() => handleOnClick(item.value)} />
															) : (
																item.value
															)}
															{/*{item.value.length}*/}
														</td>{/* entity value */}
													</tr>
												)
											})
										) :
											(
												details.entities.map((item, index) => {
													return (
														<tr key={index}>
															<td>{item.entity}</td>{/* in-entity name */}
															<td >{item.role}</td>{/* parameter it fulfilled */}
															<td>
																{item.value.length > 40 ? (
																	<Chip label={item.value.split("/").pop()} variant="contained" color="success" onClick={() => handleOnClick(item.value)} />
																) : (
																	item.value
																)}
																{/*{item.value.length}*/}
															</td>{/* entity value */}
														</tr>
													)
												})
											)
									}
								</tbody>
							</>
						}
					</table>
				</div>

			}
		</div>
	)
}
