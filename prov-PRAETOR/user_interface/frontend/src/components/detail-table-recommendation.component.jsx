import DetailTableEmptyComponent from "./detail-table-empty.component"

/**
 * Page that presents recommendations
 * @param props
 * @returns {JSX.Element}
 * @constructor
 */
export default function DetailTableRecommendationComponent(props) {
	const details = props.details

	return (
		<div className="container">
			{details[1].includes("Not found")
				? <DetailTableEmptyComponent content="definition found" />
				:<table className="table table-hover recommendations">
					<thead>
						<tr>
							<th>Function</th>
							<th>Details</th>
							<th className="text-center">Invocation</th>
						</tr>
					</thead>
					<tbody>
						{details.map((entry, index) => {
							if (index > 0) {
								return (
									<tr key={index}>
										<td>{entry[0]}</td> {/* function type */}
										<td>{entry[5]}</td> {/* docstring */}
										<td className="text-center">{entry[1]}</td> {/* count */}
									</tr>
								)
							}
						})
						}
					</tbody>
				</table>
			}
		</div>
	)
}
