import React, { useEffect } from "react"
import DetailTableEntityComponent from "./detail-table-entity.component"
import ActivityChainComponent from "./activity-chain.component"
import functionData from "../data/function-data.json"

/**
 * Component that contains the data on invocation: uuid, in/outEntities, and visualization in pipeline with adjacent activities.
 * Displayed on entity details page.
 * @param props
 * @returns {*}
 * @constructor
 */
export default function DetailTableWrapperEntityComponent(props) {
	const data = props.data
	const pipeline = props.pipeline
	const selection = props.selection //selected range of data on sliders and affected data
	const activityChain = props.pipelineData

	useEffect(() => {
		//important for updates in selection to be reflected
	}, [selection])

	return (
		data.map((f, index) => {
			return (
				<div key={index}>
					<p className="h5 container mt-4 instance">Invocation {index + 1}: {f.function}</p>
					<div className="container ms-3 ">
						<DetailTableEntityComponent
							pipeline={pipeline}
							details={f.entities_in}
							content="In-Entities"
							action="consumed"
						/>
						<DetailTableEntityComponent
							pipeline={pipeline}
							details={f.entities_out}
							content="Out-Entities"
							action="produced"
						/>
						<ActivityChainComponent data={activityChain} content="Adjacent activities" invocation={f.function} functionData={functionData} pipeline={pipeline} />
					</div>
					{/* for spacing only */}
					<div className="mb-5"></div>
				</div>
			)
		})
	)
}
