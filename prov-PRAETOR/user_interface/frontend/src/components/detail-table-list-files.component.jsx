import Summary from "./summary"
import React, {useState, useEffect} from "react"

/**
 * Component that lists pipelines and presents controls if selected pipeline(-s) exist
 * @param props
 * @returns {JSX.Element}
 * @constructor
 */
export default function ListFilesComponent(props){
	const files = props.files
	const isShowingSummary = props.isShowingSummary
	const [selectedPipelines, setSelectedPipelines] = useState([]) //selected pipelines
	const clickedAway = props.isClickedAway

	/**
	 * hook to update parent component once user selection changes
	 */
	useEffect(() => {
		props.selectedFiles(selectedPipelines)
	}, [selectedPipelines])

	/**
	 * hook to reset child selection once user clicks outside the list
	 */
	useEffect(() => {
		resetState()
	}, [clickedAway])

	/**
	 * hook to update child component's selection once files changed (e.g. after delete)
	 */
	useEffect(() => {
		resetState()
	}, [files])

	/**
	 * method that updates the fileset after a pipeline was clicked.
	 * @param pipeline
	 */
	function handleClick(pipeline) {
		const currentState = selectedPipelines

		setSelectedPipelines(
			currentState.includes(pipeline.fullName)
				? selectedPipelines.filter(i => {return i !== pipeline.fullName})
				: [...selectedPipelines, pipeline.fullName]
		)
	}

	/**
	 * method that resets child's selection
	 */
	function resetState() {
		setSelectedPipelines([])
	}

	return(
		<tbody className="home-pipeline-list container">
			{
				files.map((i) => (
					<tr key={i.fullName} onClick={()=>handleClick(i)} className={selectedPipelines.includes(i.fullName) ? " selected " : "" }>
						<td key={i.fullName + "_title"} >
							<div className="pipeline-name">{i.displayName}</div>
						</td>
						<td key={i.fullName+ "_summary"}>
							{(isShowingSummary && selectedPipelines.includes(i.fullName))
								?<Summary pipeline={selectedPipelines}/>
								:<></>
							}
						</td>
					</tr>
				))
			}
		</tbody>
	)
}
