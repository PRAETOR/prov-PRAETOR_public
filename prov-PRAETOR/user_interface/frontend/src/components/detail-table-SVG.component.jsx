import React, {useState} from "react"
import ClickableTableHeader from "./clickable-table-header.component"

/**
 * Displays SVG of the pipeline on the details page.
 * @param props
 */
export default function DetailTableSVGComponent(props) {
	const data = props.data

	const [isOpen, setIsOpen] = useState(true)

	return (
		<div className="mt-4 svg">
			<ClickableTableHeader content="Graph" isOpen={(status)=>setIsOpen(status)}/>
			{isOpen && <div dangerouslySetInnerHTML={{__html: data}}></div>
			}
		</div>
	)
}
