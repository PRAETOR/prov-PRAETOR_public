import React, {useEffect, useState} from "react"
import Slider from "@mui/material/Slider"
import { createTheme } from "@mui/material/styles"
import {ThemeProvider} from "@mui/material"

/**
 * Function to facilitate calculation of min/max for slider according to the rule of thumb https://en.wikipedia.org/wiki/Histogram#Number_of_bins_and_width
 * @param max max value in array of inputs
 * @param min min value in array of inputs
 * @param bins number of bins expected on histogram
 * @returns {number}
 */
function getTickSize(max, min, bins) {
	let result = 0

	let temp = Math.round((max-min)/bins)
	let digits = temp.toString().length
	let firstDig = (Number(temp.toString().charAt(0))+1).toString()
	for(let i = 0; i<digits-1; i++) {
		firstDig += "0"
	}
	result = Math.round(temp/Number(firstDig))*firstDig

	return result
}

/**
 * Resets max so that the largest value is included with the ticksize.
 * @param max
 * @param min
 * @param bins
 * @returns {number}
 */
function getMaxDimension(max, min, bins) {
	const ticksize = getTickSize(max, min, bins)

	while ((max - ticksize*bins) > 0 ) {
		bins = bins + 1
	}
	return ticksize*bins
}

/**
 * Slider that allows to narrow down the list of displayed invocations.
 * Values in slider try to resemble those in histogram.
 * Displayed on entity page.
 * @param props
 * @returns {JSX.Element}
 * @constructor
 */
export default function RangeSliderComponent(props) {
	const range = props.range.map(i => i.value).sort((a,b) => a - b)
	const param = props.param
	const min = range[0]
	const max = range[range.length-1]

	//if min=max, check which of them should be zero
	let maxDimension = max > 0 ? max : 0
	let minDimension = maxDimension > 0 ? 0 : min

	if (min !== max) { // if min!=max, calculate the maxLimit
		maxDimension = getMaxDimension(max, min, 5)
	}

	const [values, setValues] = useState([minDimension,maxDimension])

	//this one communicates selected values back to parent
	useEffect(() => {
		props.selectedVals({"parameter": param, "values": values})
	}, [values])

	const handleChange = (event, newValue) => { setValues(newValue) }

	//just to change the color of the slide
	const theme = createTheme({ palette: { primary: { main:"#007E33" , }, },})

	return (
		<ThemeProvider theme={theme}>
			<Slider
				value={values}
				min={minDimension}
				max={maxDimension}
				onChange={handleChange}
				valueLabelDisplay="auto"
				color="primary"
			/>
		</ThemeProvider>
	)
}
