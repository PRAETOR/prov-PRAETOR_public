import React, { useState } from "react"
import { ProgressBar } from "react-bootstrap"
import DetailTableEmptyComponent from "./detail-table-empty.component"
import ClickableTableHeader from "./clickable-table-header.component"
import VisualizationHelper from "../services/visualization-helper.service"
import SortArrowsComponent from "./sort-arrows.component"
import DownloadCsv from "./download-csv.component"
/**
 * Displays statistics table for memory and time usage for function types on details page.
 * @param props
 */
export default function DetailTableStatisticsComponent(props) {
	const content = props.content
	const total = props.total
	const attribute = props.attribute
	const merged = props.merged
	const selected = props.selected //selected functions

	const [isOpen, setIsOpen] = useState(true)
	const { items, requestSort } = VisualizationHelper.useSortableData(merged) //sorter

	const csvHeaders = []
	const csvData = []

	attribute == "time"
		? merged.map(item => {
			csvData.push(
				{
					"Function": item["type"],
					"Module": item["module"],
					"Time(s)": item["time"]
				}
			)
		})
		: merged.map(item => {
			csvData.push(
				{
					"Function": item["type"],
					"Module": item["module"],
					"Memory(MB)": item["memory"]
				}
			)
		})

	attribute == "time"
		? csvHeaders.push(
			{ label: "Function", key: "Function" },
			{ label: "Module", key: "Module" },
			{ label: "Time(s)", key: "Time(s)" }
		)
		: csvHeaders.push(
			{ label: "Function", key: "Function" },
			{ label: "Module", key: "Module" },
			{ label: "Memory(MB)", key: "Memory(MB)" }
		)

	return (
		<div className={"mt-4  " + attribute}>
			<div className="d-flex justify-content-start">
				<ClickableTableHeader content={content} isOpen={(status) => setIsOpen(status)} />
				<DownloadCsv csvHeaders={csvHeaders} csvData={csvData} filename={content} />
			</div>
			{isOpen &&
				<div className="container">
					{items.length <= 0
						? <DetailTableEmptyComponent content={content} />
						: <table className="table table-hover ">
							<thead>
								<tr>
									<th onClick={() => requestSort("type")} style={{ width: "20.0%" }}> Function
										<SortArrowsComponent />
									</th>
									<th onClick={() => requestSort("module")} style={{ width: "20.0%" }}>Module
										<SortArrowsComponent />
									</th>
									<th onClick={() => requestSort(attribute)} style={{ width: "15.0%" }}>
										{attribute.charAt(0).toUpperCase() + attribute.slice(1)} {attribute === "memory" ? "(MB)" : "(s)"}
										<SortArrowsComponent />
									</th>
									{
										attribute === "time" && <th>{attribute.charAt(0).toUpperCase() + attribute.slice(1)} (%)</th>
									}
								</tr>
							</thead>
							<tbody>
								{
									items.map((entry, index) => {
										return (
											<tr key={index} className={selected.includes(entry.type) ? " selected " : ""}>
												<td>{String(entry.type)}</td> {/*String(entry.type).replace(/([A-Z])/g, " $1").replace(/^./, function(str){return str.toUpperCase()})*/}
												<td>{entry.module ?? "user-defined"}</td>
												<td >{attribute === "time" && entry[attribute] === 0 /* due to the temp fix for no timestamps */
													? "NA"
													: parseFloat(entry[attribute]).toFixed(2)}</td>
												{attribute === "time" &&
													<td>
														<ProgressBar variant="success" now={(entry[attribute] * 100) / total} label={((entry[attribute] * 100) / total).toFixed(1) + "%"} />
													</td>
												}
											</tr>
										)
									})
								}
							</tbody>
						</table>
					}
				</div>
			}
		</div>
	)
}  
