import React from "react"
import VisualizationHelper from "../services/visualization-helper.service"
import Plotly from "react-plotly.js"

/**
 * Creates the barchart for function time consumption (aggregated). Changes color if selection includes a function
 * @param data contains data as an array
 * @param y value for y-axis name
 * @param x value for x-axis name
 * @param selectedData functions selected by user in table
 */

export default function PlotyBarChart(props) {
	const data = props.data //default data
	const selected = props.selected //functions selected in table
	const title = props.title //chart title
	const y = props.y //y-axis title
	const x = props.x //x-axis title
	const content = props.content //for integration tests
	//const stackedBool = props.isStacked //if stacked, change the chart's mode


	const xData = []
	const yData = []
	const barColor = []

	data.map((i) => {
		xData.push(i.type)
		yData.push(Number(i.time))
		if (selected?.includes(i.type) && VisualizationHelper.getColor(selected?.indexOf(i.type)) !== -1) {
			barColor.push(VisualizationHelper.getColor(selected.indexOf(i.type)))
		}
		else {
			barColor.push("#007E33")
		}
	})

	const graphData = [{
		type: "bar",
		x: xData,
		y: yData,
		marker: {
			color: barColor
		}
	}]

	const layout = {
		title: { text: "<b>" + title + "</b>", x: 0.14, xanchor: "left" },
		yref: "paper",
		xaxis: { title: "<i>" + x + "</i>", showgrid: true, zeroline: false, showline: false, font: "Raleway" },
		yaxis: { title: "<i>" + y + "</i>", showgrid: true, zeroline: false, showline: false, font: "Raleway" },
		font: { family: "Raleway", size: 12 },
	}

	const config = {
		showLink: false,
		displayModeBar: true,
		toImageButtonOptions: {
			format: "png",
			filename: title.replaceAll(" ", "_"),
			height: 500,
			scale: 1 // Multiply title/legend/axis/canvas sizes by this factor
		},
		responsive: true,
		"displaylogo": false
	}

	return (
		<div className={"mt-4 " + content + " charts"}>
			<div className={" " + content + "-graph"}>
				<div className="row ">
					<Plotly data={graphData} layout={layout} config={config} />
				</div>
			</div>
			{/*<div className={"mt-4 "} ></div>*/}
		</div>
	)
}