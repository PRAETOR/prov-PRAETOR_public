import React, { useEffect, useState } from "react"
import Chip from "@mui/material/Chip"
import Stack from "@mui/material/Stack"
import Tooltip from "@mui/material/Tooltip"
import ClickableTableHeader from "./clickable-table-header.component"
import VisualizationHelper from "../services/visualization-helper.service"

/**
 * Component that displays the invocation and adjacent activities.
 * By default, only one previous and one following are shown. User can add more, if available.
 * No way to reduce, only reset.
 * Displayed on entity details page.
 * @param props
 * @returns {JSX.Element}
 * @constructor
 */
export default function ActivityChainComponent(props) {
	const data = props.data.map(i => i.function)
	const content = props.content
	const functions = props.functionData //for function definitions
	const pipeline = props.pipeline
	//let navigate = useNavigate()

	const [invocation, setInvocation] = useState(props.invocation) // this is necessary as when sliders used the invocation change is not reflected in the rest
	const [center, setCenter] = useState(data.indexOf(invocation)) //invocation
	const [minLimit, setMinLimit] = useState(center > 0 ? center - 1 : 0) //calculate limit to avoid min getting out of range
	const [min, setMin] = useState(minLimit) //left of invocation
	const [max, setMax] = useState(center + 1) //right of invocation
	const [chain, setChain] = useState(data.slice(min, max + 1)) //because slice does [min, max), ie max not included
	const [isOpen, setIsOpen] = useState(true)
	const [functionDefinitions, setFunctionDefinitions] = useState([]) //for function definitions

	/**
	 * hook to recalculate main variables if invocation changes (e.g. due to sorting in histogram)
	 */
	useEffect(() => {
		setInvocation(props.invocation)
		setCenter(data.indexOf(invocation))
		setMinLimit(center > 0 ? center - 1 : 0)
		resetState()
	}, [invocation])

	/**
	 * hook to adjust presented functions if user clicks on + left of main invocation
	 */
	useEffect(() => {
		setChain(data.slice(min, max + 1))
	}, [min])

	/**
	 * hook to adjust presented functions if user clicks on + right of main invocation
	 */
	useEffect(() => {
		setChain(data.slice(min, max + 1))
	}, [max])

	/**
	 * getting the function body for the respective function name
	 */
	useEffect(() => {
		const definitions = {}
		functions.forEach((fn) => {
			const fnName = Object.keys(fn)[0]
			definitions[fnName] = fn[fnName]
		})
		setFunctionDefinitions(definitions)
	}, [props.functions])

	const handleMin = () => {
		setMin(minLimit > 0 ? min - 1 : minLimit) // because slice doesn't like startIndex<0
	}

	const handleMax = () => {
		setMax(max + 1) //because slice will go until array.length, so it doesn't matter if it's out of bound here
	}

	/**
	 * goes back to initially presented state
	 */
	function resetState() {
		setMin(minLimit)
		setMax(center + 1)
	}

	const handleOnClick = (value) => {
		window.open(`/entities/${pipeline}/${value}`)
	}

	return (
		<>
			<ClickableTableHeader content={content} isOpen={(status) => setIsOpen(status)} />
			{isOpen && <div className="activity-chain container mb-4" >
				<Stack direction="row" spacing={1} >
					{/* + left of main invocation */}
					{min > 0 && <Chip label="+" onClick={handleMin} />}
					{/* invocations */}
					{chain.map((i, index) => {
						const fnName = VisualizationHelper.removeTimestamp(i)
						const fnDefinition = functionDefinitions[fnName]
						return (
							<div key={index}>
								<Tooltip title={fnDefinition}>
									{i === invocation
										? <Chip label={VisualizationHelper.removeTimestamp(i)} color="success" onClick={() => { handleOnClick(fnName) }} /> //main invocation
										: <Chip label={VisualizationHelper.removeTimestamp(i)} variant="outlined" onClick={() => { handleOnClick(fnName) }} /> //adjacent ones	 
									}
								</Tooltip>
							</div>

						)
					})}
					{/* + right of main invocation */}
					{max < data.length - 1 && <Chip label="+" onClick={handleMax} />}
					{/* reset button */}
					{(min !== minLimit || max !== center + 1) && <Chip label="reset" onClick={() => resetState()} />}
				</Stack>
			</div>
			}
		</>
	)
}
