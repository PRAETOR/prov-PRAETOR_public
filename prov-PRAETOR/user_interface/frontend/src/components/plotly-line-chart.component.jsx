import React from "react"
import Plot from "react-plotly.js"
import VisualizationHelper from "../services/visualization-helper.service"

/**
 * Creates the line chart for pipeline's memory consumption over time
 * @param sortedData
 * @param selectedData
 */
function createDataLineChart(sortedData, selectedData) {
	let defaultSet = sortedData
	let colored = []

	if (selectedData.length > 0) {
		colored = createColoredTraces(sortedData, selectedData) //colored traces and last processed index
		if (colored.lastIndex > 5) { //change int if color pallete grows/shrinks
			const wasColored = selectedData.slice(0, colored.lastIndex)
			defaultSet = sortedData.filter(i => i.type !== wasColored)
		}
	}

	const mainTrace = createTrace(defaultSet, "#007E33")

	let result = [mainTrace]
	if (selectedData.length > 0) {
		colored.coloredTraces.forEach(i => {
			result.push(i)
		})
	}

	return result
}

/**
 * Creates a dictionary with parameters that is later passed to plotly
 * @param data
 * @param givenColor pallete of colors available
 * @returns {{mode: string, marker: {tick0: number, color, dtick: number}, line: {shape: string}, x: [], name: string, y: [], hoverinfo: string, text: [], type: string}}
 */
function createTrace(data, givenColor) {
	let x = [] //time
	let y = [] //memory
	let labels = [] //tooltip

	data.forEach(i => {
		if (i.time === 0) {
			return
		}
		x.push(i.startedAtTime)
		x.push(i.endedAtTime)
		x.push(null)
		y.push(parseFloat(i.memory))
		y.push(parseFloat(i.memory))
		y.push(null)
		let tooltipString = "<b>function:</b> " + i.type + " <b>time:</b> " + i.time + "s "
			+ `(start:${i.startedAtTime} - end:${i.endedAtTime})`
			+ " <b>memory:</b> " + Number(parseFloat(i.memory)).toFixed(1) + " MB"
		labels.push(tooltipString)
		labels.push(tooltipString)
	})
	return {
		x: x, y: y, text: labels,
		hoverinfo: "text",
		mode: "markers+lines",
		marker: { color: givenColor, tick0: 0, dtick: 10 },
		name: "hv", line: { shape: "hv" }, type: "scatter"
	}
}

/**
 * Creates colored traces should user have selected functions earlier.
 * @param allData
 * @param selectedData
 * @returns {{coloredTraces: [], lastIndex: number}}
 */
function createColoredTraces(allData, selectedData) {
	let indexCount = 0 //stores length of processed data
	let result = []
	selectedData.forEach(i => {
		if (selectedData.indexOf(i) > 4) { //change the int here if color palette increases
			indexCount++
			return
		}
		allData.filter(item => item.type === i).forEach(i => {//select all data for that function
			let trace = createTrace([i], VisualizationHelper.getColor(indexCount)) //process each individually, otherwise the chart overlaps completely
			result.push(trace)
		})

		indexCount++
	})

	return { "coloredTraces": result, "lastIndex": indexCount }
}

/**
 * Boxplot-type line chart for time and memory usage per function
 * @param props
 */
export default function PlotlyLineChartComponent(props) {
	const data = props.data
	const selected = props.selected
	const content = props.content
	const title = props.title
	const y = props.y
	const x = props.x

	let dataLine = createDataLineChart(data, selected)

	const optionsLine = {
		title: { text: "<b>" + title + "</b>", x: 0.14, xanchor: "left" },
		yref: "paper",
		height: 500,
		xaxis: { title: "<i>" + x + "</i>", showgrid: true, zeroline: false, showline: false, font: "Raleway" },
		yaxis: { title: "<i>" + y + "</i>", showgrid: true, zeroline: false, showline: false, font: "Raleway" },
		font: { family: "Raleway", size: 12 },
		showlegend: false,
	}

	const config = {
		showLink: false,
		displayModeBar: true,
		toImageButtonOptions: {
			format: "png", // one of png, svg, jpeg, webp
			filename: title.replaceAll(" ", "_"),
			height: 500,
			scale: 1 // Multiply title/legend/axis/canvas sizes by this factor
		},
		responsive: true,
		"displaylogo": false
	}

	return (
		<div className={"mt-4 container " + { content } + " charts"}>
			<div className={"container " + { content } + "-graph"}>
				<div className="row">
					<Plot data={dataLine} layout={optionsLine} config={config} />
				</div>
			</div>
		</div>
	)
}
