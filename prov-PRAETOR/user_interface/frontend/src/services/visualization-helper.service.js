import {Component, useMemo, useState} from "react"

/**
 * Helper class that handles computation for several components.
 */
export default class VisualizationHelper extends Component{
	/**
	 * method to remove timestamp from pipeline name, which is added during upload
	 * and not needed for computation/presentation
	 * @param pipeline raw pipeline name
	 * @returns pipeline name without timestamp
	 */
	static removeTimestamp(pipeline) {
		let timeStamp = pipeline.split("_").pop()
		return pipeline.replace("_" + timeStamp, "").trim()
	}

	//raw means pipeline has timestamp
	/**
	 * method that iterates through given data and organizes it as:
	 * ["defined column title", pipeline_1 .... pipeline_n]
	 * each row has the count allocated to position referring to the pipeline
	 * @param data data aggregated by type (column[0])
	 * @param titles chart titles
	 * @param raw bool indicating whether pipeline names need additional processing
	 * @returns single row with corresponding data
	 */
	static prepareRows(data, titles, raw) {
		let temp = []
		data.forEach(i => {
			let pipeline = i[1]
			if (raw) {
				pipeline = this.removeTimestamp(pipeline)
			}

			let col = titles.indexOf(pipeline.trim()) //will not be [0]
			temp[0] = i[0]
			temp[col] = i[2]

			temp = this.completeRow(temp, titles.length - 1)
		})

		return temp
	}

	/**
	 * adds zeros to indexes with empty cells.
	 * @param data row with data
	 * @param size number of pipelines
	 * @returns row completed with zeros
	 */
	static completeRow(data, size) {
		for (let i = 1; i <= size; i++) {
			if (!data[i]) {
				data[i] = 0
			}
		}

		return data
	}

	/***
	 * method that sums function occurrences (same or unique) in a given pipeline
	 * @param data function occurrences with data on pipeline
	 * @param pipeline pipeline of interest
	 * @returns count of occurrences of same or unique functions in the pipeline
	 */
	static countFunctionPerPipeline(data, pipeline) {
		let temp = []
		data.forEach(entry => {
			entry.filter(e => {
				if (e[1].includes(pipeline.trim())) {
					temp.push(e[2])
				}
			})
		})

		temp = temp.reduce((cur, prev) => cur + prev, 0)
		return temp
	}

	/**
	 * function that reorganizes data on repeating and unique functions, aggregating them by pipelines
	 * @param sameData functions that repeat in pipelines
	 * @param diffData functions that do not repeat in pipelines
	 * @param pipelines pipelines selected by user
	 * @returns array with count of functions per pipeline per type (unique vs same)
	 */
	static aggregateByType(sameData, diffData, pipelines) {
		let graphDiff = []
		let graphSame = []
		let graph_title = ["group"]

		pipelines.forEach(p => {
			const pipelineName = this.removeTimestamp(p)
			graph_title.push(pipelineName)
			let temp = this.countFunctionPerPipeline(sameData, p)
			graphSame.push(["repeating", pipelineName, temp])

			temp = this.countFunctionPerPipeline(diffData, p)
			graphDiff.push(["unique", pipelineName, temp])
		})

		let rowSame = this.prepareRows(graphSame, graph_title, false)
		let rowDiff = this.prepareRows(graphDiff, graph_title, false)
		let result = []
		result.push(graph_title, rowSame, rowDiff)
		return result
	}

	/**
	 * function that reorganizes data on repeating and unique functions, aggregating them by function names with indication of pipelines that used them.
	 * ordered by total count per function.
	 * @param functionNames set of functions used in selected pipelines (distinct)
	 * @param pipelines	pipelines selected by user
	 * @param functions data on functions, their occurrences, and pipelines that contain them
	 * @returns array with count of function occurrence per pipeline per function name
	 */
	static aggregateByPipeline(functionNames, pipelines, functions) {
		let graph_title = ["function"]
		pipelines.forEach(p => {
			graph_title.push(this.removeTimestamp(p))
		})

		let result = []
		result.push(graph_title)

		functionNames.forEach(f => {
			let row = functions.filter(i => i[0] === f) //get all functions & their occurrences per pipeline
			row = this.prepareRows(row, graph_title, true) //func, c1, c2
			result.push(row)
		})

		result = this.sortByCount(result, pipelines)
		return result
	}

	/**
	 * sorts data by total count in descending order
	 * @param data functions and their counts per pipeline
	 * @param pipelines selected pipelines
	 * @returns reordered array
	 */
	static sortByCount(data, pipelines) {
		const lastIndex = pipelines.length //3 => [0(pipeline), 1, 2, 3]
		const title = data[0]
		let result=[]
		result.push(title)

		data.slice(1,data.length).forEach(v => {
			v.push(v.filter((i, index) => {return index>0 ?  i:""}).reduce((prev, cur) => prev+cur,0))//add totalcount for each pipeline
		})
		let sorted = data.slice(1,data.length).sort((a,b)=> a[lastIndex+1]<b[lastIndex+1]) //sort by totalcount
		sorted = sorted.slice(0,data.length).map(i=> i.slice(0,lastIndex+1)) //remove totalcount
		sorted.forEach(i=>{result.push(i)})

		return result
	}

	/**
	 * method to distinguish repeating vs uniquely used functions
	 * @param functionNames set of functions used in selected pipelines (distinct)
	 * @param data data on functions, their occurrences, and pipelines that contain them
	 * @returns dictionary with array of repeating functions and array of unique functions
	 */
	static splitSameDiff(functionNames, data) {
		const sameFunctions = []
		const diffFunctions = []
		functionNames.forEach(i => {
			const functions = data.filter(entry => entry[0] === i)
			functions.length > 1
				? sameFunctions.push(functions) //[function: pipeline : count]
				: diffFunctions.push(functions)
		})

		return {"same": sameFunctions, "diff" : diffFunctions}
	}

	/***
	 * method that organizes same functions data for presentation in tables
	 * @param data functions that occur >1 in the dataset
	 * @returns count of repeating functions
	 */
	static sameFunctionsForTable(data) {
		const result = []
		data.forEach(i => {
			let count = i
				.map(e => { return [e[0], e[2]]})
				.reduce((cur, prev) => [prev[0], cur[1]+prev[1]])
			count[1] > 1
				?  result.push(count) : ""
		})
		return result
	}

	/***
	 * method that organizes unique functions data for presentation in tables
	 * @param data functions that occur once in the dataset
	 * @returns count of unique functions
	 */
	static diffFunctionsForTable(data) {
		const result = []
		data.forEach(i=> {
			result.push(i.map(e => { return e[0]}))
		})
		return result
	}

	/***
	 * Sorts data according to type passed. Used in tabular data presentation.
	 * @param items dataset to be sorted
	 * @param config datatype to sort by (e.g. function, memory, time)
	 * @returns {{requestSort: requestSort, sortConfig: unknown, items: *[]}}
	 */
	static useSortableData(items, config = null){
		const [sortConfig, setSortConfig] = useState(config)

		const sortedItems = useMemo(() => {
			let sortableItems = [...items]
			if (sortConfig !== null) {
				sortableItems.sort((a, b) => {
					if (a[sortConfig.key] < b[sortConfig.key]) {
						return sortConfig.direction === "ascending" ? -1 : 1
					}
					if (a[sortConfig.key] > b[sortConfig.key]) {
						return sortConfig.direction === "ascending" ? 1 : -1
					}
					return 0
				})
			}
			return sortableItems
		}, [items, sortConfig])

		const requestSort = (key) => {
			let direction = "ascending"
			if (
				sortConfig &&
            sortConfig.key === key &&
            sortConfig.direction === "ascending"
			) {
				direction = "descending"
			}
			setSortConfig({ key, direction })
		}

		return { items: sortedItems, requestSort, sortConfig }
	}

	/**
	 * Function that prepares function data for presentation on entity page by grouping each entity and it's properties by function invocation and applied filters. Some properties are dropped.
	 * Initially, data is not aggregated.
	 * @param inEntities
	 * @param outEntities
	 * @param filter
	 * @returns {[]}
	 */
	static reorganizeEntityData(inEntities, outEntities, filter) {
		let invocationSet =[]
		// get only those activity uuids that comply with the given filter
		if(filter === undefined || filter.length < 1) {
			invocationSet = inEntities.entities?.map(i => i.activity) //all functions accept parameters, not all produce entities
		}else {
			let temp = []
			//function may have several parameters. get all those function uuids that comply with individual filter constraints.
			filter.forEach(f => {
				let relevantEntries = inEntities.entities.filter(i => i.role === f.parameter && i.value >= f.values[0] && i.value <= f.values[1]).map(i=>i.activity)
				relevantEntries.forEach(i => {
					let loc = temp.filter(item => item.activity === i)
					if (loc.length <1) {
						temp.push({"activity": i, "count": 1})
					} else {
						let count = temp.filter(item => item.activity === i).map(item => item.count)
						temp = temp.filter(item => item.activity !== i)
						temp.push({"activity": i, "count": Number(count)+1})
					}
				})
			})
			// leave only those function uuids in that fulfill all filters
			const threshold = filter.length
			invocationSet = temp.filter(i => i.count >= threshold).map(i => i.activity)
		}
		invocationSet = new Set(invocationSet)

		let result = []
		// change the data structure to make rendering on entity page easier
		invocationSet.forEach(invocation => {
			let inEntities_temp_arr = inEntities.entities
				.filter(i => i.activity === invocation)
				.map(i => ({"entity": i.entity, "type": i.type, "datatype": i.datatype, "value": i.value, "role": i.role, "startTime": i.startTime, "endTime": i.endTime}))

			let outEntities_temp_arr = outEntities.entities_value.filter(i => i.activity === invocation).map(i => ({"entity": i.entity, "type": i.type, "value": i.value, "role": i.role}))

			let outEntities_derived_temp_arr = outEntities.entities_wasDerivedFrom.filter(i => i.activity === invocation).map(i => ({"entity": i.entity, "derivedFrom": i.value}))

			result.push({"function": invocation,
				"entities_in": {"entities": inEntities_temp_arr },
				"entities_out": {"entities_value" : outEntities_temp_arr, "entities_wasDerivedFrom": outEntities_derived_temp_arr}
			})
		})

		return result
	}

	/***
	 * Function drafted to facilitate synching labels in Sliders but currently helps to synch histograms after slider range was changed by user.
	 * @param data
	 * @returns {[]}
	 */
	static rangeSiever(data) {
		const relevantDataTypes = ["int", "float", "long"]
		let relevantData = data
			.map(i => i.entities_in.entities) //[[objs],[objs]...]
			.reduce((prev, cur ) => prev.concat(cur), [])//[objs]
			.filter(i => relevantDataTypes.includes(i.datatype))
			.map(i => ({"parameter": i.role, "value": i.value})) //{dper: 10204}, {dpro: 12}....
		let params = new Set(relevantData.map(i => i.parameter))

		let result = []
		params.forEach(p => {
			result.push({"parameter": p, "values": relevantData.filter(i => i.parameter === p).map(i => Number(i.value))})
		})
		return result
	}

	/**
	 * Method to sieve out parameters for passed inEntities of a function. Used to generate histograms on entity page
	 * @param inEntities inEntities for given function type with invocation, param and value props present
	 * @returns {*}
	 */
	static numberSiever(inEntities) {
		const relevantDataTypes = ["int", "float", "long"]
		let inputs = inEntities.entities.filter(i => relevantDataTypes.includes(i.datatype)).map(i => ({"parameter": i.role, "value": i.value, "invocation": i.activity}))
		let params = new Set(inputs.map(i => i.parameter))

		let result = []
		params.forEach(p => {
			result.push({"parameter": p, "inputs": inputs.filter(i => i.parameter === p).map(i => ({"invocation": i.invocation, "value": Number(i.value)}))})
		})
		return result
	}

	/**
	 * Method that re-arranges function data for two pipelines. Only repeating functions are considered.
	 * @param functionData dataset used for graph-2
	 * @returns {[]}
	 */
	static aggregateByPipelineForTwo(functionData) {
		let header = ["function", "p1", "p2"]

		let result = []
		functionData
			.filter((i, index) => i[index] !== i[0] ) //&& (i[1]>0 && i[2]>0) for repeating only
			.forEach(i => {
				let datapoint = {}
				header.forEach((el, index) => {
					datapoint[el] = i[index]
				})
				result.push(datapoint)
			})
		return result
	}


	/**
	 * Consumption data processed to provide total memory/time and sorted entries.
	 * @param data
	 * @returns {{relevant_consumption_data, sorted, total_memory: number, merged: (({memory: *, type: *} & {time: *, type: *})|undefined)[], total_time: number}}
	 */
	static sortTimeMemoryForDetails(data) {
		let total_memory = 0
		let total_time = 0

		// filter the Activity type elements out since they have no time and memory-- TODO: check if this is an issue since the query was modified
		let relevant_consumption_data = data.filter((e) => e.type !== "Activity") //TODO check if this should be activityName?

		relevant_consumption_data.forEach((element) => {
			// if there is no memory at the element do not add it
			if (!isNaN(element.memory)) {
				total_memory += parseFloat(element.memory)
			}
			const time = (new Date(element.endedAtTime)- new Date(element.startedAtTime)) / 1000
			element["time"] = isNaN(time) ? 0 : time
			total_time += element["time"]
		})

		// accumulate time and memory of the functions with the same name
		let function_memory = Array.from(
			relevant_consumption_data.reduce(
				(m, { type, memory }) =>
					m.set(type, (m.get(type) || 0) + parseFloat(memory)), //memory summed here
				new Map()
			),
			([type, memory]) => ({ type, memory })
		)

		//convert memory to avg
		function_memory.forEach(e => e.memory = e.memory/(relevant_consumption_data.filter(i => i.type === e.type).length))

		const function_time = Array.from(
			relevant_consumption_data.reduce(
				(m, { type, time }) =>
					m.set(type, (m.get(type) || 0) + parseFloat(time)),
				new Map()
			),
			([type, time]) => ({ type, time })
		)

		const function_module = Array.from(
			relevant_consumption_data.reduce(
				(m, { type, module }) => m.set(type, module),
				new Map()
			),
			([type, module]) => ({ type, module })
		)

		let merged = function_memory.map((item, i) => {
			if (item.type === function_time[i].type) {
				return Object.assign({}, item, function_time[i])
			}
		})

		merged = merged.map((item, i) => {
			if (item.type === function_module[i].type) {
				return Object.assign({}, item, function_module[i])
			}
		})

		// sort the data by their start time to make it easier to evaluate them in a graph
		let sortedMemory = relevant_consumption_data.sort(
			(a, b) => new Date(a.startedAtTime) - new Date(b.startedAtTime)
		)

		return {"relevant_consumption_data": relevant_consumption_data, "merged": merged, "sorted": sortedMemory, "total_time": total_time, "total_memory": total_memory}
	}

	/**
 * contains a set of colors for selected functions. If too many functions selected, no color is returned.
 * @param index
 * @returns {string|string}
 */
	static getColor(index) {
		const colors = ["#D00000", "#FFBA08", "#3F88C5","#032B43", "#F3722C", "#a9a9a9"]
		return colors[index]
	}
}
