import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap/dist/js/bootstrap.min.js"
import "./App.css"

import { Route, BrowserRouter as Router, Routes } from "react-router-dom"

// List all sites/complex components available in the application
import Header from "./components/header.component"
import Home from "./sites/home"
import YasguiSparql from "./components/yasgui.component"
import Import from "./sites/import"
import Details from "./sites/details"
import Comparison from "./sites/comparison"
import Recommender from "./sites/recommender"
import Entities from "./sites/entities"


// Define their routes
function App() {
	return (
		<Router>
			<div className="App">
				<Header />
				<Routes>
					<Route exact path="/" element={<Home />}></Route>
					<Route exact path="/yasgui" element={<YasguiSparql />}></Route>
					<Route exact path="/import" element={<Import />}></Route>
					<Route exact path="/details/:pipeline" element={<Details />}></Route>
					<Route exact path="/compare/:pipelines" element={<Comparison/>}></Route>
					<Route exact path="/suggest/:pipeline" element={<Recommender/>}></Route>
					<Route exact path="/entities/:pipeline/:function" element={<Entities />}></Route>
				</Routes>
			</div>
		</Router>
	)
}

export default App
