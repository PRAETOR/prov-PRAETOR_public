package com.converter.provnconverter;

import com.converter.provnconverter.storage.StorageFileNotFoundException;
import com.converter.provnconverter.storage.StorageProperties;
import com.converter.provnconverter.storage.StorageService;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * contains the REST-Endpoints for the communication
 */
@Controller
public class FileUploadController {

    private final StorageService storageService;
    private final static List<String> VALID_EXTENSION = Arrays.asList("ttl", "json", "xml", "provn");

    @Autowired
    public FileUploadController(StorageService storageService) {
        this.storageService = storageService;
    }

@GetMapping("/")
    public String listUploadedFiles(Model model) {

        model.addAttribute("files", storageService.loadAll().map(
                        path -> MvcUriComponentsBuilder.fromMethodName(FileUploadController.class,
                                "serveFile", path.getFileName().toString()).build().toUri().toString())
                .collect(Collectors.toList()));

        return "uploadForm";
    }

@CrossOrigin
    @GetMapping("/allFiles")
    @ResponseBody
    public List<String> getAllFiles(){
        return storageService.loadAll().map(
                        path -> MvcUriComponentsBuilder.fromMethodName(FileUploadController.class,
                                "serveFile", path.getFileName().toString()).build().toUri().toString())
                .collect(Collectors.toList());
    }

    public ResponseEntity<String> sendFile(String fileName, boolean append){
        File f = new File(fileName);
        byte[] someByteArray = new byte[0];
        try {
            someByteArray = Files.readAllBytes(f.toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);

        MultiValueMap<String, String> fileMap = new LinkedMultiValueMap<>();
        ContentDisposition contentDisposition = ContentDisposition
                .builder("form-data")
                .name("file")
                .filename(fileName)
                .build();

        fileMap.add(HttpHeaders.CONTENT_DISPOSITION, contentDisposition.toString());

        HttpEntity<byte[]> fileEntity = new HttpEntity<>(someByteArray, fileMap);

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", fileEntity);
        body.add("append", append);

        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, httpHeaders);

        ResponseEntity<String> response = null;
        try {
            response = new RestTemplate()
                .exchange("http://frontend.svc/api/upload/", HttpMethod.POST, requestEntity, String.class);
        }catch (HttpClientErrorException e){
            e.printStackTrace();
        }

        return response;
    }

    @CrossOrigin
    @GetMapping("/files/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> serveFile(@PathVariable String filename) {

        Resource file = storageService.loadAsResource(filename);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

    @CrossOrigin
    @PostMapping("/")
    public ResponseEntity handleFileUpload(@RequestParam("file") MultipartFile file, @RequestParam("append") boolean append) {

        // check if the file has a valid extension (for conversion)
        if(!VALID_EXTENSION.contains(FilenameUtils.getExtension(file.getOriginalFilename()))){
            return new ResponseEntity(HttpStatus.UNPROCESSABLE_ENTITY);
        }

        // check if the file is empty
        if(file.isEmpty()){
            return new ResponseEntity(HttpStatus.CONFLICT);
        }

        // store the (unconverted) file
        storageService.store(file);

        StorageProperties storageProperties = new StorageProperties();
        Path filePath = Paths.get(storageProperties.getLocation());

        Path destinationFile = filePath.resolve(Paths.get(file.getOriginalFilename()))
                        .normalize().toAbsolutePath();

        FileConverter fileConverter = new FileConverter();

        // convert input file to ttl - file (for upload to the DB)
        try{
            fileConverter.doConversion(destinationFile.toString(), ".ttl");
        }catch(Exception e){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        // convert the file to a svg - file (image for the frontend)
        // for this conversion it is necessary to have graphviz installed (check this out
        // https://graphviz.gitlab.io/download/)
        //try{
        //    fileConverter.doConversion(destinationFile.toString(), ".svg");
        //}catch(Exception e){
        //    return new ResponseEntity(HttpStatus.BAD_REQUEST);
        //}

        String fileOut = destinationFile.toString().replace("upload-dir", "convert-dir");
        fileOut = FilenameUtils.removeExtension(fileOut);
        fileOut += ".ttl";

        // send the converted ttl file to the DB
        sendFile(fileOut, append);

        return ResponseEntity.ok().build();
    }

    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
        return ResponseEntity.notFound().build();
    }

}

