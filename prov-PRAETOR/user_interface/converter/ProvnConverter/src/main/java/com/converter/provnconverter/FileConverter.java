package com.converter.provnconverter;

import org.apache.commons.io.FilenameUtils;
import org.openprovenance.prov.interop.InteropFramework;
import org.openprovenance.prov.model.Document;

public class FileConverter {
    /**
     * Converts the uploaded file to the turtle format
     * @param fileIn [String] absolute path of input file
     * @param format [String] format that the file should be converted to
     */
    public void doConversion(String fileIn, String format){
        String fileOut = fileIn.replace("upload-dir", "convert-dir");
        fileOut = FilenameUtils.removeExtension(fileOut);        
        fileOut += format;

        InteropFramework interopFramework = new InteropFramework();
        Document document = interopFramework.readDocumentFromFile(fileIn);

        interopFramework.writeDocument(fileOut, document);
    }
}
