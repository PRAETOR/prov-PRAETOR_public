package com.converter.provnconverter.storage;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("storage")
public class StorageProperties {
    private String location = "upload-dir";
    private String convertLocation = "convert-dir";

    public String getLocation() {
        return location;
    }

    public String getConvertLocation(){
        return convertLocation;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setConvertLocation(String convertLocation){
        this.convertLocation = convertLocation;
    }

}
