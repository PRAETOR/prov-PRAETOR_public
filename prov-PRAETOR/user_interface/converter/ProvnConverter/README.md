# SWEP-Radioastronomie - ProvnConverter

This folder contains the converter code of the *Radioastronomie* project. This converter is based on the [ProvToolbox](https://www.github.com/lucmoreau/ProvToolbox). The application is java based and uses maven as a dependency management system.

## Usage
- install java and maven
- start the application (locally)
```console
mvn spring-boot:run
```
- visit https://localhost:8090/

## Structure
- [upload-dir](upload-dir) (contains the uploaded files from the frontend)
- [convert-dir](convert-dir) (contains the converted files from the ProvToolbox)
- [src/main/java](src/main/java) (contains the logic for the conversion)
    - [FileUploadController](src/main/java/FileUploadController.java) (contains the REST-Endpoints for the communication)
    - [FileConverter](src/main/java/FileConverter.java) (contains the usage of the ProvToolbox (-> conversion of the file))
    - [storage/](src/main/java/storage) (contains generic interfaces for the file storage (at the moment on filesystem level))
    - [resources/](src/main/java/resources) (contains minimal testing environment with web UI (https://localhost:8090))
- [Dockerfile](Dockerfile) (contains the docker configuration for the converter)