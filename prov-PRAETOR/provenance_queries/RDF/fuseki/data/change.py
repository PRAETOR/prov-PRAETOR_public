in_prov = 'example_search_prov.ttl'
out_prov = in_prov.replace('.ttl', '_changed.ttl')

with open(in_prov,'r') as f:
    data = f.read()
data = data.replace('urn:uuid:>', 'urn::uuid:>')
data = data.replace('urn:uuid', 'urn_uuid')
data = data.replace('urn::uuid:>', 'urn:uuid:>')

with open(out_prov, 'w') as f:
    f.write(data)
