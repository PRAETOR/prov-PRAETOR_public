# RDF/fuseki

Here we explain how to query prov-PRAETOR provenance within a fuseki RDF triple store.

## Requirements

The python modules requires a praetor installation (>3.1) within your python environment.

```
pip install praetor
```

To run the examples you may also want to install.

```
pip install jupyter
```

(Note we would recommend to install this inside of a conda enviroment.)


A working [fuseki](https://jena.apache.org/documentation/fuseki2/) database installation is also required. The docker implementation can be pull by using the following command

```
docker pull secoresearch/fuseki
```

Have the container running in the background 
```
docker run --rm -it -p 3030:3030 --name fuseki -e ADMIN_PASSWORD=admin -e ENABLE_DATA_WRITE=true -e ENABLE_UPDATE=true -e ENABLE_UPLOAD=true -e QUERY_TIMEOUT=60000 secoresearch/fuseki
```
Once you run the container the service can be found at [localhost:3030](http://localhost:3030)

Please note that the password and username is admin. For more information on running this container, please see - https://github.com/SemanticComputing/fuseki-docker


Native installation instructions for fuseki can be found [here](https://jena.apache.org/documentation/fuseki2/fuseki-docker.html), alternatively the  container for the [user interface](https://gitlab.mpcdf.mpg.de/PRAETOR/prov-PRAETOR_public/-/tree/main/prov-PRAETOR/user_interface?ref_type=heads) is a compatible solution. 


## Running the code

The [praetor\_analysis.py](https://gitlab.mpcdf.mpg.de/PRAETOR/prov-PRAETOR_public/-/blob/main/prov-PRAETOR/provenance_generation/python3/praetor_analysis_rdf.py) and [add\_quality.py](https://gitlab.mpcdf.mpg.de/PRAETOR/prov-PRAETOR_public/-/blob/main/prov-PRAETOR/provenance_generation/python_package/praetor/praetor/add_quality.py) modules includes a large collection of functions for querying and modifyng provenance.

For instructions and example usage, please see the notebooks in the [tutorials](https://gitlab.mpcdf.mpg.de/PRAETOR/prov-PRAETOR_public/-/tree/main/prov-PRAETOR/provenance_queries/RDF/fuseki/tutorials) directory. 

Note, the code is based on python 3 and mostly compatible for python 2 (python 2 use, you will need to change "from pandas.io.json import json_normalise" to "from pandas.json import json_normalise") in praetor_analysis.py.

The fuseki triple store consumes provenance in ttl format. Conversion from provn format to ttl format can be achieved via [convert2TTL.py](https://gitlab.mpcdf.mpg.de/praetor/praetor_public/-/blob/main/praetor/provenance_queries/RDF/fuseki/convert2TTL.py), usage is as follows:

```
python convert2TTL.py provenance_file.provn
```

This command will produce provenance_file.ttl, ready to be uploaded to the database.

In case you need to do this by hand, the [ProvToolBox](https://lucmoreau.github.io/ProvToolbox/) can be used to transform a provn format dataset to ttl format

```
provconvert --infile example.provn --outfile example.ttl
```

Similarly, to translate to json format:

```
provconvert --infile example.provn --outfile example.json
```


