import os, fnmatch, subprocess

def findReplace(directory, find, replace, filePattern):
    for path, dirs, files in os.walk(os.path.abspath(directory)):
        for filename in fnmatch.filter(files, filePattern):
            filepath = os.path.join(path, filename)
            with open(filepath) as f:
                s = f.read()

            s = s.replace(find, replace)
            with open(filepath, "w") as f:
                f.write(s)
            if find in filename:
                new_name = filename.replace(find, replace)
                subprocess.call(['mv', path+'/'+filename, path+'/'+new_name])

def find_print(directory, find, replace, filePattern):
    for path, dirs, files in os.walk(os.path.abspath(directory)):
        for filename in fnmatch.filter(files, filePattern):
            filepath = os.path.join(path, filename)
            with open(filepath) as f:
                for line in f.readlines():
                    if find in line:
                        print(line[line.index(find):line.index(find)+7])
                        print(filepath, line)

for f_type in ['*.py' ,'*.json', '*.md', '*.txt', '*.ipynb', '*.provn', '*.rst', '*.html', '*.js']:
	findReplace('.', 'praetor', 'praetor', f_type)
	findReplace('.', 'prtr', 'prtr', f_type)
	#findReplace('.', 'praetor', 'praetor', '*.py')
	findReplace('.', 'PRAETOR', 'PRAETOR', f_type)

#findReplace('.', 'praetor', 'praetor', '*.md')
#findReplace('.', 'PRAETOR', 'PRAETOR', '*.md')
#findReplace('.', 'prtr', 'prtr', '*.md')

