# Provenance Queries

The code needed to query the provenance is contained within the praetor python package (only within the python 3 version, due to some requirements). In case you have not installed the praetor python package please follow the instruction at [prov_PRAETOR](https://gitlab.mpcdf.mpg.de/PRAETOR/prov-PRAETOR_public/-/tree/main/prov-PRAETOR/provenance_generation?ref_type=heads#installation)

Due to the size of the provenance recording a simple python module interfact might not scale as required and we offer two different set of frameworks to query provenance using either RDF triple stores or graph databases. Note that both approaches are valid for investigation, however RDF triple stores offer more features and tools therefore we recommend RDF triple stores.


The [RDF/fuseki](https://gitlab.mpcdf.mpg.de/PRAETOR/prov-PRAETOR_public/-/tree/main/prov-PRAETOR/provenance_queries/RDF/fuseki) and [Neo4j](https://gitlab.mpcdf.mpg.de/PRAETOR/prov-PRAETOR_public/-/tree/main/prov-PRAETOR/provenance_queries/Neo4j) directories contain all of the necessary information for uploading and querying provenance within fuseki triple store and a Neo4j graph database, respectively. This includes:
- installation instructions
- python modules containing functions for querying and uploading
- python notebook tutorials, utilising the aformentioned python modules

Note that one of these services needs to be running. 
