# Demo-repo for provenance queries

This repository contains examples of the queries used within the Tapp 2024 serialisation paper. 
It contains an ipython notebook for each serialisation. These notebooks contain examples of all queries used and a python wrapper for upload the data to the databases and executing the queries.

To start, you will need to generate some PRAETOR provenance for your python pipeline, instructions on how to do so can be found [here](https://gitlab.mpcdf.mpg.de/PRAETOR/prov-PRAETOR_public/-/tree/main/prov-PRAETOR/provenance_generation).
This provenance will then need to be converted to either turtle or json format for the triple store and graph database notebooks, respectively. This can be achieved via the [ProvToolBox](http://lucmoreau.github.io/ProvToolbox/). 

You will also need working triple store and graph data installtions, instructions can be found [here](https://gitlab.mpcdf.mpg.de/PRAETOR/prov-PRAETOR_public/-/tree/main/prov-PRAETOR/provenance_queries/RDF/fuseki) and [here](https://gitlab.mpcdf.mpg.de/PRAETOR/prov-PRAETOR_public/-/tree/main/prov-PRAETOR/provenance_queries/Neo4j), respectively (note, the Neo4j instructions rely upon the prov2neo python package which is broken at the time of writing due to a deleted dependancy).

Finally, install the python requirements contained within [requirements.txt](https://gitlab.mpcdf.mpg.de/PRAETOR/prov-PRAETOR_public/-/blob/main/prov-PRAETOR/provenance_queries/turtle_vs_rdf/requirements.txt), change the name of prov_file within each notebook to that of your provenance data, update the database info in each notebook (username, db name, password, etc.), and run the queries. 

