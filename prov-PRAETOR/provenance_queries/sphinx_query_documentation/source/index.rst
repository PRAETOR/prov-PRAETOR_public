.. provenance_queries documentation master file, created by
   sphinx-quickstart on Thu Jul 13 19:43:48 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to provenance_queries's documentation!
==============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

=====
Neo4j
=====

.. automodule:: praetor_analysis_neo
   :members:

.. currentmodule:: praetor_analysis_neo
.. autosummary::
   :toctree: _autosummary

===
RDF
===

.. automodule:: praetor_analysis_rdf
   :members:

.. currentmodule:: praetor_analysis_rdf
.. autosummary::
   :toctree: _autosummary


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
