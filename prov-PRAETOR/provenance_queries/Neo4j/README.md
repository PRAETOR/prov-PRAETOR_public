# Neo4j

Here we explain how to query prov-PRAETOR provenance within a Neo4j graph database.

The Neo4j graph database work is contingent on the [prov2neo](https://github.com/DLR-SC/prov2neo) python package which is currently broken. So these tutorials may not work for the time being. 

## Requirements

The python modules required are as follows:
- [prov](https://pypi.org/project/prov/)
- [neo4j](https://pypi.org/project/neo4j/)
- [prov2neo](https://pypi.org/project/prov2neo/)

All can be installed with the following command:
```
pip install prov neo4j prov2neo
```
Python 3.7+ reccomended. 

A working Neo4j installation is also required, installation instructions can be found [here](https://neo4j.com/docs/operations-manual/current/installation/)

Once you have installed Neo4j for your operating system, you just start the service and check by visiting [localhost:7474](localhost:7474)


## Usage

The python notebook [tutorial.ipynb](https://gitlab.mpcdf.mpg.de/PRAETOR/prov-PRAETOR_public/-/blob/main/prov-PRAETOR/provenance_queries/Neo4j/tutorial.ipynb) demonstrates usage for each function contained within [praetor_analysis_neo.py](https://gitlab.mpcdf.mpg.de/PRAETOR/prov-PRAETOR_public/-/blob/main/prov-PRAETOR/provenance_generation/python3/praetor_analysis_neo.py?ref_type=heads).

