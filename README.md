![alt text](PROV_PRAETOR_LOGO.jpg "prov-PRAETOR"){width=25%}



# prov-PRAETOR

prov - **P**ipeline p**R**ovenance for **A**nalysis **E**valuation **T**rust **O**r **R**eproducibility


## What is prov-PRAETOR?

It is a software suite that automatically documents the processing of a python workflow. The software provides sufficient information for reproducibility and analysis. Post-proverance generation, the software suite includes tools to analyse and interpretate the provenance. 


## Why do you want to use it?

You are running a workflow and want to understand what kind of python-functions and classes and their inputs and outputs are used. A suite of use-cases are compiled in [Johnson et al. 2021](https://www.usenix.org/conference/tapp2021/presentation/johnson) and a first sight on the evaluation process is discussed in [Johnson et al. 2024](https://arxiv.org/pdf/2407.14290).
	

## What do you expect to get?

The provenance data is stored in [provn](https://www.w3.org/TR/prov-n/) format. The data model used to structure the provenance is based on the PROV standard. However to accompany all the use cases the data model has been extended. The additional model components are described in [the data model](https://gitlab.mpcdf.mpg.de/PRAETOR/prov-PRAETOR_public/-/tree/main/prov-PRAETOR/provenance_model).

## How the software suite has been organised

- [provenance_generation](https://gitlab.mpcdf.mpg.de/PRAETOR/prov-PRAETOR_public/-/tree/main/prov-PRAETOR/provenance_generation?ref_type=heads) - records provenance of a workflow execution 
- [user_interface](https://gitlab.mpcdf.mpg.de/PRAETOR/prov-PRAETOR_public/-/tree/main/prov-PRAETOR/user_interface?ref_type=heads) - GUI to explore the workflows provenance
- [provenance_queries](https://gitlab.mpcdf.mpg.de/PRAETOR/prov-PRAETOR_public/-/tree/main/prov-PRAETOR/provenance_queries?ref_type=heads) - python modules for analysis of provenance, using [Neo4J](https://neo4j.com) graph databases and/or RDF triple stores, [fuseki](https://jena.apache.org/documentation/fuseki2/) and [rdflib](https://rdflib.readthedocs.io/en/stable/)

## Lets start

Go [here](https://gitlab.mpcdf.mpg.de/PRAETOR/prov-PRAETOR_public/-/tree/main/prov-PRAETOR)

## A full n2n nutshell-example 

Go [here](https://gitlab.mpcdf.mpg.de/PRAETOR/prov-PRAETOR_public/-/tree/main/prov-PRAETOR/provenance_generation/singularity/prov-PRAETOR_nutshell_example)

## Acknowledgement
We acknowledge the support of the German Federal Ministry for Economic Affairs and Energy on the basis of a decision of the German Bundestag under the project number 50OO1905.

## If you use the code 

Please cite: <br>

&nbsp; **Pipeline Provenance for Analysis, Evaluation, Trust or Reproducibility** <br>
&nbsp; Johnson, Klöckner, Muzafarova, Lackeos, Champion, Dembska, Schindler, Paradies <br>
&nbsp; Research Notes of the AAS, 8, 4, 2024 <br>
&nbsp; [paper and bibtex](https://iopscience.iop.org/article/10.3847/2515-5172/ad3dfc/meta)



## People involved

Michael Johnson,
Kristen Lackeos,
Albina Muzafarova,
Alex Otsepaev,
Johannes Riedel,
Mursalin Sayeed,
Marta Dembska,
Lynn Hansen,
Marcus Paradies,
Sirko Schindler,
David Champion,
Hans-Rainer Klöckner

2020-2024

